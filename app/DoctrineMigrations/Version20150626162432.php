<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150626162432 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE brainstyle_characteristics (id VARCHAR(32) NOT NULL, style VARCHAR(32) DEFAULT NULL, text LONGTEXT NOT NULL, locale VARCHAR(512) DEFAULT NULL, INDEX IDX_972D857433BDB86A (style), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE brainstyle_tip_translations (id INT AUTO_INCREMENT NOT NULL, object_id VARCHAR(32) DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_7FD0F07E232D562B (object_id), UNIQUE INDEX lookup_unique_idx (locale, object_id, field), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE brainstyle_tips (id VARCHAR(32) NOT NULL, text LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE brainstyle_characteristics ADD CONSTRAINT FK_972D857433BDB86A FOREIGN KEY (style) REFERENCES test_styles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brainstyle_tip_translations ADD CONSTRAINT FK_7FD0F07E232D562B FOREIGN KEY (object_id) REFERENCES brainstyle_tips (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE brainstyle_tip_translations DROP FOREIGN KEY FK_7FD0F07E232D562B');
        $this->addSql('DROP TABLE brainstyle_characteristics');
        $this->addSql('DROP TABLE brainstyle_tip_translations');
        $this->addSql('DROP TABLE brainstyle_tips');
    }
}

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150702143815 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE brainstyle_tips ADD style VARCHAR(32) DEFAULT NULL');
        $this->addSql('ALTER TABLE brainstyle_tips ADD CONSTRAINT FK_BA7C308E33BDB86A FOREIGN KEY (style) REFERENCES test_styles (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_BA7C308E33BDB86A ON brainstyle_tips (style)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE brainstyle_tips DROP FOREIGN KEY FK_BA7C308E33BDB86A');
        $this->addSql('DROP INDEX IDX_BA7C308E33BDB86A ON brainstyle_tips');
        $this->addSql('ALTER TABLE brainstyle_tips DROP style');
    }
}

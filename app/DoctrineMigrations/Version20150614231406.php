<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150614231406 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE graph_images (id VARCHAR(32) NOT NULL, image_name VARCHAR(255) NOT NULL, updatedAt DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders (id VARCHAR(32) NOT NULL, balance_id VARCHAR(32) DEFAULT NULL, user_id VARCHAR(32) DEFAULT NULL, amount INT NOT NULL, status ENUM(\'0\', \'1\') NOT NULL COMMENT \'(DC2Type:OrderStatusType)\', created DATETIME NOT NULL, updated DATETIME NOT NULL, INDEX IDX_E52FFDEEAE91A3DD (balance_id), INDEX IDX_E52FFDEEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_test_result (id VARCHAR(32) NOT NULL, user VARCHAR(32) DEFAULT NULL, style VARCHAR(32) DEFAULT NULL, point INT NOT NULL, INDEX IDX_2DA80FA88D93D649 (user), INDEX IDX_2DA80FA833BDB86A (style), UNIQUE INDEX search_idx (style, user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE balances (id VARCHAR(32) NOT NULL, user_id VARCHAR(32) DEFAULT NULL, amount_users INT NOT NULL, UNIQUE INDEX UNIQ_41A7E40FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE projects_groups (id VARCHAR(32) NOT NULL, project_id VARCHAR(32) DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_3199A58F166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE projects (id VARCHAR(32) NOT NULL, owner_id VARCHAR(32) DEFAULT NULL, name VARCHAR(255) NOT NULL, team VARCHAR(255) NOT NULL, amount_new_users INT NOT NULL, hash VARCHAR(255) NOT NULL, INDEX IDX_5C93B3A47E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user (id VARCHAR(32) NOT NULL, main_style VARCHAR(32) DEFAULT NULL, username VARCHAR(255) NOT NULL, username_canonical VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, email_canonical VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, locked TINYINT(1) NOT NULL, expired TINYINT(1) NOT NULL, expires_at DATETIME DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', credentials_expired TINYINT(1) NOT NULL, credentials_expire_at DATETIME DEFAULT NULL, test_pass TINYINT(1) NOT NULL, first_name VARCHAR(512) NOT NULL, last_name VARCHAR(512) NOT NULL, gender VARCHAR(512) DEFAULT NULL, birth_year INT DEFAULT NULL, education_level VARCHAR(512) DEFAULT NULL, reason TEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', locale VARCHAR(512) DEFAULT NULL, company_name VARCHAR(512) DEFAULT NULL, UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), INDEX IDX_957A647990E10B2C (main_style), UNIQUE INDEX email_idx (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_projects (user_id VARCHAR(32) NOT NULL, project_id VARCHAR(32) NOT NULL, INDEX IDX_27D2987EA76ED395 (user_id), INDEX IDX_27D2987E166D1F9C (project_id), PRIMARY KEY(user_id, project_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_projects_groups (user_id VARCHAR(32) NOT NULL, project_group_id VARCHAR(32) NOT NULL, INDEX IDX_8AEC5A15A76ED395 (user_id), INDEX IDX_8AEC5A15C31A529C (project_group_id), PRIMARY KEY(user_id, project_group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE practitioner_user (student VARCHAR(32) NOT NULL, practitioner VARCHAR(32) NOT NULL, INDEX IDX_9D10B136B723AF33 (student), INDEX IDX_9D10B13617323CBC (practitioner), PRIMARY KEY(student, practitioner)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_question (id VARCHAR(32) NOT NULL, text LONGTEXT NOT NULL, hash VARCHAR(255) NOT NULL, order_type INT NOT NULL, answer_type VARCHAR(255) NOT NULL, min_available_result INT NOT NULL, UNIQUE INDEX UNIQ_23944218C12F6D3E (order_type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_results (id VARCHAR(32) NOT NULL, question_id VARCHAR(32) DEFAULT NULL, user_id VARCHAR(32) DEFAULT NULL, results LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\', INDEX IDX_43E230DC1E27F6BF (question_id), INDEX IDX_43E230DCA76ED395 (user_id), UNIQUE INDEX result_unq_idx (user_id, question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_answer_translations (id INT AUTO_INCREMENT NOT NULL, object_id VARCHAR(32) DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_E744E01232D562B (object_id), UNIQUE INDEX lookup_unique_idx (locale, object_id, field), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_question_translations (id INT AUTO_INCREMENT NOT NULL, object_id VARCHAR(32) DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_6314DEF3232D562B (object_id), UNIQUE INDEX lookup_unique_idx (locale, object_id, field), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_styles_translations (id INT AUTO_INCREMENT NOT NULL, object_id VARCHAR(32) DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_467BC161232D562B (object_id), UNIQUE INDEX lookup_unique_idx (locale, object_id, field), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_styles (id VARCHAR(32) NOT NULL, name VARCHAR(45) NOT NULL, UNIQUE INDEX UNIQ_2183FDDB5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_answer (id VARCHAR(32) NOT NULL, question_id VARCHAR(32) DEFAULT NULL, text LONGTEXT NOT NULL, result_id INT DEFAULT NULL, INDEX IDX_4D044D0B1E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEAE91A3DD FOREIGN KEY (balance_id) REFERENCES balances (id)');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_test_result ADD CONSTRAINT FK_2DA80FA88D93D649 FOREIGN KEY (user) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE user_test_result ADD CONSTRAINT FK_2DA80FA833BDB86A FOREIGN KEY (style) REFERENCES test_styles (id)');
        $this->addSql('ALTER TABLE balances ADD CONSTRAINT FK_41A7E40FA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE projects_groups ADD CONSTRAINT FK_3199A58F166D1F9C FOREIGN KEY (project_id) REFERENCES projects (id)');
        $this->addSql('ALTER TABLE projects ADD CONSTRAINT FK_5C93B3A47E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A647990E10B2C FOREIGN KEY (main_style) REFERENCES test_styles (id)');
        $this->addSql('ALTER TABLE users_projects ADD CONSTRAINT FK_27D2987EA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_projects ADD CONSTRAINT FK_27D2987E166D1F9C FOREIGN KEY (project_id) REFERENCES projects (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_projects_groups ADD CONSTRAINT FK_8AEC5A15A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE users_projects_groups ADD CONSTRAINT FK_8AEC5A15C31A529C FOREIGN KEY (project_group_id) REFERENCES projects_groups (id)');
        $this->addSql('ALTER TABLE practitioner_user ADD CONSTRAINT FK_9D10B136B723AF33 FOREIGN KEY (student) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE practitioner_user ADD CONSTRAINT FK_9D10B13617323CBC FOREIGN KEY (practitioner) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE test_results ADD CONSTRAINT FK_43E230DC1E27F6BF FOREIGN KEY (question_id) REFERENCES test_question (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE test_results ADD CONSTRAINT FK_43E230DCA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE test_answer_translations ADD CONSTRAINT FK_E744E01232D562B FOREIGN KEY (object_id) REFERENCES test_answer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE test_question_translations ADD CONSTRAINT FK_6314DEF3232D562B FOREIGN KEY (object_id) REFERENCES test_question (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE test_styles_translations ADD CONSTRAINT FK_467BC161232D562B FOREIGN KEY (object_id) REFERENCES test_styles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE test_answer ADD CONSTRAINT FK_4D044D0B1E27F6BF FOREIGN KEY (question_id) REFERENCES test_question (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEEAE91A3DD');
        $this->addSql('ALTER TABLE users_projects_groups DROP FOREIGN KEY FK_8AEC5A15C31A529C');
        $this->addSql('ALTER TABLE projects_groups DROP FOREIGN KEY FK_3199A58F166D1F9C');
        $this->addSql('ALTER TABLE users_projects DROP FOREIGN KEY FK_27D2987E166D1F9C');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEEA76ED395');
        $this->addSql('ALTER TABLE user_test_result DROP FOREIGN KEY FK_2DA80FA88D93D649');
        $this->addSql('ALTER TABLE balances DROP FOREIGN KEY FK_41A7E40FA76ED395');
        $this->addSql('ALTER TABLE projects DROP FOREIGN KEY FK_5C93B3A47E3C61F9');
        $this->addSql('ALTER TABLE users_projects DROP FOREIGN KEY FK_27D2987EA76ED395');
        $this->addSql('ALTER TABLE users_projects_groups DROP FOREIGN KEY FK_8AEC5A15A76ED395');
        $this->addSql('ALTER TABLE practitioner_user DROP FOREIGN KEY FK_9D10B136B723AF33');
        $this->addSql('ALTER TABLE practitioner_user DROP FOREIGN KEY FK_9D10B13617323CBC');
        $this->addSql('ALTER TABLE test_results DROP FOREIGN KEY FK_43E230DCA76ED395');
        $this->addSql('ALTER TABLE test_results DROP FOREIGN KEY FK_43E230DC1E27F6BF');
        $this->addSql('ALTER TABLE test_question_translations DROP FOREIGN KEY FK_6314DEF3232D562B');
        $this->addSql('ALTER TABLE test_answer DROP FOREIGN KEY FK_4D044D0B1E27F6BF');
        $this->addSql('ALTER TABLE user_test_result DROP FOREIGN KEY FK_2DA80FA833BDB86A');
        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A647990E10B2C');
        $this->addSql('ALTER TABLE test_styles_translations DROP FOREIGN KEY FK_467BC161232D562B');
        $this->addSql('ALTER TABLE test_answer_translations DROP FOREIGN KEY FK_E744E01232D562B');
        $this->addSql('DROP TABLE graph_images');
        $this->addSql('DROP TABLE orders');
        $this->addSql('DROP TABLE user_test_result');
        $this->addSql('DROP TABLE balances');
        $this->addSql('DROP TABLE projects_groups');
        $this->addSql('DROP TABLE projects');
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('DROP TABLE users_projects');
        $this->addSql('DROP TABLE users_projects_groups');
        $this->addSql('DROP TABLE practitioner_user');
        $this->addSql('DROP TABLE test_question');
        $this->addSql('DROP TABLE test_results');
        $this->addSql('DROP TABLE test_answer_translations');
        $this->addSql('DROP TABLE test_question_translations');
        $this->addSql('DROP TABLE test_styles_translations');
        $this->addSql('DROP TABLE test_styles');
        $this->addSql('DROP TABLE test_answer');
    }
}

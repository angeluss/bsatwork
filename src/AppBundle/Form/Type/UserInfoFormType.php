<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UserInfoFormType
 */
class UserInfoFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $years = [];
        for ($i = 1920; $i <= (date('Y') - 15); $i++) {
            $years[] = $i;
        }

        $builder->add('gender', 'choice', [
                'label' => 'userinfo.labels.gender',
                'choices' => [
                    'male' => 'male',
                    'female' => 'female',
                ],
                'required' => true,
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('birthYear', 'choice', [
                'label' => 'userinfo.labels.birthyear',
                'choices' => $years,
            ])
            ->add('educationLevel', 'choice', [
                'label'   => 'userinfo.labels.educationlevel',
                'choices' => [
                    'primary school' => 'primary school',
                    'junior high school' => 'junior high school',
                    'senior high school' => 'senior high school',
                    'junior college' => 'junior college',
                    'senior college' => 'senior college',
                    'Master of Arts (MA) or Master of Science (MS) or equivalent' => 'Master of Arts (MA) or Master of Science (MS) or equivalent',
                    'Doctoral thesis and degree (Phd)' => 'Doctoral thesis and degree (Phd)',
                ],
                'required' => true,
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('reason', 'choice', [
                'label'   => 'userinfo.labels.reason',
                'choices' => [
                    'to get an idea of the type of jobs that would suit me' => 'to get an idea of the type of jobs that would suit me',
                    'to gain more insight in myself' => 'to gain more insight in myself',
                    'to develop myself' => 'to develop myself',
                    'to prepare for a job interview or an assessment' => 'to prepare for a job interview or an assessment',
                ],
                'constraints' => [new NotBlank()],
                'required'    => true,
                'multiple'    => true,
                'expanded'    => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
        ]);
    }
    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'user_info';
    }
}

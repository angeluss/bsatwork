<?php

namespace AppBundle\Form\Type;


use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class UserCreateFormType
 */
class UserCreateFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('userType', 'choice', [
            'label'   => 'regform.usertype',
            'mapped'  => false,
            'choices' => User::getUserTypes(),
            'data'    => User::USER_TYPE_EMPLOYER,
        ])
            ->add('userType', 'choice', [
                'label'   => 'regform.usertype',
                'mapped'  => false,
                'choices' => User::getUserTypes(),
                'data'    => User::USER_TYPE_EMPLOYER,
            ])
            ->add('firstName', 'text', [
                'label'              => false,
                'attr'               => [
                    'placeholder' => 'regform.firstname',
                ],
                'translation_domain' => 'messages',
                'required' => true,
            ])
            ->add('lastName', 'text', [
                'label'              => false,
                'attr'               => [
                    'placeholder' => 'regform.lastname',
                ],
                'translation_domain' => 'messages',
                'required' => true,
            ])
            ->add('email', 'email', [
                'label'              => 'form.email',
                'attr'               => [
                    'placeholder' => 'form.email',
                ],
                'translation_domain' => 'FOSUserBundle',
            ])
            ->add('save', 'submit', [
                'label' => 'Create',
                'attr'  => [
                    'class' => 'button radius small expand bg-green',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
        ]);
    }
    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'user_create';
    }
}
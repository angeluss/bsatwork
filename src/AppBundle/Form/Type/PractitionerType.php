<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


/**
 * Class PractitionerType
 *
 * @package AppBundle\Form\Type
 */
class PractitionerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', 'text', [
                'translation_domain' => 'messages',
                'required' => true,
                'label' => 'regform.firstname'
            ])
            ->add('lastName', 'text', [
                'label' => 'regform.lastname',
                'translation_domain' => 'messages',
                'required' => true
            ])
            ->add('companyName', 'text', [
                'label' => 'regform.company_name',
                'translation_domain' => 'messages',
                'required' => true
            ])
            ->add('email', 'email', [
                'translation_domain' => 'messages',
                'required' => true,
                'label' => 'regform.email'
            ])
            ->add('plainPassword', 'repeated', [
                'type'            => 'password',
                'options'         => ['translation_domain' => 'FOSUserBundle'],
                'first_options'   => [
                    'label' => 'form.password',
                ],
                'second_options'  => [
                    'label' => 'form.password_confirmation',
                ],
                'invalid_message' => 'fos_user.password.mismatch',
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_practitioner';
    }
}

<?php
namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class RegistrationFormType
 * @package AppBundle\Form\Type
 */
class RegistrationFormType extends AbstractType
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('userType', 'choice', [
                'label'   => 'regform.usertype',
                'mapped'  => false,
                'choices' => User::getUserTypes(),
                'data'    => User::USER_TYPE_EMPLOYER,
            ])
            ->add('firstName', 'text', [
                'label'              => false,
                'attr'               => [
                    'placeholder' => 'regform.firstname'
                ],
                'translation_domain' => 'messages',
                'required' => true
            ])
            ->add('lastName', 'text', [
                'label'              => false,
                'attr'               => [
                    'placeholder' => 'regform.lastname'
                ],
                'translation_domain' => 'messages',
                'required' => true
            ])
            ->add('email', 'email', [
                'label'              => 'form.email',
                'attr'               => [
                    'placeholder' => 'form.email'
                ],
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('plainPassword', 'repeated', [
                'type'            => 'password',
                'options'         => ['translation_domain' => 'FOSUserBundle'],
                'first_options'   => [
                    'label' => false,
                    'attr'               => [
                        'placeholder' => 'form.password'
                    ],
                ],
                'second_options'  => [
                    'label' => false,
                    'attr'               => [
                        'placeholder' => 'form.password_confirmation'
                    ],
                ],
                'invalid_message' => 'fos_user.password.mismatch',
            ])
            ->add('locale', 'choice', [
                'label'   => false,
                'attr'               => [
                    'placeholder' => 'regform.locale'
                ],
                'choices' => [
                    'nl' => 'Dutch',
                    'en' => 'English'
                ],
                'data'    => 'nl'
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'intention'  => 'registration',
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'registration';
    }


    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }
}

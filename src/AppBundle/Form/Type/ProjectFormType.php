<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ProjectFormType
 *
 */
class ProjectFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', [
                'label'              => 'Company name',
                'translation_domain' => 'messages'
            ])
            ->add('team', 'text', [
                'label'              => 'Team name',
                'translation_domain' => 'messages'
            ])
            ->add('amountNewUsers', 'integer', [
                'attr' => [
                    'class' => 'positive-number', 'pattern' => '^[0-9]*$'
                ],
                'label'              => 'Amount of users that can register',
                'translation_domain' => 'messages'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Project'
        ]);
    }
    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'project';
    }
}

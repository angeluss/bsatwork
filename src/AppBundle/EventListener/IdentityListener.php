<?php

namespace AppBundle\EventListener;


use AppBundle\Entity\Balance;
use AppBundle\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class UserListener
 *
 * @package AppBundle\EventListener
 */
class IdentityListener
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (method_exists($entity, 'getId') && method_exists($entity, 'setId')) {
            if (!$entity->getId()) {
                $entity->setId(md5(uniqid() . mt_rand()));
            }
        }
    }
}
<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use AppBundle\Mailer\Mailer;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Mailer\MailerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Translation\Translator;

/**
 * Class RegistrationListener
 *
 * @package AppBundle\EventListener
 */
class RegistrationListener implements EventSubscriberInterface
{
    /**
     * @var Translator $translator
     */
    private $translator;

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var UrlGeneratorInterface
     */
    private $router;
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var array
     */
    private $locales;

    /**
     * @var TokenStorage $securityToken
     */
    private $securityToken;

    /**
     * @param EntityManager         $entityManager
     * @param UrlGeneratorInterface $router
     * @param MailerInterface       $mailer
     * @param array                 $locales
     * @param TokenStorage          $securityToken
     * @param Translator            $translator
     */
    public function __construct(
        EntityManager $entityManager,
        UrlGeneratorInterface $router,
        MailerInterface $mailer,
        $locales,
        TokenStorage $securityToken,
        Translator $translator
    ) {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->mailer = $mailer;
        $this->locales = $locales;
        $this->securityToken = $securityToken;
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialize',
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
        ];
    }

    /**
     * @param GetResponseUserEvent $event
     */
    public function onRegistrationInitialize(GetResponseUserEvent $event)
    {
        $project = $this->findProjectByHash($event->getRequest()->get('hash', ''));

        if (0 == $project->getAmountNewUsers()) {
            $event->setResponse(new RedirectResponse($this->router->generate('registration_error')));
        }
    }

    /**
     * @param FormEvent $event
     */
    public function onRegistrationSuccess(FormEvent $event)
    {
        /** @var User $user */
        $form = $event->getForm();
        $user = $form->getData();

        $userType = $form->get('userType')->getData();

        $project = $this->findProjectByHash($event->getRequest()->get('hash', ''));
        $project->addParticipants($user);
        $project->decreaseAmountNewUsers();

        if ($project->isUsersActivated()) {
            $user->setApproved(true);
        }

        $user->setEnabled(true);
        $user->addRole(User::getRoleByType($userType));
        $this->translator->setLocale($user->getLocale());

        if ($user->hasRole('ROLE_MANAGER') || $user->hasRole('ROLE_EMPLOYER')) {
            $this->mailer->sendWelcomeEmailMessage($user);
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->securityToken->setToken($token);
        }


        $event->setResponse(new RedirectResponse($this->router->generate('test_explanation', [
            '_locale' => $user->getLocale(),
        ])));
    }

    /**
     * @return array
     */
    public function getLocales()
    {
        return $this->locales;
    }

    /**
     * @param string $hash
     *
     * @return \AppBundle\Entity\Project
     */
    private function findProjectByHash($hash)
    {
        $project = $this->entityManager->getRepository('AppBundle:Project')->findOneBy(['hash' => $hash]);

        if (!$project) {
            throw new NotFoundHttpException('Project not found');
        }

        return $project;
    }
}

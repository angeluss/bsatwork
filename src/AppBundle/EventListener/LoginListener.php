<?php
namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

/**
 * Class LoginListener
 */
class LoginListener implements AuthenticationSuccessHandlerInterface
{
    protected $router;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @param Router       $router
     * @param TokenStorage $tokenStorage
     */
    public function __construct(Router $router, TokenStorage $tokenStorage)
    {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request        $request
     * @param TokenInterface $token
     *
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        /**
         * @var User $user
         */
        $user = $this->tokenStorage->getToken()->getUser();
        $url = 'dashboard_index';
        if (($user->hasRole('ROLE_MANAGER') || $user->hasRole('ROLE_EMPLOYER')) && (!$user->isApproved() || !$user->hasRole('ROLE_APPROVED'))) {
            if (!$user->isTestPass()) {
                $url = 'test_explanation';
            } else {
                $this->tokenStorage->setToken(null);
                $url = 'login_error';
            }
        }
        $response = new RedirectResponse($this->router->generate($url));

        return $response;
    }
}
<?php

namespace AppBundle\EventListener;


use AppBundle\Entity\Balance;
use AppBundle\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class UserListener
 *
 * @package AppBundle\EventListener
 */
class UserListener
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof User) {
            if ($entity->getBalance() == null) {
                $balance = new Balance();
                $balance->setUser($entity);
                $balance->setAmountUsers(0);
                $entity->setBalance($balance);
                $entityManager->persist($balance);
            }
        }
    }
}
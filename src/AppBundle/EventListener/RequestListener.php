<?php

namespace AppBundle\EventListener;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class RequestListener
 */
class RequestListener
{
    protected $router;

    protected $reader;
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @param TokenStorage          $tokenStorage
     * @param UrlGeneratorInterface $router
     */
    public function __construct(TokenStorage $tokenStorage, UrlGeneratorInterface $router)
    {
        $this->router = $router;
        $this->reader = new AnnotationReader();
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $calledAction = null;
        $controllerParams = explode('::', $request->attributes->get('_controller'));
        if (count($controllerParams) < 2) {
            return;
        }

        $controller = new $controllerParams[0]();
        $object = new \ReflectionObject($controller);
        $method = $object->getMethod($controllerParams[1]);

        foreach ($this->reader->getMethodAnnotations($method) as $configuration) {
            if (isset($configuration->access)) {
                $user = $this->tokenStorage->getToken()->getUser();
                if ('anon.' === $user) {
                    return;
                }
                if (($user->hasRole('ROLE_MANAGER') || $user->hasRole('ROLE_EMPLOYER'))) {
                    if (!$user->isTestPass()) {
                        $event->setResponse(new RedirectResponse($this->router->generate('test_explanation')));
                    }
                }
            }
        }
    }
}
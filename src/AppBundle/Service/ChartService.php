<?php

namespace AppBundle\Service;

use AppBundle\Entity\ChartImage;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use TestBundle\Entity\Style;

/**
 * Class ChartService
 *
 * @package AppBundle\Service
 */
class ChartService
{

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * @var $kernelDir
     */
    private $kernelDir;

    /**
     * Constructor
     *
     * @param  string $kernelDir
     * @param  EntityManager $entityManager
     */
    public function __construct($kernelDir, EntityManager $entityManager)
    {
        $this->kernelDir = $kernelDir;
        $this->entityManager = $entityManager;
    }

    /**
     * Check if graph with such data already generated
     *
     * @param string $fileName
     *
     * @return bool
     */
    public function hasGraph($fileName)
    {
        $chart = $this->entityManager->getRepository('AppBundle:ChartImage')->findOneBy([
            'imageName' => $fileName
        ]);

        return $chart;
    }

    /**
     * Get graph
     * @param array $data
     *
     * @return ChartImage
     */
    public function getGraph($data)
    {
        $fileName = '';
        foreach ($data as $point) {
            $fileName .= $point;
        }

        $background = imagecreatefrompng($this->kernelDir . '/../src/AppBundle/Resources/public/images/model.png');
        $imHeight = imagesy($background);
        $imWidth = imagesx($background);

        $chartImage = $this->hasGraph($fileName);
        if (!$chartImage) {
            $centerX = ($imWidth / 2) + 1;
            $centerY = $imHeight / 2;

            imagesetstyle($background, [0xbbbbbb]);

            imagesetthickness($background, 8);

            imageline($background, $centerX, $centerY - ((385 / 100) * $data[Style::CONCEPTUALISER]),
                $centerX + ((270 / 100) * $data[Style::INNOVATOR]), $centerY - ((270 / 100) * $data[Style::INNOVATOR]), IMG_COLOR_STYLED);
            imageline($background, $centerX + ((270 / 100) * $data[Style::INNOVATOR]), $centerY - ((270 / 100) * $data[Style::INNOVATOR]),
                $centerX + ((385 / 100) * $data[Style::CONNECTOR]), $centerY, IMG_COLOR_STYLED);
            imageline($background, $centerX + ((385 / 100) * $data[Style::CONNECTOR]), $centerY,
                $centerX + ((270 / 100) * $data[Style::HUMANIZER]), $centerY + ((270 / 100) * $data[Style::HUMANIZER]), IMG_COLOR_STYLED);
            imageline($background, $centerX + ((270 / 100) * $data[Style::HUMANIZER]), $centerY + ((270 / 100) * $data[Style::HUMANIZER]),
                $centerX, $centerY + ((385 / 100) * $data[Style::ORGANIZER]), IMG_COLOR_STYLED);
            imageline($background, $centerX, $centerY + ((385 / 100) * $data[Style::ORGANIZER]),
                $centerX - ((270 / 100) * $data[Style::IMPLEMENTER]), $centerY + ((270 / 100) * $data[Style::IMPLEMENTER]), IMG_COLOR_STYLED);
            imageline($background, $centerX - ((270 / 100) * $data[Style::IMPLEMENTER]), $centerY + ((270 / 100) * $data[Style::IMPLEMENTER]),
                $centerX - ((385 / 100) * $data[Style::SYSTEMISER]), $centerY, IMG_COLOR_STYLED);
            imageline($background, $centerX - ((385 / 100) * $data[Style::SYSTEMISER]), $centerY,
                $centerX - ((270 / 100) * $data[Style::ANALYZER]), $centerY - ((270 / 100) * $data[Style::ANALYZER]), IMG_COLOR_STYLED);
            imageline($background, $centerX - ((270 / 100) * $data[Style::ANALYZER]), $centerY - ((270 / 100) * $data[Style::ANALYZER]),
                $centerX, $centerY - ((385 / 100) * $data[Style::CONCEPTUALISER]), IMG_COLOR_STYLED);


            $tmpName = tempnam(sys_get_temp_dir(), 'chart_');

            $chartImage = new ChartImage();

            imagejpeg($background, $tmpName);

            $chartImage->setImageFile(new UploadedFile($tmpName, $fileName . '.png', null, null, null, true));

            $this->entityManager->persist($chartImage);
            $this->entityManager->flush();
        }

        return $chartImage;
    }
}
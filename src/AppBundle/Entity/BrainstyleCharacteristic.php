<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BrainstyleCharacteristic
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\BrainstyleCharacteristicRepository")
 * @ORM\Table(name="brainstyle_characteristics")
 */
class BrainstyleCharacteristic
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="text", type="text")
     */
    private $text;


    /**
     * @ORM\ManyToOne(targetEntity="TestBundle\Entity\Style", inversedBy="characteristics")
     * @ORM\JoinColumn(name="style", referencedColumnName="id", onDelete="CASCADE")
     */
    private $style;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=512, nullable=true)
     * @Assert\Choice(choices = {"nl", "en"}, message = "Choose a valid locale.")
     */
    private $locale;

    public function __construct()
    {
        $this->id = md5(uniqid() . mt_rand());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getText();
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @param mixed $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

}
<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use TestBundle\Entity\Style;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class User
 *
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="fos_user", uniqueConstraints={@ORM\UniqueConstraint(name="email_idx", columns={"email"})})
 * @UniqueEntity("email")
 */
class User extends BaseUser
{
    const USER_TYPE_EMPLOYER = 1;
    const USER_TYPE_MANAGER = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @var string $plainPassword
     *
     * @Assert\Length(
     *      min=6,
     *      minMessage="You password must be 6 characters length"
     * )
     */
    protected $plainPassword;

    /**
     * @var Balance|null
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Balance", mappedBy="user")
     */
    private $balance;

    /**
     * @var Project[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProjectGroup", mappedBy="owner", orphanRemoval=true)
     */
    private $mySubgroups;

    /**
     * @var Project[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Project", mappedBy="owner", orphanRemoval=true)
     */
    private $myProjects;

    /**
     * @var Project[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project", inversedBy="participants", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="users_projects",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $projects;

    /**
     * @var Project[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\ProjectGroup", inversedBy="participants", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="users_projects_groups",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="project_group_id", referencedColumnName="id")}
     *      )
     */
    private $projectGroups;

    /**
     * @var Order[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order", mappedBy="user", cascade={"persist"})
     */
    private $orders;

    /**
     * @var bool
     *
     * @ORM\Column(name="test_pass", type="boolean")
     */
    private $testPass;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=512)
     *
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=512)
     *
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=512, nullable=true)
     *
     * @Assert\Choice(choices = {"male", "female"}, message = "Choose a valid gender.")
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="birth_year", type="integer", nullable=true)
     *
     * @Assert\Type(
     *     type="integer",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $birthYear;

    /**
     * @var string
     *
     * @ORM\Column(name="education_level", type="string", length=512, nullable=true)
     */
    private $educationLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="array", length=512, nullable=true)
     */
    private $reason;

    /**
     * @var bool
     *
     * @ORM\Column(name="approved", type="boolean")
     */
    private $approved;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=512, nullable=true)
     *
     * @Assert\Choice(choices = {"nl", "en"}, message = "Choose a valid locale.")
     */
    private $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=512, nullable=true)
     */
    private $companyName;

    /**
     * @var Style
     *
     * @ORM\ManyToOne(targetEntity="TestBundle\Entity\Style")
     * @ORM\JoinColumn(name="main_style", referencedColumnName="id")
     */
    private $mainStyle;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserTestResult", mappedBy="user")
     **/
    private $results;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="practitioners")
     **/
    private $students;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="students")
     * @ORM\JoinTable(name="practitioner_user",
     *      joinColumns={@ORM\JoinColumn(name="student", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="practitioner", referencedColumnName="id")}
     *      )
     **/
    private $practitioners;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->id = md5(uniqid().mt_rand());
        $this->testPass = false;
        $this->approved = false;
        $this->myProjects = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->projectGroups = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->results = new ArrayCollection();
        $this->students = new ArrayCollection();
        $this->practitioners = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        parent::setEmail($email);
        $this->setUsername($email);
    }

    /**
     * @return Balance
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @return int
     */
    public function getAvailableUserBalance()
    {
        $availableUsers = 0;
        if ($this->balance) {
            $availableUsers = $this->balance->getAmountUsers();
        }

        return $availableUsers;
    }

    /**
     * @param Balance $balance
     *
     * @return User
     */
    public function setBalance(Balance $balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @return ProjectGroup[]|ArrayCollection
     */
    public function getMySubgroups()
    {
        return $this->mySubgroups;
    }

    /**
     * @param ProjectGroup[]|ArrayCollection $myMySubgroups
     *
     * @return User
     */
    public function setMySubgroups($myMySubgroups)
    {
        $this->mySubgroups = $myMySubgroups;

        return $this;
    }

    /**
     * @param ProjectGroup $projectGroup
     *
     * @return $this
     */
    public function addMyProjectGroup(ProjectGroup $projectGroup)
    {
        if (!$this->mySubgroups->contains($projectGroup)) {
            $this->mySubgroups->add($projectGroup);
            $projectGroup->setOwner($this);
        }

        return $this;
    }

    /**
     * @param ProjectGroup $projectGroup
     *
     * @return $this
     */
    public function removeMyProjectGroup(ProjectGroup $projectGroup)
    {
        if ($this->mySubgroups->contains($projectGroup)) {
            $this->mySubgroups->removeElement($projectGroup);
        }

        return $this;
    }

    /**
     * @return Project[]|ArrayCollection
     */
    public function getMyProjects()
    {
        return $this->myProjects;
    }

    /**
     * @param Project[]|ArrayCollection $myProjects
     *
     * @return User
     */
    public function setMyProjects($myProjects)
    {
        $this->myProjects = $myProjects;

        return $this;
    }

    /**
     * @param Project $project
     *
     * @return $this
     */
    public function addMyProject(Project $project)
    {
        if (!$this->myProjects->contains($project)) {
            $this->myProjects->add($project);
            $project->setOwner($this);
        }

        return $this;
    }

    /**
     * @param Project $project
     *
     * @return $this
     */
    public function removeMyProject(Project $project)
    {
        if ($this->myProjects->contains($project)) {
            $this->myProjects->removeElement($project);
        }

        return $this;
    }

    /**
     * @return Project[]|ArrayCollection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @param Project[]|ArrayCollection $projects
     *
     * @return User
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;

        return $this;
    }

    /**
     * @param Project $project
     *
     * @return $this
     */
    public function addProject(Project $project)
    {
        if (!$this->projects->contains($project)) {
            $this->projects->add($project);
        }

        return $this;
    }

    /**
     * @param Project $project
     *
     * @return $this
     */
    public function removeProject(Project $project)
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
        }

        return $this;
    }

    /**
     * @return ProjectGroup[]|ArrayCollection
     */
    public function getProjectGroups()
    {
        return $this->projectGroups;
    }

    /**
     * @param ProjectGroup[]|ArrayCollection $projectGroups
     *
     * @return User
     */
    public function setProjectGroups($projectGroups)
    {
        $this->projectGroups = $projectGroups;

        return $this;
    }

    /**
     * @param ProjectGroup $projectGroup
     *
     * @return $this
     */
    public function addProjectGroup(ProjectGroup $projectGroup)
    {
        if (!$this->projectGroups->contains($projectGroup)) {
            $this->projectGroups->add($projectGroup);
        }

        return $this;
    }

    /**
     * @param ProjectGroup $projectGroup
     *
     * @return $this
     */
    public function removeProjectGroup(ProjectGroup $projectGroup)
    {
        if ($this->projectGroups->contains($projectGroup)) {
            $this->projectGroups->removeElement($projectGroup);
        }

        return $this;
    }

    /**
     * @return Order[]|ArrayCollection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param Order[]|ArrayCollection $orders
     *
     * @return User
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;

        return $this;
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function addOrder(Order $order)
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->setUser($this);
            $this->getBalance()->addOrder($order);
        }

        return $this;
    }

    /**
     * @return boolean
     */
    public function isTestPass()
    {
        return $this->testPass;
    }

    /**
     * @return boolean
     */
    public function getTestPass()
    {
        return $this->testPass;
    }

    /**
     * @param boolean $testPass
     */
    public function setTestPass($testPass)
    {
        $this->testPass = $testPass;
    }

    /**
     * @return boolean
     */
    public function isApproved()
    {
        return $this->approved;
    }

    /**
     * @param boolean $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
        if ($approved) {
            $this->addRole('ROLE_APPROVED');
        } else {
            $this->removeRole('ROLE_APPROVED');
        }
    }

    /**
     * @return array
     */
    public static function getUserTypes()
    {
        return [
            self::USER_TYPE_EMPLOYER => 'Employee',
            self::USER_TYPE_MANAGER => 'Manager',
        ];
    }

    /**
     * @param int $type
     *
     * @return string
     */
    public static function getRoleByType($type)
    {
        $typeRoles = [
            self::USER_TYPE_EMPLOYER => 'ROLE_EMPLOYER',
            self::USER_TYPE_MANAGER => 'ROLE_MANAGER',
        ];
        if (!array_key_exists($type, $typeRoles)) {
            throw new \InvalidArgumentException(sprintf('User type value "%s" invalid', $typeRoles));
        }

        return $typeRoles[$type];
    }

    /**
     * @return Style
     */
    public function getMainStyle()
    {
        return $this->mainStyle;
    }

    /**
     * @param Style $mainStyle
     */
    public function setMainStyle($mainStyle)
    {
        $this->mainStyle = $mainStyle;
    }

    /**
     * @return null|string
     */
    public function getMainStyleName()
    {
        if ($this->mainStyle instanceof Style) {
            return $this->mainStyle->getName();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getProjectGroupNames()
    {
        $names = [];
        /** @var ProjectGroup $projectGroup */
        foreach ($this->projectGroups as $projectGroup) {
            $names[] = $projectGroup->getName();
        }

        return implode(', ', $names);
    }

    /**
     * @return string
     */
    public function getProjectNames()
    {
        $names = [];
        /** @var Project $project */
        foreach ($this->projects as $project) {
            $names[] = $project->getName();
        }
        $names = array_unique($names);

        return implode(', ', $names);
    }

    /**
     * @return string
     */
    public function getTeamNames()
    {
        $names = [];
        /** @var Project $project */
        foreach ($this->projects as $project) {
            $names[] = $project->getTeam();
        }

        return implode(', ', $names);
    }

    /**
     * @return string
     */
    public function getUserType()
    {
        $userType = implode(', ', $this->getRoles());
        if ($this->hasRole('ROLE_EMPLOYER')) {
            $userType = 'Employee';
        }
        if ($this->hasRole('ROLE_MANAGER')) {
            $userType = 'Manager';
        }

        return $userType;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getBirthYear()
    {
        return $this->birthYear;
    }

    /**
     * @param string $birthYear
     */
    public function setBirthYear($birthYear)
    {
        $this->birthYear = $birthYear;
    }

    /**
     * @return string
     */
    public function getEducationLevel()
    {
        return $this->educationLevel;
    }

    /**
     * @param string $educationLevel
     */
    public function setEducationLevel($educationLevel)
    {
        $this->educationLevel = $educationLevel;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return ArrayCollection
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param ArrayCollection $results
     */
    public function setResults($results)
    {
        $this->results = $results;
    }

    /**
     * @return ArrayCollection
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * @param ArrayCollection $students
     */
    public function setStudents(ArrayCollection $students)
    {
        $this->students = $students;
    }

    /**
     * @param User $student
     */
    public function addStudents(User $student)
    {
        if (!$this->students->contains($student)) {
            $this->students->add($student);
        }
    }

    /**
     * @param User $student
     */
    public function removeStudent(User $student)
    {
        if ($this->students->contains($student)) {
            $this->students->removeElement($student);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getPractitioners()
    {
        return $this->practitioners;
    }

    /**
     * @param ArrayCollection $practitioners
     */
    public function setPractitioners(ArrayCollection $practitioners)
    {
        $this->practitioners = $practitioners;
    }

    /**
     * @param User $practitioner
     */
    public function addPractitioners(User $practitioner)
    {
        if (!$this->practitioners->contains($practitioner)) {
            $this->practitioners->add($practitioner);
        }
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param int $amount
     *
     * @throws \AppBundle\Exception\NegativeBalanceException
     */
    public function addBalanceUserAmount($amount)
    {
        $userAmount = $this->balance->getAmountUsers();
        $this->balance->setAmountUsers($userAmount + $amount);
    }

    /**
     * @param int $amount
     *
     * @throws \AppBundle\Exception\NegativeBalanceException
     */
    public function setBalanceUserAmount($amount)
    {
        $this->balance->setAmountUsers($amount);
    }
}

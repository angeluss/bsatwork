<?php

namespace AppBundle\Entity\Repository;


use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Exception\ProjectLimitUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * Class UserRepository
 *
 * @package AppBundle\Entity\Repository
 */
class ProjectRepository extends EntityRepository
{

    /**
     * @param User  $user
     * @param array $filters
     *
     * @return array|mixed
     */
    public function getProjectsByUser(User $user, array $filters)
    {
        $result = [];
        $fields = ['p.name', 'p.team', 'userCount'];
        $qb = $this->createQueryBuilder('p');
        $qb->select($fields[0], $fields[1], 'COUNT(DISTINCT up) as userCount', 'p.id as DT_RowId');
        $qb->leftJoin('p.participants', 'up');
        $qb = $this->makeFilters($qb, $filters, $fields, false);
        $qb->groupBy('p.id');

        if ($user->hasRole('ROLE_ADMIN')) {
            $result = $qb->getQuery()->execute();
        }

        if ($user->hasRole('ROLE_PRACTITIONER')) {
            $qb->andWhere('p.owner = :owner')
                ->setParameter('owner', $user);
            $result = $qb->getQuery()->execute();

        }

        return $result;
    }

    /**
     * @param User  $user
     * @param array $filters
     *
     * @return int|mixed
     */
    public function getCountByUser(User $user, $filters)
    {
        $result = 0;
        $fields = ['p.name', 'p.team', ''];
        unset($filters['start']);
        unset($filters['length']);
        $qb = $this->createQueryBuilder('p');
        $qb->select('COUNT(DISTINCT p)');
        $qb->leftJoin('p.participants', 'up');
        $qb = $this->makeFilters($qb, $filters, $fields, false);

        if ($user->hasRole('ROLE_ADMIN')) {
            $result = $qb->getQuery()->getSingleScalarResult();
        }

        if ($user->hasRole('ROLE_PRACTITIONER')) {
            $qb->andWhere('p.owner = :owner')
                ->setParameter('owner', $user);
            $result = $qb->getQuery()->getSingleScalarResult();
        }

        return $result;
    }

    /**
     * @param QueryBuilder $qb
     * @param array        $filters
     * @param array        $fields
     * @param bool         $dimensions
     *
     * @return QueryBuilder
     */
    private function makeFilters(QueryBuilder $qb, array $filters, array $fields, $dimensions)
    {
        if (isset($filters['order']) && is_array($filters['order'])) {
            foreach ($filters['order'] as $sortedField) {
                if (!empty($fields[$sortedField['column']])) {
                    $qb->addOrderBy($fields[$sortedField['column']], $sortedField['dir']);
                }
            }
        }

        if (isset($filters['start'])) {
            $qb->setFirstResult($filters['start']);
        }
        if (isset($filters['length'])) {
            $qb->setMaxResults($filters['length']);
        }
        if (!empty($filters['search']['value'])) {
            $qb->where('p.name LIKE :company_name OR p.team LIKE :team');
            $qb->setParameter('company_name', $filters['search']['value'] . '%')
                ->setParameter('team', $filters['search']['value'] . '%');
            if($dimensions) {
                $qb->andWhere('OR u.firstName LIKE :firstName OR u.lastName LIKE :lastName')
                   ->setParameters([
                    'firstName' => $filters['search']['value'] . '%',
                    'lastName', $filters['search']['value'] . '%'
                ]);
            }
        }

        return $qb;
    }

    /**
     * @param array   $userIds
     * @param Project $project
     *
     * @throws ProjectLimitUserException
     * @throws \Doctrine\ORM\ORMException
     */
    public function addUser(array $userIds, Project $project)
    {
        $em = $this->getEntityManager();
        foreach ($userIds as $id) {
            $ref = $em->getReference('AppBundle:User', $id);
            $project->addParticipants($ref);
            $project->getOwner()->addStudents($ref);
        }
        $em->flush();
    }

    /**
     * @param array   $userIds
     * @param Project $project
     *
     * @throws ProjectLimitUserException
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteUser(array $userIds, Project $project)
    {
        $em = $this->getEntityManager();
        foreach ($userIds as $id) {
            $user = $em->getReference('AppBundle:User', $id);
            $project->removeParticipants($user);
            $project->getOwner()->removeStudent($user);
        }
        $em->flush();
    }

    /**
     * @param int    $projectId
     * @param string $resultType
     *
     * @return array
     */
    public function getMainStyleByTeam($projectId, $resultType = 'default')
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('ROUND(AVG(ur.point),0) as avg_point', 'st.name')
            ->from('AppBundle:UserTestResult', 'ur')
            ->join('ur.style', 'st')
            ->join('ur.user', 'u')
            ->join('u.projects', 'pg')
            ->where('u.testPass = :testPass')
            ->andWhere('pg.id = :project')
            ->groupBy('st.name')
            ->orderBy('st.orderType')
            ->setParameter('project', $projectId)
            ->setParameter('testPass', true);

        $result = [];
        if ($resultType == 'formatted') {
            $projectStylePoints = $qb->getQuery()->getArrayResult();
            foreach ($projectStylePoints as $stylePoint) {
                $result[$stylePoint['name']] = $stylePoint['avg_point'];
            }
        } else {
            $result = $qb->getQuery()->getArrayResult();
        }

        return $result;

    }

    /**
     * @param int   $projectId
     * @param array $filters
     *
     * @return array
     */
    public function getDimensions($projectId, array $filters)
    {
        $fields = [
            'user_name',
            '',
            'style_1',
            'style_2',
            'style_3',
            'style_4',
            'style_5',
            'style_6',
            'style_7',
            'style_8',
            'style_name'
        ];
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('CONCAT_WS(\' \',u.firstName, u.lastName) AS user_name', 'p.name AS company_name',
                'ROUND(SUM(IF (st.orderType = 1, ur.point, 0))) as style_1',
                'ROUND(SUM(IF (st.orderType = 2, ur.point, 0))) as style_2',
                'ROUND(SUM(IF (st.orderType = 3, ur.point, 0))) as style_3',
                'ROUND(SUM(IF (st.orderType = 4, ur.point, 0))) as style_4',
                'ROUND(SUM(IF (st.orderType = 5, ur.point, 0))) as style_5',
                'ROUND(SUM(IF (st.orderType = 6, ur.point, 0))) as style_6',
                'ROUND(SUM(IF (st.orderType = 7, ur.point, 0))) as style_7',
                'ROUND(SUM(IF (st.orderType = 8, ur.point, 0))) as style_8',
                'ust.name AS style_name'

            )
            ->from('AppBundle:UserTestResult', 'ur')
            ->join('ur.user', 'u')
            ->join('u.mainStyle', 'ust')
            ->join('u.projects', 'p')
            ->join('ur.style', 'st');
        $this->makeFilters($qb, $filters, $fields, true);
        $qb->andWhere('u.testPass = :testPass')
            ->andWhere('p.id = :project')
            ->groupBy('u.id')
            ->addGroupBy('ur.user')
            ->setParameter('project', $projectId)
            ->setParameter('testPass', true);


        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param int   $projectId
     * @param array $filters
     *
     * @return array
     */
    public function getDimensionsUserCount($projectId, array $filters)
    {
        $fields = [];
        unset($filters['start']);
        unset($filters['length']);
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(DISTINCT ur.user)')
            ->from('AppBundle:UserTestResult', 'ur')
            ->join('ur.user', 'u')
            ->join('u.mainStyle', 'ust')
            ->join('u.projects', 'p');
        $this->makeFilters($qb, $filters, $fields, true);

        $qb->andWhere('u.testPass = :testPass')
            ->andWhere('p.id = :project')
            ->setParameter('project', $projectId)
            ->setParameter('testPass', true);


        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = 0;
        }

        return $result;
    }
}
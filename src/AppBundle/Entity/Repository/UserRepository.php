<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectGroup;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * Class UserRepository
 *
 * @package AppBundle\Entity\Repository
 */
class UserRepository extends EntityRepository
{

    /**
     * @param User  $user
     * @param array $filters
     *
     * @return array|mixed
     */
    public function getRelatedUsers(User $user, array $filters)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->leftJoin('u.projects', 'up');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $fields = ['up.name', 'up.team', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle'];
        $qb = $this->makeFilters($qb, $filters, $fields);
        $qb->groupBy('u.id');
        if ($user->hasRole('ROLE_ADMIN')) {
            return $qb->getQuery()->execute();
        }
        if ($user->hasRole('ROLE_PRACTITIONER')) {
            $qb->innerJoin('u.practitioners', 'upr', 'WITH', 'upr.id = :practitioner');
            $qb->setParameter('practitioner', $user);

            return $qb->getQuery()->execute();
        }

        return [];
    }

    /**
     * @param User  $user
     * @param array $filters
     *
     * @return int|mixed
     */
    public function getRelatedUsersCount(User $user, $filters)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select('COUNT(u)');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $fields = ['', '', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle'];
        unset($filters['start']);
        unset($filters['length']);
        $qb = $this->makeFilters($qb, $filters, $fields);
        $result = [];

        if ($user->hasRole('ROLE_ADMIN')) {
            try {
                $result = $qb->getQuery()->getSingleScalarResult();
            } catch (NoResultException $e) {
                $result = 0;
            }

        }
        if ($user->hasRole('ROLE_PRACTITIONER')) {
            $qb->innerJoin('u.practitioners', 'upr', 'WITH', 'upr.id = :practitioner');
            $qb->setParameter('practitioner', $user);

            try {
                $result = $qb->getQuery()->getSingleScalarResult();
            } catch (NoResultException $e) {
                $result = 0;
            }
        }

        return $result;
    }

    /**
     * @param array $filters
     *
     * @return array|mixed
     */
    public function getUsersGeneralInfo(array $filters)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->eq('u.testPass', ':testPass'));
        $qb->setParameter('testPass', true);
        $fields = ['u.firstName', 'u.lastName'];
        $qb = $this->makeFilters($qb, $filters, $fields);

        return $qb->getQuery()->execute();
    }

    /**
     * @param array $filters
     *
     * @return int|mixed
     */
    public function getUsersGeneralInfoCount($filters)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select('COUNT(u)');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->eq('u.testPass', ':testPass'));
        $qb->setParameter('testPass', true);
        $fields = ['u.firstName', 'u.lastName'];
        unset($filters['start']);
        unset($filters['length']);
        $qb = $this->makeFilters($qb, $filters, $fields);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param array        $filters
     *
     * @return array|mixed
     */
    public function getRelatedUsersBySubgroup(ProjectGroup $projectGroup, array $filters)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->leftJoin('u.projects', 'up');
        $qb->innerJoin('u.projectGroups', 'pg');
        $qb->where('pg.id = :projectGroup');
        $qb->setParameter('projectGroup', $projectGroup->getId());
        $fields = ['up.name', 'up.team', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle'];
        $qb = $this->makeFilters($qb, $filters, $fields);
        $qb->groupBy('u.id');

        return $qb->getQuery()->execute();
    }

    /**
     * @param array $filters
     *
     * @return array|mixed
     */
    public function getPractitionerUsers(array $filters)
    {
        $qb = $this->createQueryBuilder('u');
        $fields = ['u.firstName', 'u.lastName', 'u.companyName', 'projectCount', 'usersCount'];
        $qb->select($fields[0], $fields[1], $fields[2], 'COUNT(DISTINCT up) as projectCount',
            'COUNT(DISTINCT st) as usersCount', 'u.id as DT_RowId');
        $qb->leftJoin('u.myProjects', 'up');
        $qb->leftJoin('u.students', 'st');
        $qb->where('u.roles LIKE \'%ROLE_PRACTITIONER%\' ');
        $qb->groupBy('u.id');
        $qb = $this->makeFilters($qb, $filters, $fields, true);

        return $qb->getQuery()->execute();
    }

    /**
     * @param array $filters
     *
     * @return mixed
     */
    public function getCountPractitionerUsers(array $filters)
    {
        $fields = ['u.firstName', 'u.lastName', 'u.companyName', '', ''];
        unset($filters['start']);
        unset($filters['length']);
        $qb = $this->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->where('u.roles LIKE \'%ROLE_PRACTITIONER%\' ');
        $qb = $this->makeFilters($qb, $filters, $fields, true);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Project $project
     * @param User    $user
     * @param array   $filters
     *
     * @return mixed
     */
    public function getAvailableUserToProject(Project $project, User $user, array $filters)
    {
        $qbUser = $this->createQueryBuilder('us')
            ->join('us.projects', 'usp')
            ->where('usp.id = :project');

        $fields = ['up.name', 'up.team', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle'];
        $qb = $this->createQueryBuilder('u');
        $qb->leftjoin('u.projects', 'up');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->notIn('u.id', $qbUser->getDQL()));
        $qb->setParameter('project', $project->getId());
        $qb->groupBy('u.id');
        if (!$user->hasRole('ROLE_ADMIN')) {
            $qb->join('u.practitioners', 'pract');
            $qb->andWhere('pract.id = :practitioner');
            $qb->setParameter('practitioner', $project->getOwner()->getId());
        }

        $qb = $this->makeFilters($qb, $filters, $fields);


        return $qb->getQuery()->execute();
    }

    /**
     * @param Project $project
     * @param array   $filters
     *
     * @return mixed
     */
    public function getProjectUsers(Project $project, array $filters)
    {
        $qbUser = $this->createQueryBuilder('us')
            ->join('us.projects', 'usp')
            ->where('usp.id = :project');

        $fields = ['u.roles', 'u.firstName', 'u.lastName', 'u.pg.name', 'u.mainStyle', ''];
        $qb = $this->createQueryBuilder('u');
        $qb->leftjoin('u.projects', 'up');
        $qb->leftJoin('u.projectGroups', 'pg');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->in('u.id', $qbUser->getDQL()));
        $qb->groupBy('u.id');
        $qb->setParameter('project', $project->getId());
        $qb = $this->makeFilters($qb, $filters, $fields);


        return $qb->getQuery()->execute();
    }

    /**
     * @param Project $project
     * @param array   $filters
     *
     * @return mixed
     */
    public function getProjectUsersCount(Project $project, array $filters)
    {
        unset($filters['start']);
        unset($filters['length']);
        $qbUser = $this->createQueryBuilder('us')
            ->join('us.projects', 'usp')
            ->where('usp.id = :project');

        $fields = ['u.roles', 'u.firstName', 'u.lastName', 'u.pg.name', 'u.mainStyle', ''];
        $qb = $this->createQueryBuilder('u');
        $qb->select('COUNT(u.id)');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->in('u.id', $qbUser->getDQL()));
        $qb->setParameter('project', $project->getId());
        $qb = $this->makeFilters($qb, $filters, $fields);


        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = 0;
        }

        return $result;
    }

    /**
     * @param Project $project
     * @param array   $filters
     *
     * @return mixed
     */
    public function getProjectNotActivatedUsers(Project $project, array $filters)
    {
        $qbUser = $this->createQueryBuilder('us')
            ->join('us.projects', 'usp')
            ->where('usp.id = :project');

        $qb = $this->createQueryBuilder('u');
        $qb->leftjoin('u.projects', 'up');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->in('u.id', $qbUser->getDQL()));
        $qb->andWhere($qb->expr()->eq('u.approved', ':approve'));
        $qb->andWhere($qb->expr()->eq('u.testPass', ':testPass'));
        $qb->groupBy('u.id');
        $qb->setParameters([
            'project' => $project->getId(),
            'approve' => false,
            'testPass' => true,
        ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param User         $user
     * @param array        $filters
     *
     * @return mixed
     */
    public function getAvailableUserToProjectGroup(ProjectGroup $projectGroup, User $user, array $filters)
    {
        $qbUser = $this->createQueryBuilder('us')
            ->join('us.projectGroups', 'uspg')
            ->where('uspg.id = :projectGroup');

        $fields = ['up.name', 'up.team', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle'];
        $qb = $this->createQueryBuilder('u');
        $qb->join('u.projects', 'up');
        $qb->join('u.practitioners', 'upr');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->eq('up.id', ':project'));
        $qb->andWhere($qb->expr()->eq('upr.id', ':practitioner'));
        $qb->andWhere($qb->expr()->notIn('u.id', $qbUser->getDQL()));
        $qb->groupBy('u.id');
        $qb->setParameters([
            'project'      => $projectGroup->getProject(),
            'projectGroup' => $projectGroup,
            'practitioner' => $user,
        ]);
        $qb = $this->makeFilters($qb, $filters, $fields);


        return $qb->getQuery()->execute();
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param array        $filters
     *
     * @return mixed
     */
    public function getUserProjectGroup(ProjectGroup $projectGroup, array $filters)
    {
        $fields = ['up.name', 'up.team', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle'];
        $qb = $this->createQueryBuilder('u');
        $qb->leftjoin('u.projectGroups', 'upg');
        $qb->leftjoin('u.projects', 'up');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->where('upg.id = :projectGroup');
        $qb->groupBy('u.id');
        $qb->setParameter('projectGroup', $projectGroup);
        $qb = $this->makeFilters($qb, $filters, $fields);


        return $qb->getQuery()->execute();
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param array        $filters
     *
     * @return mixed
     */
    public function getUserProjectGroupCount(ProjectGroup $projectGroup, array $filters)
    {
        unset($filters['start']);
        unset($filters['length']);
        $fields = ['up.name', 'up.team', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle'];
        $qb = $this->createQueryBuilder('u');
        $qb->select('COUNT(DISTINCT u.id)');
        $qb->leftjoin('u.projectGroups', 'upg');
        $qb->leftjoin('u.projects', 'up');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->where('upg.id = :projectGroup');
        $qb->setParameter('projectGroup', $projectGroup);
        $qb = $this->makeFilters($qb, $filters, $fields);

        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = 0;
        }

        return $result;
    }


    /**
     * @param ProjectGroup $projectGroup
     * @param User         $user
     * @param array        $filters
     *
     * @return mixed
     */
    public function getAvailableUserToProjectGroupCount(ProjectGroup $projectGroup, User $user, array $filters)
    {
        unset($filters['start']);
        unset($filters['length']);
        $qbUser = $this->createQueryBuilder('us')
            ->join('us.projectGroups', 'uspg')
            ->where('uspg.id = :projectGroup');

        $fields = ['up.name', 'up.team', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle'];
        $qb = $this->createQueryBuilder('u');
        $qb->select('COUNT(DISTINCT u.id)');
        $qb->join('u.projects', 'up');
        $qb->join('u.practitioners', 'upr');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->eq('up.id', ':project'));
        $qb->andWhere($qb->expr()->eq('upr.id', ':practitioner'));
        $qb->andWhere($qb->expr()->notIn('u.id', $qbUser->getDQL()));
        $qb->setParameters([
            'project'      => $projectGroup->getProject(),
            'projectGroup' => $projectGroup,
            'practitioner' => $user,
        ]);
        $qb = $this->makeFilters($qb, $filters, $fields);

        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = 0;
        }

        return $result;
    }


    /**
     * @param Project $project
     * @param User    $user
     * @param array   $filters
     *
     * @return mixed
     */
    public function getAvailableUserToProjectCount(Project $project, User $user, array $filters)
    {
        unset($filters['start']);
        unset($filters['length']);
        $qbUser = $this->createQueryBuilder('us')
            ->join('us.projects', 'usp')
            ->where('usp.id = :project');

        $fields = ['', '', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle', ''];
        $qb = $this->createQueryBuilder('u');
        $qb->select('COUNT(u.id)');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->notIn('u.id', $qbUser->getDQL()));
        $qb->setParameter('project', $project->getId());
        if (!$user->hasRole('ROLE_ADMIN')) {
            $qb->join('u.practitioners', 'pract');
            $qb->andWhere('pract.id = :practitioner');
            $qb->setParameter('practitioner', $project->getOwner()->getId());
        }
        $qb = $this->makeFilters($qb, $filters, $fields);


        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = 0;
        }

        return $result;
    }

    /**
     * @param Project $project
     *
     * @return mixed
     */
    public function getProjectNotActivatedUsersCount(Project $project)
    {
        $qbUser = $this->createQueryBuilder('us')
            ->join('us.projects', 'usp')
            ->where('usp.id = :project');

        $qb = $this->createQueryBuilder('u');
        $qb->select('COUNT(u.id)');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->in('u.id', $qbUser->getDQL()));
        $qb->andWhere($qb->expr()->eq('u.approved', ':approved'));
        $qb->andWhere($qb->expr()->eq('u.testPass', ':testPass'));
        $qb->setParameters([
            'project'  => $project->getId(),
            'approved' => false,
            'testPass' => true,
        ]);


        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = 0;
        }

        return $result;
    }

    /**
     * @param int $projectId
     *
     * @return array
     */
    public function getProjectUsersWithPassedTest($projectId)
    {
        $qbUser = $this->createQueryBuilder('us')
            ->join('us.projects', 'usp')
            ->where('usp.id = :project');

        $qb = $this->createQueryBuilder('u');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ')
            ->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'')
            ->andWhere($qb->expr()->in('u.id', $qbUser->getDQL()))
            ->andWhere($qb->expr()->eq('u.testPass', ':testPass'))
            ->setParameters([
                'project' => $projectId,
                'testPass' => true,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $projectGroupId
     *
     * @return array
     */
    public function getProjectGroupUsersWithPassedTest($projectGroupId)
    {
        $qbUser = $this->createQueryBuilder('us')
            ->join('us.projectGroups', 'uspg')
            ->where('uspg.id = :projectGroup');

        $qb = $this->createQueryBuilder('u');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ')
            ->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'')
            ->andWhere($qb->expr()->in('u.id', $qbUser->getDQL()))
            ->andWhere($qb->expr()->eq('u.testPass', ':testPass'))
            ->setParameters([
                'projectGroup' => $projectGroupId,
                'testPass' => true,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $projectNames
     * @param array $filters
     *
     * @return mixed
     */
    public function getUsersByCompanyAndPractitioner($projectNames, array $filters)
    {

        $fields = ['up.name', 'up.team', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle', 'pg.name'];
        $qb = $this->createQueryBuilder('u');
        $qb->leftJoin('u.projects', 'up');
        $qb->leftJoin('u.projectGroups', 'pg');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->groupBy('u.id');
        $qb->andWhere($qb->expr()->in('up.name', ':projectNames'));
        $qb->setParameters([
            'projectNames'  => $projectNames,
        ]);

        $qb = $this->makeFilters($qb, $filters, $fields);

        return $qb->getQuery()->execute();
    }

    /**
     * @param array $projectNames
     * @param array $filters
     *
     * @return int|mixed
     */
    public function getUsersByCompanyAndPractitionerCount($projectNames, array $filters)
    {
        unset($filters['start']);
        unset($filters['length']);

        $fields = ['up.name', 'up.team', 'u.roles', 'u.firstName', 'u.lastName', 'u.mainStyle', 'pg.name'];
        $qb = $this->createQueryBuilder('u');
        $qb->leftjoin('u.projects', 'up');
        $qb->select('COUNT(DISTINCT u.id)');
        $qb->where('u.roles LIKE \'%ROLE_EMPLOYER%\' ');
        $qb->orwhere('u.roles LIKE \'%ROLE_MANAGER%\'');
        $qb->andWhere($qb->expr()->in('up.name', ':projectNames'));
        $qb->setParameters([
            'projectNames'  => $projectNames,
        ]);

        $qb = $this->makeFilters($qb, $filters, $fields);

        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = 0;
        }

        return $result;
    }

    /**
     * @param QueryBuilder $qb
     * @param array        $filters
     * @param array        $fields
     * @param bool         $practitioner
     *
     * @return QueryBuilder
     */
    private function makeFilters(QueryBuilder $qb, array $filters, array $fields, $practitioner = false)
    {
        if (isset($filters['order']) && is_array($filters['order'])) {
            foreach ($filters['order'] as $sortedField) {
                if (!empty($fields[$sortedField['column']])) {
                    $qb->addOrderBy($fields[$sortedField['column']], $sortedField['dir']);
                }
            }
        }

        if (isset($filters['start'])) {
            $qb->setFirstResult($filters['start']);
        }
        if (isset($filters['length'])) {
            $qb->setMaxResults($filters['length']);
        }
        if (!empty($filters['search']['value'])) {
            if ($practitioner) {
                $qb->andWhere('u.firstName LIKE :first_name OR u.lastName LIKE :last_name OR u.companyName LIKE :project_name');
                $qb->setParameter('first_name', $filters['search']['value'].'%')
                    ->setParameter('last_name', $filters['search']['value'].'%')
                    ->setParameter('project_name', $filters['search']['value'].'%');
            } else {
                $qb->join('u.mainStyle', 'ums');
                $qb->join('u.projects', 'uproj');
                $qb->andWhere('u.firstName LIKE :first_name OR u.lastName LIKE :last_name OR ums.name LIKE :main_style OR uproj.name LIKE :project_name OR uproj.team LIKE :team_name');
                $qb->setParameter('first_name', $filters['search']['value'].'%')
                    ->setParameter('last_name', $filters['search']['value'].'%')
                    ->setParameter('project_name', $filters['search']['value'].'%')
                    ->setParameter('main_style', $filters['search']['value'].'%')
                    ->setParameter('team_name', $filters['search']['value'].'%');
            }
        }

        return $qb;
    }
}

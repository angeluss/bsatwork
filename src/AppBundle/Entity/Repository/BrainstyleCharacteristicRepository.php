<?php

namespace AppBundle\Entity\Repository;


use TestBundle\Entity\Style;
use Doctrine\ORM\EntityRepository;

/**
 * Class BrainstyleCharacteristicRepository
 *
 */
class BrainstyleCharacteristicRepository extends EntityRepository
{

    /**
     * @param Style  $style
     * @param string $locale
     *
     * @return array
     */
    public function getRandomCharacteristicByStyleAndLocale(Style $style, $locale)
    {
        $qb = $this->createQueryBuilder('bc');

        $count = $qb->select('COUNT(bc.id)')
            ->where($qb->expr()->eq('bc.style', ':style'))
            ->andWhere($qb->expr()->eq('bc.locale', ':locale'))
            ->setParameters([
                'style'  => $style,
                'locale' => $locale
            ])
            ->getQuery()
            ->getSingleScalarResult();

        $offset = rand(1, $count);

        return $qb->select('bc.text')
            ->where($qb->expr()->eq('bc.style', ':style'))
            ->andWhere($qb->expr()->eq('bc.locale', ':locale'))
            ->setParameters([
                'style' => $style,
                'locale' => $locale
            ])
            ->setFirstResult($offset)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
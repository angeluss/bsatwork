<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\User;
use AppBundle\Entity\UserTestResult;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use TestBundle\Entity\Style;

/**
 * Class AnswerRepository
 *
 * @package TestBundle\Entity\Repository
 */
class UserTestResultRepository extends EntityRepository
{
    /**
     * @param array $data
     * @param User  $user
     */
    public function setUserResults($data, User $user)
    {
        $styles = $this->getEntityManager()
            ->getRepository('TestBundle:Style')
            ->findAll();
        /** @var Style $style */
        foreach ($styles as $style) {
            if ($style->getName() == $data['mainStyle']) {
                $user->setMainStyle($style);
            }
            if (isset($data[$style->getName()])) {
                $userResult = $this->getEntityManager()
                    ->getRepository('AppBundle:UserTestResult')
                    ->findOneBy(['user' => $user, 'style' => $style]);
                if ($userResult) {
                    $userResult->setPoint($data[$style->getName()]);
                } else {
                    $userTestResult = new UserTestResult($data[$style->getName()], $user, $style);
                    $this->getEntityManager()->persist($userTestResult);
                }
            }
        }
        $user->setTestPass(true);
        $this->getEntityManager()->flush();
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getEightDimension(User $user)
    {
        $testResult = [
            Style::CONCEPTUALISER => 0,
            Style::INNOVATOR => 0,
            Style::CONNECTOR => 0,
            Style::HUMANIZER => 0,
            Style::ORGANIZER => 0,
            Style::IMPLEMENTER => 0,
            Style::SYSTEMISER => 0,
            Style::ANALYZER => 0,
        ];
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('st.name', 'ur.point')
            ->from('AppBundle:UserTestResult', 'ur')
            ->leftJoin('ur.style', 'st')
            ->where('ur.user = :user')
            ->setParameter('user', $user);
        $results = $qb->getQuery()->getArrayResult();

        foreach ($results as $result) {
            if (isset($testResult[$result['name']])) {
                $testResult[$result['name']] = $result['point'];
            }
        }

        return $testResult;

    }

}
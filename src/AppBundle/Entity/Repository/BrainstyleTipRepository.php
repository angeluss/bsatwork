<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class BrainstyleTipRepository
 *
 */
class BrainstyleTipRepository extends EntityRepository
{

    /**
     * @return array
     */
    public function getRandomTip()
    {
        $qb = $this->createQueryBuilder('bt');

        $count = $qb->select('COUNT(bt.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $offset = rand(1, $count);

        $qb = $this->createQueryBuilder('bt');

        $dql = $qb
            ->setFirstResult($offset)
            ->setMaxResults(1)
            ->getQuery();

        return $dql->getOneOrNullResult();
    }
}
<?php

namespace AppBundle\Entity\Repository;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

/**
 * Class OrderRepository
 *
 * @package AppBundle\Entity\Repository
 */
class OrderRepository extends EntityRepository
{
    /**
     * @param array $filters
     *
     * @return array
     */
    public function getOrders(array $filters)
    {
        $fields = [
            'o.created',
            'ou.firstName',
            'ou.lastName',
            'ou.companyName',
            'o.amount',
            'o.status',
            'o.id as DT_RowId'
        ];
        $qb = $this->createQueryBuilder('o')
            ->select('DATE_FORMAT(o.created, \'%d-%m-%Y\') as created', $fields[1], $fields[2], $fields[3],
                $fields[4], $fields[5], $fields[6])
            ->leftJoin('o.user', 'ou')
            ->groupBy('o.id');
        $qb = $this->makeFilters($qb, $filters, $fields);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param array $filters
     * @param User  $user
     *
     * @return array
     */
    public function getUserOrders(array $filters, User $user)
    {
        $fields = [
            'o.created',
            'o.amount',
            'o.id as DT_RowId'
        ];
        $qb = $this->createQueryBuilder('o')
            ->select('DATE_FORMAT(o.created, \'%d-%m-%Y\') as created', $fields[1], $fields[2])
            ->groupBy('o.id');
        $fields[2] = 'o.id';
        $qb = $this->practitionerOrdersMakeFilters($qb, $filters, $fields);
        $qb->andWhere('o.user = :user')
            ->setParameter('user', $user);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param array $filters
     * @param User  $user
     *
     * @return array
     */
    public function getUserCountOrders(array $filters, User $user)
    {
        $fields = ['o.created', '', '', '', 'o.amount'];
        unset($filters['start']);
        unset($filters['length']);
        $qb = $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->leftJoin('o.user', 'ou');
        $qb = $this->makeFilters($qb, $filters, $fields);
        $qb->andWhere('o.user = :user')
            ->setParameter('user', $user);
        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = 0;
        }

        return $result;
    }

    /**
     * @param array $filters
     *
     * @return array
     */
    public function getCountOrders(array $filters)
    {
        $fields = ['o.created', '', '', '', 'o.amount'];
        unset($filters['start']);
        unset($filters['length']);
        $qb = $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->leftJoin('o.user', 'ou');
        $qb = $this->makeFilters($qb, $filters, $fields);

        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = 0;
        }

        return $result;
    }

    /**
     * @param QueryBuilder $qb
     * @param array        $filters
     * @param array        $fields
     *
     * @return QueryBuilder
     */
    private function makeFilters(QueryBuilder $qb, array $filters, array $fields)
    {
        if (isset($filters['order']) && is_array($filters['order'])) {
            foreach ($filters['order'] as $sortedField) {
                if (!empty($fields[$sortedField['column']])) {
                    $qb->addOrderBy($fields[$sortedField['column']], $sortedField['dir']);
                }
            }
        }

        if (isset($filters['start'])) {
            $qb->setFirstResult($filters['start']);
        }
        if (isset($filters['length'])) {
            $qb->setMaxResults($filters['length']);
        }
        if (!empty($filters['search']['value'])) {
            $qb->where('o.created LIKE :created OR ou.firstName LIKE :firstname OR ou.lastName LIKE :lastname ');
            $qb->setParameter('created', '%' . $filters['search']['value'] . '%')
                ->setParameter('lastname', $filters['search']['value'] . '%')
                ->setParameter('firstname', $filters['search']['value'] . '%');
        }

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param array        $filters
     * @param array        $fields
     *
     * @return QueryBuilder
     */
    private function practitionerOrdersMakeFilters(QueryBuilder $qb, array $filters, array $fields)
    {
        if (isset($filters['order']) && is_array($filters['order'])) {
            foreach ($filters['order'] as $sortedField) {
                if (!empty($fields[$sortedField['column']])) {
                    $qb->addOrderBy($fields[$sortedField['column']], $sortedField['dir']);
                }
            }
        }

        if (isset($filters['start'])) {
            $qb->setFirstResult($filters['start']);
        }
        if (isset($filters['length'])) {
            $qb->setMaxResults($filters['length']);
        }
        if (!empty($filters['search']['value'])) {
            $qb->where('o.created LIKE :created OR o.amount LIKE :amount');
            $qb->setParameter('created', '%' . $filters['search']['value'] . '%')
                ->setParameter('amount', $filters['search']['value'] . '%');
        }

        return $qb;
    }
}
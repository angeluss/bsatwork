<?php

namespace AppBundle\Entity\Repository;


use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectGroup;
use AppBundle\Entity\User;
use AppBundle\Exception\ProjectLimitUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ProjectGroupRepository
 *
 * @package AppBundle\Entity\Repository
 */
class ProjectGroupRepository extends EntityRepository
{

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getSubgroups(User $user)
    {
        $qb = $this->createQueryBuilder('pg')
            ->select('pg', 'ug')
            ->leftjoin('pg.project', 'p')
            ->leftjoin('pg.participants', 'ug')
            ->where('p.owner = :practitioner')
            ->orderBy('pg.id', 'desc')
            ->setParameter('practitioner', $user);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param Project $project
     * @param User    $user
     *
     * @return mixed
     */
    public function getProjectSubgroups(Project $project, User $user)
    {
        $qb = $this->createQueryBuilder('pg')
            ->select('pg', 'ug')
            ->leftjoin('pg.project', 'p')
            ->leftjoin('pg.participants', 'ug')
            ->where('p.id = :project')
            ->orderBy('pg.id', 'desc')
            ->setParameter('project', $project);

        if ($user->hasRole('ROLE_EMPLOYER') || $user->hasRole('ROLE_MANAGER')) {
            $qb->andWhere('pg.owner = :user OR pg.owner IS NULL')
               ->setParameter('user', $user);
        } else {
            $qb->andWhere($qb->expr()->isNull('pg.owner'));
        }

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getUserSubgroups(User $user)
    {
        $qb = $this->createQueryBuilder('pg');
        $qb->select('pg', 'ug')
           ->leftjoin('pg.project', 'p')
           ->leftjoin('pg.participants', 'ug')
           ->where('pg.owner = :user')
           ->andWhere($qb->expr()->isNull('pg.project'))
           ->orderBy('pg.id', 'desc')
           ->setParameter('user', $user);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param int    $projectGroupId
     * @param string $resultType
     *
     * @return array
     */
    public function getMainStyleBySubgroup($projectGroupId, $resultType = 'default')
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('ROUND(AVG(ur.point),0) as avg_point', 'st.name')
            ->from('AppBundle:UserTestResult', 'ur')
            ->join('ur.style', 'st')
            ->join('ur.user', 'u')
            ->join('u.projectGroups', 'pg')
            ->where('u.testPass = :testPass')
            ->andWhere('pg.id = :projectGroup')
            ->groupBy('st.name')
            ->orderBy('st.orderType')
            ->setParameter('projectGroup', $projectGroupId)
            ->setParameter('testPass', true);

        $result = [];
        if ($resultType == 'formatted') {
            $projectStylePoints = $qb->getQuery()->getArrayResult();
            foreach ($projectStylePoints as $stylePoint) {
                $result[$stylePoint['name']] = $stylePoint['avg_point'];
            }
        } else {
            $result = $qb->getQuery()->getArrayResult();
        }

        return $result;

    }

    /**
     * @param int   $projectGroupId
     * @param array $filters
     *
     * @return array
     */
    public function getDimensions($projectGroupId, array $filters)
    {
        $fields = [
            'user_name',
            '',
            'style_1',
            'style_2',
            'style_3',
            'style_4',
            'style_5',
            'style_6',
            'style_7',
            'style_8',
            'style_name'
        ];
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('CONCAT_WS(\' \',u.firstName, u.lastName) AS user_name', 'pg.name AS group_name',
                'ROUND(SUM(IF (st.orderType = 1, ur.point, 0))) as style_1',
                'ROUND(SUM(IF (st.orderType = 2, ur.point, 0))) as style_2',
                'ROUND(SUM(IF (st.orderType = 3, ur.point, 0))) as style_3',
                'ROUND(SUM(IF (st.orderType = 4, ur.point, 0))) as style_4',
                'ROUND(SUM(IF (st.orderType = 5, ur.point, 0))) as style_5',
                'ROUND(SUM(IF (st.orderType = 6, ur.point, 0))) as style_6',
                'ROUND(SUM(IF (st.orderType = 7, ur.point, 0))) as style_7',
                'ROUND(SUM(IF (st.orderType = 8, ur.point, 0))) as style_8',
                'ust.name AS style_name'

            )
            ->from('AppBundle:UserTestResult', 'ur')
            ->join('ur.user', 'u')
            ->join('u.mainStyle', 'ust')
            ->join('u.projectGroups', 'pg')
            ->join('ur.style', 'st')
            ->where('u.testPass = :testPass')
            ->andWhere('pg.id = :projectGroup')
            ->groupBy('u.id')
            ->addGroupBy('ur.user')
            ->setParameter('projectGroup', $projectGroupId)
            ->setParameter('testPass', true);
        $this->makeFilters($qb, $filters, $fields);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param int   $projectGroupId
     * @param array $filters
     *
     * @return array
     */
    public function getDimensionsUserCount($projectGroupId, array $filters)
    {
        $fields = [];
        unset($filters['start']);
        unset($filters['length']);
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(DISTINCT ur.user)')
            ->from('AppBundle:UserTestResult', 'ur')
            ->join('ur.user', 'u')
            ->join('u.mainStyle', 'ust')
            ->join('u.projectGroups', 'pg')
            ->where('u.testPass = :testPass')
            ->andWhere('pg.id = :projectGroup')
            ->setParameter('projectGroup', $projectGroupId)
            ->setParameter('testPass', true);
        $this->makeFilters($qb, $filters, $fields);


        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = 0;
        }

        return $result;
    }

    /**
     * @param array        $userIds
     * @param ProjectGroup $projectGroup
     *
     * @throws ProjectLimitUserException
     * @throws \Doctrine\ORM\ORMException
     */
    public function addUser(array $userIds, ProjectGroup $projectGroup)
    {
        $em = $this->getEntityManager();
        foreach ($userIds as $id) {
            $ref = $em->getReference('AppBundle:User', $id);
            $projectGroup->addParticipants($ref);
        }
        $em->flush();
    }

    /**
     * @param array        $userIds
     * @param ProjectGroup $projectGroup
     *
     * @throws ProjectLimitUserException
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteUsers(array $userIds, ProjectGroup $projectGroup)
    {
        $em = $this->getEntityManager();
        foreach ($userIds as $id) {
            $ref = $em->getReference('AppBundle:User', $id);
            $projectGroup->removeParticipants($ref);
        }
        $em->flush();
    }

    /**
     * @param QueryBuilder $qb
     * @param array        $filters
     * @param array        $fields
     *
     * @return QueryBuilder
     */
    private function makeFilters(QueryBuilder $qb, array $filters, array $fields)
    {
        if (isset($filters['order']) && is_array($filters['order'])) {
            foreach ($filters['order'] as $sortedField) {
                if (!empty($fields[$sortedField['column']])) {
                    $qb->addOrderBy($fields[$sortedField['column']], $sortedField['dir']);
                }
            }
        }

        if (isset($filters['start'])) {
            $qb->setFirstResult($filters['start']);
        }
        if (isset($filters['length'])) {
            $qb->setMaxResults($filters['length']);
        }
        if (!empty($filters['search']['value'])) {
            $qb->andWhere('u.firstName LIKE :firstname OR u.lastName LIKE :lastname OR ust.name LIKE :style_name');
            $qb->setParameter('lastname', $filters['search']['value'] . '%')
                ->setParameter('style_name', $filters['search']['value'] . '%')
                ->setParameter('firstname', $filters['search']['value'] . '%');
        }

        return $qb;
    }
}
<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use AppBundle\Entity\Translation\BrainstyleTipTranslation;

/**
 * Class BrainstyleTip
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\BrainstyleTipRepository")
 * @ORM\Table(name="brainstyle_tips")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\Translation\BrainstyleTipTranslation")
 */
class BrainstyleTip implements Translatable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="TestBundle\Entity\Style", inversedBy="tips")
     * @ORM\JoinColumn(name="style", referencedColumnName="id", onDelete="CASCADE")
     */
    private $style;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *   targetEntity="AppBundle\Entity\Translation\BrainstyleTipTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;

    public function __construct()
    {
        $this->id = md5(uniqid() . mt_rand());
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getText();
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param BrainstyleTipTranslation $t
     */
    public function addTranslation(BrainstyleTipTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * @return mixed
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @param mixed $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

}
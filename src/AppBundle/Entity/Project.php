<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Project
 *
 * @ORM\Table(name="projects")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ProjectRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"checkAvailableUserOrders"})
 *
 */
class Project
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="team", type="string", length=255)
     */
    private $team;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount_new_users", type="integer")
     * @Assert\GreaterThanOrEqual(
     *     value = 0
     * )
     */
    private $amountNewUsers = 0;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="myProjects")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $owner;

    /**
     * @var User[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="projects", cascade={"persist", "remove"})
     */
    private $participants;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255)
     */
    private $hash;

    /**
     * @var bool
     *
     * @ORM\Column(name="users_activated", type="boolean")
     */
    private $usersActivated = false;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProjectGroup", mappedBy="project", cascade={"persist", "remove"})
     */
    private $subgroups;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id = md5(uniqid().mt_rand());
        $this->participants = new ArrayCollection();
        $this->subgroups = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set team
     *
     * @param string $team
     *
     * @return Project
     */
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return string
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set amountNewUsers
     *
     * @param integer $amountNewUsers
     *
     * @return Project
     */
    public function setAmountNewUsers($amountNewUsers)
    {
        $userDiff = $this->amountNewUsers - $amountNewUsers;
        if ($this->owner) {
            $balance = $this->owner->getBalance();
            $amount = $balance->getAmountUsers() + $userDiff;
            $balance->setAmountUsers($amount);
        }

        $this->amountNewUsers = $amountNewUsers;

        return $this;
    }

    /**
     * Get amountNewUsers
     *
     * @return integer
     */
    public function getAmountNewUsers()
    {
        return $this->amountNewUsers;
    }

    /**
     * @return $this
     */
    public function decreaseAmountNewUsers()
    {
        --$this->amountNewUsers;

        return $this;
    }

    /**
     * Add more amountNewUsers
     *
     * @param integer $countNewUsers
     *
     * @return $this
     */
    public function addAmountNewUsers($countNewUsers)
    {
        $this->setAmountNewUsers($this->amountNewUsers + $countNewUsers);

        return $this;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     *
     * @return Project
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @param User[]|ArrayCollection $participants
     *
     * @return Project
     */
    public function setParticipants($participants)
    {
        $this->participants = $participants;
        foreach ($participants as $participant) {
            if ($this->owner) {
                $participant->addPractitioners($this->getOwner());
            }
        }


        return $this;
    }

    /**
     * @param User $participant
     *
     * @return $this
     */
    public function addParticipants(User $participant)
    {
        if (!$this->participants->contains($participant)) {
            $this->participants->add($participant);
            $participant->addProject($this);
            if ($this->owner) {
                $participant->addPractitioners($this->getOwner());
            }
        }

        return $this;
    }

    /**
     * @param User $participant
     *
     * @return $this
     */
    public function removeParticipants(User $participant)
    {
        if ($this->participants->contains($participant)) {
            $this->participants->removeElement($participant);
            $participant->removeProject($this);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     *
     * @return Project
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @param ExecutionContextInterface $context
     */
    public function checkAvailableUserOrders(ExecutionContextInterface $context)
    {
        if ($this->getOwner() && $this->getOwner()->getAvailableUserBalance() < 0) {
            $context->addViolationAt('amountNewUsers', 'project.order_more_tests');
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getSubgroups()
    {
        return $this->subgroups;
    }

    /**
     * @param ArrayCollection $subgroups
     */
    public function setSubgroups($subgroups)
    {
        $this->subgroups = $subgroups;
    }

    /**
     * @param ProjectGroup $subgroup
     */
    public function addSubgroup(ProjectGroup $subgroup)
    {
        if (!$this->subgroups->contains($subgroup)) {
            $this->addSubgroup($subgroup);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }


    /**
     * @return boolean
     */
    public function isUsersActivated()
    {
        return $this->usersActivated;
    }

    /**
     * @param boolean $usersActivated
     */
    public function setUsersActivated($usersActivated)
    {
        $this->usersActivated = $usersActivated;
    }
}

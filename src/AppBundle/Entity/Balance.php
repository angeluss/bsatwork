<?php

namespace AppBundle\Entity;

use AppBundle\Exception\NegativeBalanceException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Balance
 *
 * @ORM\Table(name="balances")
 * @ORM\Entity
 */
class Balance
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @var integer
     *
     * @Assert\GreaterThanOrEqual(
     *     value = 0,
     *     message = "project.order_more_tests"
     * )
     * @ORM\Column(name="amount_users", type="integer")
     */
    private $amountUsers;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="balance")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var Order[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order", mappedBy="balance", cascade={"persist"})
     */
    private $orders;

    public function __construct()
    {
        $this->id = md5(uniqid() . mt_rand());
        $this->orders = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amountUsers
     *
     * @param integer $amountUsers
     *
     * @return Balance
     * @throws NegativeBalanceException
     */
    public function setAmountUsers($amountUsers)
    {
        if ($amountUsers < 0) {
            throw new NegativeBalanceException();
        }
        $this->amountUsers = $amountUsers;

        return $this;
    }

    /**
     * Get amountUsers
     *
     * @return integer
     */
    public function getAmountUsers()
    {
        return $this->amountUsers;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Balance
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Order[]|ArrayCollection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param Order[]|ArrayCollection $orders
     *
     * @return Balance
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;

        return $this;
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function addOrder(Order $order)
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->setBalance($this);
        }

        return $this;
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function removeOrder(Order $order)
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
        }

        return $this;
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TestBundle\Entity\Style;

/**
 * Balance
 *
 * @ORM\Table(name="user_test_result", uniqueConstraints={@ORM\UniqueConstraint(name="search_idx", columns={"style", "user"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserTestResultRepository")
 */
class UserTestResult
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="point", type="decimal", scale=2)
     */
    private $point;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="results")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @var Style
     *
     * @ORM\ManyToOne(targetEntity="TestBundle\Entity\Style")
     * @ORM\JoinColumn(name="style", referencedColumnName="id")
     */
    private $style;

    /**
     * @param int $point
     * @param User $user
     * @param Style $style
     */
    public function __construct($point = 0, User $user = null, Style $style = null)
    {
        $this->id = md5(uniqid() . mt_rand());
        $this->point = $point;
        $this->user = $user;
        $this->style = $style;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Balance
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Style
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @param Style $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @return int
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param int $point
     */
    public function setPoint($point)
    {
        $this->point = $point;
    }

}

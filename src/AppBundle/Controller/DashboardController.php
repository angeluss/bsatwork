<?php

namespace AppBundle\Controller;

use AppBundle\Annotations\NoTestNoAccess;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DashboardController
 *
 * @package AppBundle\Controller
 */
class DashboardController extends Controller
{
    /**
     * @Route("/dashboard/", name="dashboard_index")
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('@App/dashboard/index.html.twig');
    }

    /**
     *
     * @return Response
     */
    public function getAddOrderAction()
    {
        $form = $this->createForm($this->get('appbundle.form.type.add_order'));

        return $this->render('@App/dashboard/order/add-order.html.twig', ['form' => $form->createView()]);
    }
}
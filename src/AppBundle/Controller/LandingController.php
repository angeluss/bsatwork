<?php
namespace AppBundle\Controller;

use AppBundle\Annotations\NoTestNoAccess;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LandingController
 *
 * @package AppBundle\Controller
 */
class LandingController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function indexAction()
    {
        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Route("/registration/error", name="registration_error")
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function errorRegistrationAction()
    {
        return $this->render('AppBundle:landing:error_registration.html.twig');
    }

    /**
     * @Route("/login/error", name="login_error")
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function errorLoginAction()
    {
        $response = $this->render('@FOSUser/Security/user_not_approved_error.html.twig');

        return $response;
    }

    /**
     * @Route("/contact-us", name="contact-us")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function contactUsAction(Request $request)
    {
        $mailer = $this->get('appbundle.mailer.mailer');
        $name = $request->request->get('name');
        $email = $request->request->get('email');
        $phone = $request->request->get('phone');
        $mailer->sendContactUsMessage($name, $email, $phone);

        return $this->render('@App/landing/index.html.twig');
    }
}

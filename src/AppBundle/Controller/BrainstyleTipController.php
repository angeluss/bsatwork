<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BrainstyleTip;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BrainstyleTipController
 *
 */
class BrainstyleTipController extends Controller
{
    /**
     * @return Response
     */
    public function getRandomTipAction()
    {
        $tipRepo = $this->get('doctrine.orm.entity_manager')->getRepository('AppBundle:BrainstyleTip');
        /** @var BrainstyleTip $tip $tip */
        $tip = $tipRepo->getRandomTip();
        return $this->render('@App/dashboard/users/brainstyle_tip.html.twig', [
            'tip'   => $tip->getText(),
            'style' => $tip->getStyle()->getName()
        ]);
    }
}

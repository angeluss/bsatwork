<?php

namespace AppBundle\Controller;

use AppBundle\Annotations\NoTestNoAccess;
use AppBundle\DBAL\Types\OrderStatusType;
use AppBundle\Entity\Order;
use AppBundle\Entity\User;
use AppBundle\Exception\NegativeBalanceException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * Class OrderController
 *
 * @package AppBundle\Controller
 */
class OrderController extends Controller
{
    /**
     * @Route("/dashboard/order/list", name="dashboard_order_list", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return Response
     */
    public function listAction()
    {
        return $this->render('@App/dashboard/order/list.html.twig');
    }

    /**
     * @param Request $request
     *
     * @Route("/dashboard/order/list-data/", name="dashboard_order_list_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return JsonResponse
     */
    public function ajaxListAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $userRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Order');
        $orders = $userRepo->getOrders($request->query->all());
        $ordersCount = $userRepo->getCountOrders($request->query->all());
        $data = ['data' => $orders, 'recordsTotal' => $ordersCount, 'recordsFiltered' => $ordersCount];

        return new JsonResponse($data);
    }

    /**
     * @param Request $request
     *
     * @Route("/dashboard/order/list-data/user", name="dashboard_order_user_list_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_PRACTITIONER')")
     *
     * @return JsonResponse
     */
    public function ajaxPractitionerListAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $userRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Order');
        $orders = $userRepo->getUserOrders($request->query->all(), $this->getUser());
        $ordersCount = $userRepo->getUserCountOrders($request->query->all(), $this->getUser());
        $data = ['data' => $orders, 'recordsTotal' => $ordersCount, 'recordsFiltered' => $ordersCount];

        return new JsonResponse($data);
    }

    /**
     * @param Order   $order
     * @param Request $request
     *
     * @Security("has_role('ROLE_ADMIN')")
     * @NoTestNoAccess
     * @Route("/dashboard/order/change-status/{order}", name="dashboard_change_status_ajax", options={"expose"=true})
     *
     * @return Response
     */
    public function ajaxChangStatusAction(Order $order, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $status = $request->get('status');
        if (!in_array($status, [0, 1])) {
            throw new InvalidParameterException('Status parameter is not available');
        }

        $order->setStatus($status);
        $this->get('doctrine.orm.entity_manager')->flush();

        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @Route("/dashboard/order/list/user", name="dashboard_order_user_list", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_PRACTITIONER')")
     *
     * @return Response
     */
    public function listPractitionerAction()
    {
        $form = $this->createForm($this->get('appbundle.form.type.add_order'));

        return $this->render('@App/dashboard/order/practitioner_list.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     *
     * @Security("has_role('ROLE_PRACTITIONER')")
     * @NoTestNoAccess
     * @Route("/dashboard/order/create", name="dashboard_order_create", options={"expose"=true})
     * @return Response
     */
    public function createOrder(Request $request)
    {
        $order = new Order();
        $form = $this->createForm($this->get('appbundle.form.type.add_order'), $order);
        if ($request->isMethod('POST')) {
            $order->setUser($this->getUser());
            $form->handleRequest($request);
            if ($form->isValid()) {
                $balance = $order->getUser()->getBalance();
                $balance->addOrder($order);
                $order->getUser()->addBalanceUserAmount($order->getAmount());
                $this->get('doctrine.orm.entity_manager')->persist($order);
                $this->get('doctrine.orm.entity_manager')->flush();
                $this->get('session')->getFlashBag()->add('success',
                    $this->get('translator')->trans('Order was successfully created')
                );

                return $this->redirect($request->headers->get('referer'));
            }
        }

        return $this->render('@App/dashboard/order/practitioner_list.html.twig',
            ['form' => $form->createView(), 'show_create' => true]);
    }

    /**
     * @param Order $order
     *
     * @return Response
     * @Method("POST")
     *
     * @Security("has_role('ROLE_PRACTITIONER')")
     * @Route("/dashboard/order/{order}/delete", name="dashboard_order_delete", options={"expose"=true})
     * @NoTestNoAccess
     */
    public function deleteOrder(Order $order)
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user != $order->getUser()) {
            throw new AccessDeniedException();
        }

        if ($user->getAvailableUserBalance() < $order->getAmount()) {

        }

        if (OrderStatusType::STATUS_PAID == $order->getStatus()) {
            $this->get('session')->getFlashBag()->add('error',
                $this->get('translator')->trans('Can not delete approved order! Please contact your admin')
            );

            return $this->redirectToRoute('dashboard_order_user_list');
        }
        $balance = $user->getBalance();
        try {
            $balance->setAmountUsers($balance->getAmountUsers() - $order->getAmount());

        } catch (NegativeBalanceException $e) {
            $this->get('session')->getFlashBag()->add('error',
                $this->get('translator')->trans('Can not delete order! Balance can not be negative')
            );

            return $this->redirectToRoute('dashboard_order_user_list');
        }
        $balance->removeOrder($order);
        $this->get('doctrine.orm.entity_manager')->remove($order);
        $this->get('doctrine.orm.entity_manager')->flush();

        $this->get('session')->getFlashBag()->add('success',
            $this->get('translator')->trans('Order was successfully deleted')
        );


        return $this->redirectToRoute('dashboard_order_user_list');
    }

}

<?php

namespace AppBundle\Controller;

use AppBundle\Annotations\NoTestNoAccess;
use AppBundle\Entity\ChartImage;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectGroup;
use AppBundle\Entity\User;
use AppBundle\Exception\NegativeBalanceException;
use AppBundle\Exception\ProjectLimitUserException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ProjectController
 *
 * @package AppBundle\Controller
 */
class ProjectController extends Controller
{
    /**
     * @Route("/dashboard/projects", name="dashboard_projects_list")
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function listAction()
    {
        return $this->render('@App/dashboard/project/list.html.twig');
    }

    /**
     * @param Request $request
     *
     * @Route("/dashboard/projects/list-ajax", name="dashboard_ajax_projects_list", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function overviewAjaxProjectsAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $projectRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Project');
        $projects = $projectRepo->getProjectsByUser($this->getUser(), $request->query->all());
        $projectCount = (int) $projectRepo->getCountByUser($this->getUser(), $request->query->all());
        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $projectCount,
            'recordsFiltered' => $projectCount,
            'data' => $projects
        ];

        return new JsonResponse($data);
    }

    /**
     * @param Project $project
     * @param Request $request
     *
     * @Route("/dashboard/projects/{project}/available-users", name="dashboard_get_available_users_list_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function getAvailableUserProjectAjaxAction(Project $project, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $this->getUser() != $project->getOwner() &&
            !$project->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        $userRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User');
        $users = $userRepo->getAvailableUserToProject($project, $this->getUser(), $request->query->all());
        $userCount = $userRepo->getAvailableUserToProjectCount($project, $this->getUser(), $request->query->all());

        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $users
        ];

        $userData = $this->get('jms_serializer')->serialize($data, 'json');

        return new Response($userData, 200, ['Content-Type' => 'application/json']);
    }


    /**
     * @param Project $project
     * @param Request $request
     *
     * @Route("/dashboard/projects/{project}/add-users", name="dashboard_add_users_project_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function addUserAjaxAction(Project $project, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $project->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        $userIds = $request->get('userIds', []);

        if (count($userIds) == 0) {
            return new JsonResponse([
                'status' => 'error',
                'message' => $this->get('translator')->trans('Action aborted No items were selected')
            ]);

        }
        try {
            $this->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Project')
                ->addUser($userIds, $project);
        } catch (ProjectLimitUserException $e) {
            return new JsonResponse([
                'status' => 'error',
                'message' => sprintf($this->get('translator')->trans('User limit max %d'),
                    $project->getAmountNewUsers())
            ]);
        }

        return new JsonResponse(['status' => 'ok']);
    }


    /**
     * @param Request $request
     *
     * @Route("/dashboard/projects/create", name="dashboard_projects_create")
     * @NoTestNoAccess
     * @return Response
     */
    public function createProject(Request $request)
    {
        $project = new Project();
        $form = $this->createForm($this->get('appbundle.form.type.projectformtype'), $project);
        if ($request->isMethod('post')) {
            $project->setOwner($this->getUser());
            try {
                $form->handleRequest($request);
            } catch (NegativeBalanceException $e) {
                $form->get('amountNewUsers')->addError(
                    new FormError($this->get('translator')->trans('validator.project.order_more_tests'))
                );

                return $this->render('@App/dashboard/project/create.html.twig', ['form' => $form->createView()]);
            }
            if ($form->isValid()) {
                $project->setHash($this->get('fos_user.util.token_generator')->generateToken());
                $this->get('doctrine.orm.entity_manager')->persist($project);
                $this->get('doctrine.orm.entity_manager')->flush();
                $this->get('session')->getFlashBag()->add('success',
                    $this->get('translator')->trans('Project was successfully created')
                );

                return $this->redirectToRoute('dashboard_projects_list');
            }
        }

        return $this->render('@App/dashboard/project/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Project $project
     *
     * @return Response
     *
     * @Route("/dashboard/projects/{project}/info", name="dashboard_projects_info", options={"expose"=true})
     * @NoTestNoAccess
     */
    public function projectInfo(Project $project)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $project->getOwner() != $this->getUser() &&
            !$project->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        $userRepo = $this->get('doctrine.orm.entity_manager')->getRepository('AppBundle:User');
        $notActivatedUsersCount = $userRepo->getProjectNotActivatedUsersCount($project);
        $projectStyle = $this->getProjectMainStyle($project);

        return $this->render(
            '@App/dashboard/project/info.html.twig',
            [
                'project'                => $project,
                'projectStyle'           => $projectStyle,
                'notActivatedUsersCount' => $notActivatedUsersCount
            ]);
    }

    /**
     * @param Request $request
     * @param Project $project
     *
     * @return Response
     *
     * @Route("/dashboard/projects/{project}/edit", name="dashboard_projects_edit", options={"expose"=true})
     * @NoTestNoAccess
     */
    public function editProject(Request $request, Project $project)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') && $this->getUser() != $project->getOwner()) {
            throw new AccessDeniedException();
        }
        $form = $this->createForm($this->get('appbundle.form.type.projectformtype'), $project);
        if ($request->isMethod('post')) {
            try {
                $form->handleRequest($request);
            } catch (NegativeBalanceException $e) {
                $form->get('amountNewUsers')->addError(
                    new FormError($this->get('translator')->trans('validator.project.order_more_tests'))
                );

                return $this->render('@App/dashboard/project/create.html.twig', ['form' => $form->createView()]);
            }
            if ($form->isValid()) {
                $this->get('doctrine.orm.entity_manager')->flush();
                $this->get('session')->getFlashBag()->add('success',
                    $this->get('translator')->trans('Project was successfully updated')
                );

                return $this->redirectToRoute('dashboard_projects_info', [
                    'project' => $project->getId()
                ]);
            }
        }

        return $this->render(
            '@App/dashboard/project/edit.html.twig',
            [
                'form' => $form->createView(),
                'project' => $project,
            ]);
    }

    /**
     * @param Project $project
     * @param Request $request
     *
     * @Route("/dashboard/projects/{project}/users", name="dashboard_get_project_users_list_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function getProjectUsersAjaxAction(Project $project, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $project->getOwner() != $this->getUser() &&
            !$project->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $userRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User');
        $users = $userRepo->getProjectUsers($project, $request->query->all());
        /** @var User $user */
        foreach ($users as $user) {
            /** @var ProjectGroup[] $userSubgroups */
            $userSubgroups = $user->getProjectGroups();
            foreach ($userSubgroups as $subgroup) {
                if ($subgroup->getProject() !== $project) {
                    $user->removeProjectGroup($subgroup);
                } else {
                    if ($subgroup->getOwner() !== $currentUser && $subgroup->getOwner() !== null) {
                        $user->removeProjectGroup($subgroup);
                    }
                }
            }
        }
        $userCount = $userRepo->getProjectUsersCount($project, $request->query->all());

        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $users,
        ];

        $userData = $this->get('jms_serializer')->serialize($data, 'json');

        return new Response($userData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param Project $project
     * @param Request $request
     *
     * @Route("/dashboard/projects/{project}/delete-users", name="dashboard_delete_users_project_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function deleteUserAjaxAction(Project $project, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $project->getOwner() != $this->getUser() &&
            !$project->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        $userIds = $request->get('userIds', []);
        $usersCount = count($userIds);
        if ($usersCount == 0) {
            return new JsonResponse([
                'status' => 'error',
                'message' => $this->get('translator')->trans('Action aborted. No items were selected')
            ]);

        }
        $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Project')
            ->deleteUser($userIds, $project);

        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @param Project $project
     * @param Request $request
     *
     * @Route("/dashboard/projects/{project}/add-accounts", name="dashboard_project_add_accounts", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function addAmountNewUsers(Project $project, Request $request)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $project->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        if ($request->isMethod('post')) {
            $count = $request->get('amountNewUsers');
            $route = $request->headers->get('referer');

            try {
                $project->addAmountNewUsers($count);
            } catch (NegativeBalanceException $e) {
                $this->addFlash('error',
                    $this->get('translator')->trans('validator.project.order_more_tests')
                );

                return $this->redirect($route);
            }
            $this->get('doctrine.orm.entity_manager')->flush();
            $this->addFlash('success',
                $this->get('translator')->trans('Project accounts was successfully added')
            );

            return $this->redirect($route);
        }

        return $this->createNotFoundException();
    }

    /**
     * @param Project $project
     * @param Request $request
     *
     * @Route("/dashboard/projects/{project}/activate-users", name="dashboard_project_activate_users", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function activateProjectUsers(Project $project, Request $request)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $project->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        $userRepo = $this->getDoctrine()->getRepository('AppBundle:User');
        /** @var User[] $users */
        $users = $userRepo->getProjectNotActivatedUsers($project, $request->query->all());
        $route = $request->headers->get('referer');
        $translator = $this->get('translator.default');

        foreach ($users as $user) {
            $user->setApproved(true);
            $translator->setLocale($user->getLocale());
            $this->get('appbundle.mailer.mailer')->sendAccountApproveMessage($user);
        }

        $translator->setLocale($request->getLocale());
        $this->addFlash('success',
            $this->get('translator')->trans('Project users were successfully activated!')
        );

        $project->setUsersActivated(true);
        $em = $this->get('doctrine.orm.entity_manager');
        $em->persist($project);
        $em->flush();

        return $this->redirect($route);
    }

    /**
     * @param Project $project
     *
     * @return Response
     */
    public function getProjectMainStyle(Project $project)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $project->getOwner() != $this->getUser() &&
            !$project->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }

        $projectUserWithPassedTest = $this->getDoctrine()->getRepository('AppBundle:User')->getProjectUsersWithPassedTest($project->getId());
        if (!$project->getParticipants()->isEmpty() && !empty($projectUserWithPassedTest)) {
            $mainStylesScores = $this->getCoreStylesScore($project);
            $style = $this->get('testbundle.calculate_result')->calculateGroupStyle($mainStylesScores);
        } else {
            $style = null;
        }

        return $style;
    }

    /**
     * @param Project $project
     * @param Request $request
     *
     * @Route("/dashboard/project/web-image/{id}/", name="dashboard_project_web_image_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     * @internal param User $user
     * @internal param Request $request
     */
    public function ajaxWebImagePathAction(Project $project, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $project->getOwner() != $this->getUser() &&
            !$project->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        $resultStyles = [];
        $projectRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Project');
        $styles = $projectRepo->getMainStyleByTeam($project->getId());
        foreach ($styles as $styleResult) {
            $resultStyles[$styleResult['name']] = $styleResult['avg_point'];
        }

        $webImage = $this->get('appbundle.graph')->getGraph($resultStyles);
        $webImageThumbnailUrl = '';
        if ($webImage instanceof ChartImage) {
            $webPath = $this->get('vich_uploader.templating.helper.uploader_helper')
                ->asset($webImage, 'imageFile');
            $webImageThumbnailUrl = $this->get('liip_imagine.cache.manager')->getBrowserPath($webPath, 'web_path');
        }

        return new JsonResponse(['data' => ['thumbnailUrl' => $webImageThumbnailUrl]]);
    }

    /**
     * @param Project $project
     *
     * @Route("/dashboard/project/{id}/web-image/", name="project_web_image", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     * @internal param User $user
     */
    public function webImagePathAction(Project $project)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $project->getOwner() != $this->getUser() &&
            !$project->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        $resultStyles = [];
        $projectRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Project');
        $styles = $projectRepo->getMainStyleByTeam($project->getId());
        foreach ($styles as $styleResult) {
            $resultStyles[$styleResult['name']] = $styleResult['avg_point'];
        }

        $webImage = $this->get('appbundle.graph')->getGraph($resultStyles);
        $webImageThumbnailUrl = '';
        if ($webImage instanceof ChartImage) {
            $webPath = $this->get('vich_uploader.templating.helper.uploader_helper')
                ->asset($webImage, 'imageFile');
            $webImageThumbnailUrl = $this->get('liip_imagine.cache.manager')->getBrowserPath($webPath, 'web_path');
        }

        return $webImageThumbnailUrl;
    }

    /**
     * @param Project $project
     * @param Request $request
     *
     * @Route("/dashboard/project/dimensions-ajax/{id}", name="dashboard_ajax_project_dimensions", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function overviewAjaxDimensionsAction(Project $project, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $project->getOwner() != $this->getUser() &&
            !$project->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        $projectRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Project');
        $groupDimensions = (array) $projectRepo->getDimensions($project->getId(), $request->query->all());
        $avgGroup = (array) $projectRepo->getMainStyleByTeam($project->getId());
        $userCount = (array) $projectRepo->getDimensionsUserCount($project->getId(), $request->query->all());
        $data['data'] = $groupDimensions;
        $data['group_data'] = $avgGroup;

        if (count($avgGroup) > 0) {
            $mainStylesScores = $this->getCoreStylesScore($project);
            $groupStyle = $this->get('testbundle.calculate_result')->calculateGroupStyle($mainStylesScores);
        }

        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $groupDimensions,
            'group_data' => $avgGroup,
            'group_style' => isset($groupStyle) ? $groupStyle : '',
        ];

        return new JsonResponse($data);
    }

    /**
     * Download PDF report for project
     *
     * @Route("/dashboard/project/{project}/download-pdf", name="dashboard_project_pdf", options={"expose"=true})
     * @NoTestNoAccess
     * @param Project $project
     * @return Response
     */
    public function downloadPdfAction(Project $project)
    {
        $projectStyle = $this->getProjectMainStyle($project);
        $projectWeb   = $this->webImagePathAction($project);
        $projectUsers = $this->getDoctrine()->getRepository('AppBundle:User')->getProjectUsersWithPassedTest($project->getId());

        $html = $this->renderView('@App/dashboard/project/project_pdf_report.html.twig', [
            'project' => $project,
            'project_style' => $projectStyle,
            'project_web' => $projectWeb,
            'project_users' => $projectUsers,
        ]);

        $filename = 'project-report.pdf';

        $mpdfService = $this->get('tfox.mpdfport');

        $response = $mpdfService->generatePdf($html, [
            'outputDest'      => 'S',
            'outputFilename'  => $filename,
            'constructorArgs' => [
                'mode'          => 'BLANK',
                'format'        => 'A4',
                'margin_left'   => 10,
                'margin_right'  => 10,
                'margin_top'    => 10,
                'margin_bottom' => 10,
                'margin_header' => 10,
                'margin_footer' => 10,
            ],
        ]);

        return new Response($response, 200, [
            'Content-Type'        => 'application/pdf',
            'Content-Disposition' => 'attachment; filename='.$filename,
        ]);
    }

    /**
     * @param Project $project
     *
     * @return array
     */
    private function getCoreStylesScore(Project $project)
    {
        $coreStyles = [
            'Innovator',
            'Humaniser',
            'Implementer',
            'Analyser',
        ];

        $projectRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Project');
        $styles = (array) $projectRepo->getMainStyleByTeam($project->getId(), 'formatted');
        $mainStylesScores = [];
        foreach ($styles as $styleName => $point) {
            if (in_array($styleName, $coreStyles)) {
                $mainStylesScores[$styleName] = $point;
            }
        }

        return $mainStylesScores;
    }
}

<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TestBundle\Entity\Style;

/**
 * Class BrainstyleCharacteristicController
 *
 */
class BrainstyleCharacteristicController extends Controller
{
    /**
     * @param Style   $style
     * @param Request $request
     *
     * @return Response
     */
    public function getRandomCharacteristicAction(Style $style, Request $request)
    {
        $characteristicRepo = $this->get('doctrine.orm.entity_manager')->getRepository('AppBundle:BrainstyleCharacteristic');
        $characteristic = $characteristicRepo->getRandomCharacteristicByStyleAndLocale($style, $request->getLocale());

        return new Response($characteristic['text']);
    }
}

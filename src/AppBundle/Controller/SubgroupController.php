<?php

namespace AppBundle\Controller;

use AppBundle\Annotations\NoTestNoAccess;
use AppBundle\Entity\ChartImage;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectGroup;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class SubgroupController
 *
 * @package AppBundle\Controller
 */
class SubgroupController extends Controller
{

    /**
     * @param Project $project
     * @param Form    $createForm
     *
     * @Route("/dashboard/project/{id}/subgroups", name="dashboard_project_subgroups", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function projectListAction(Project $project, $createForm = null)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $this->getUser() != $project->getOwner() &&
            !$project->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        $subgroupRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:ProjectGroup');
        $subgroups = $subgroupRepo->getProjectSubgroups($project, $this->getUser());
        if (null === $createForm) {
            $form = $this->get('appbundle.form.type.project_subgroup');
            $createForm = $this->createForm($form);
        }

        return $this->render('@App/dashboard/subgroup/list.html.twig', [
                'project'    => $project,
                'subgroups'  => $subgroups,
                'createForm' => $createForm->createView(),
            ]);
    }

    /**
     * @param Form $createForm
     *
     * @Route("/dashboard/subgroups", name="dashboard_user_subgroups", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function userListAction($createForm = null)
    {
        $subgroupRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:ProjectGroup');
        $subgroups = $subgroupRepo->getUserSubgroups($this->getUser());
        if (null === $createForm) {
            $form = $this->get('appbundle.form.type.project_subgroup');
            $createForm = $this->createForm($form);
        }

        return $this->render('@App/dashboard/subgroup/list_user_subgroup.html.twig', [
            'subgroups'  => $subgroups,
            'createForm' => $createForm->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/dashboard/subgroup/create", name="dashboard_user_subgroup_create", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function createUserSubgroupAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $form = $this->get('appbundle.form.type.project_subgroup');
        $formCreate = $this->createForm($form);
        $formCreate->handleRequest($request);
        if ($formCreate->isValid()) {
            /** @var ProjectGroup $subgroup */
            $subgroup = $formCreate->getData();
            $subgroup->setProject(null);
            $subgroup->setOwner($this->getUser());

            $em->persist($subgroup);
            $em->flush();
            $userIds = $request->get('user-ids');
            if (!empty($userIds)) {
                $userIds = explode(',', $request->get('user-ids'));
                $em->getRepository('AppBundle:ProjectGroup')->addUser($userIds, $subgroup);
            }
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Subgroup was successfully created'));

            return $this->redirect($this->generateUrl('dashboard_user_subgroups'));

        } else {
            return $this->render('@App/dashboard/subgroup/create_user_subgroup.html.twig', [
                'form'    => $formCreate->createView(),
            ]);
        }
    }

    /**
     * @param Project $project
     * @param Request $request
     *
     * @Route("/dashboard/project/{project}/subgroup/create", name="dashboard_project_subgroup_create", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function createProjectSubgroupAction(Project $project, Request $request)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $this->getUser() != $project->getOwner() &&
            !$project->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $form = $this->get('appbundle.form.type.project_subgroup');
        $formCreate = $this->createForm($form);
        $formCreate->handleRequest($request);
        if ($formCreate->isValid()) {
            /** @var ProjectGroup $subgroup */
            $subgroup = $formCreate->getData();
            $subgroup->setProject($project);
            if ($this->getUser()->hasRole('ROLE_EMPLOYER') || $this->getUser()->hasRole('ROLE_MANAGER')) {
                $subgroup->setOwner($this->getUser());
            }
            $em->persist($subgroup);
            $em->flush();
            $userIds = $request->get('user-ids');
            if (!empty($userIds)) {
                $userIds = explode(',', $request->get('user-ids'));
                $em->getRepository('AppBundle:ProjectGroup')->addUser($userIds, $subgroup);
                foreach ($userIds as $userId) {
                    $subgroupUser = $em->getRepository('AppBundle:User')->find($userId);
                    if ($subgroupUser) {
                        $subgroupUser->addProjectGroup($subgroup);
                    }
                }
            }
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Subgroup was successfully created'));

            return $this->redirect($this->generateUrl('dashboard_project_subgroups', [
                'id' => $project->getId(),
            ]));

        } else {
            return $this->render('@App/dashboard/subgroup/create.html.twig', [
                'form'    => $formCreate->createView(),
                'project' => $project,
            ]);
        }
    }

    /**
     * @param ProjectGroup $projectGroup
     *
     * @Route("/dashboard/subgroup/delete/{id}", name="dashboard_subgroup_delete", options={"expose"=true})
     * @NoTestNoAccess
     * @Method("POST")
     *
     * @return Response
     */
    public function deleteAction(ProjectGroup $projectGroup)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            (($projectGroup->getProject() && $this->getUser() != $projectGroup->getProject()->getOwner() &&
                !$projectGroup->getProject()->getParticipants()->contains($this->getUser()))) ||
            $projectGroup->getOwner() && $projectGroup->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine.orm.entity_manager');
        $em->remove($projectGroup);
        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('Subgroup was successfully deleted')
        );

        if ($projectGroup->getProject() instanceof Project) {
            return $this->redirectToRoute(
                'dashboard_project_subgroups',
                [
                    'id' => $projectGroup->getProject()->getId(),
                ]
            );
        } else {
            return $this->redirectToRoute('dashboard_user_subgroups');
        }
    }

    /**
     * @param ProjectGroup $projectGroup
     *
     * @Route("/dashboard/subgroup/{id}/delete", name="dashboard_ajax_subgroup_delete", options={"expose"=true})
     * @NoTestNoAccess
     * @Method("POST")
     *
     * @return Response
     */
    public function ajaxDeleteAction(ProjectGroup $projectGroup)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            (($projectGroup->getProject() && $this->getUser() != $projectGroup->getProject()->getOwner() &&
                !$projectGroup->getProject()->getParticipants()->contains($this->getUser()))) ||
            $projectGroup->getOwner() && $projectGroup->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        try {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->remove($projectGroup);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('Subgroup was successfully deleted')
            );
        } catch (\Exception $e) {
            $this->get('logger')->addError($e);
            $this->get('session')->getFlashBag()->add(
                'error',
                $this->get('translator')->trans('Subgroup was not deleted')
            );
        }

        if ($projectGroup->getProject() instanceof Project) {
            return new JsonResponse(
                [
                    'redirect' => $this->get('router')->generate(
                        'dashboard_project_subgroups',
                        [
                            'id' => $projectGroup->getProject()->getId(),
                        ]
                    ),
                ]
            );
        } else {
            return new JsonResponse(['redirect' => $this->get('router')->generate('dashboard_user_subgroups')]);
        }
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param Request      $request
     *
     * @Route("/dashboard/subgroup/delete/{id}/users", name="dashboard_subgroup_users_delete", options={"expose"=true})
     * @NoTestNoAccess
     * @Method("POST")
     *
     * @return Response
     */
    public function deleteUsersAction(ProjectGroup $projectGroup, Request $request)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            (($projectGroup->getProject() && $this->getUser() != $projectGroup->getProject()->getOwner() &&
                !$projectGroup->getProject()->getParticipants()->contains($this->getUser()))) ||
            $projectGroup->getOwner() && $projectGroup->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        $userIds = $request->get('userIds', []);
        if (count($userIds) == 0) {
            return new JsonResponse([
                'status' => 'error',
                'message' => $this->get('translator')->trans('Action aborted No items were selected'),
            ]);
        }
        try {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->getRepository('AppBundle:ProjectGroup')
                ->deleteUsers($userIds, $projectGroup);
        } catch (Exception $e) {
            $this->get('logger')->addError($e);
            $this->get('session')->getFlashBag()->add(
                'error',
                $this->get('translator')->trans('Users from subgroup were not deleted')
            );
        }

        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param Request      $request
     *
     * @Route("/dashboard/subgroup/main-style-ajax/{id}", name="dashboard_ajax_main_style", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function ajaxSubgroupMainStyle(ProjectGroup $projectGroup, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$projectGroup->getParticipants()->isEmpty()) {
            $mainStylesScores = $this->getCoreStylesScore($projectGroup);
            $style = $this->get('testbundle.calculate_result')->calculateGroupStyle($mainStylesScores);
            $data['style'] = $style;
        } else {
            $data['style'] = null;
        }

        return new JsonResponse($data);
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param Request      $request
     *
     * @Route("/dashboard/subgroup/dimensions-ajax/{id}", name="dashboard_ajax_dimensions", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function overviewAjaxDimensionsAction(ProjectGroup $projectGroup, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            (($projectGroup->getProject() && $this->getUser() != $projectGroup->getProject()->getOwner() &&
                !$projectGroup->getProject()->getParticipants()->contains($this->getUser()))) ||
            $projectGroup->getOwner() && $projectGroup->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        $subgroupRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:ProjectGroup');
        $groupDimensions = (array) $subgroupRepo->getDimensions($projectGroup->getId(), $request->query->all());
        $avgGroup = (array) $subgroupRepo->getMainStyleBySubgroup($projectGroup->getId());
        $userCount = (array) $subgroupRepo->getDimensionsUserCount($projectGroup->getId(), $request->query->all());
        $data['data'] = $groupDimensions;
        $data['group_data'] = $avgGroup;

        if (count($avgGroup) > 0) {
            $mainStylesScores = $this->getCoreStylesScore($projectGroup);
            $groupStyle = $this->get('testbundle.calculate_result')->calculateGroupStyle($mainStylesScores);
        }

        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $groupDimensions,
            'group_data' => $avgGroup,
            'group_style' => isset($groupStyle) ? $groupStyle : '',
        ];

        return new JsonResponse($data);
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param Request      $request
     *
     * @Route("/dashboard/subgroup/web-image/{id}/", name="dashboard_subgroup_web_image_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     * @internal param User $user
     * @internal param Request $request
     */
    public function ajaxWebImagePathAction(ProjectGroup $projectGroup, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            (($projectGroup->getProject() && $this->getUser() != $projectGroup->getProject()->getOwner() &&
                !$projectGroup->getProject()->getParticipants()->contains($this->getUser()))) ||
            $projectGroup->getOwner() && $projectGroup->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        $resultStyles = [];
        $subgroupRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:ProjectGroup');
        $styles = $subgroupRepo->getMainStyleBySubgroup($projectGroup->getId());
        foreach ($styles as $styleResult) {
            $resultStyles[$styleResult['name']] = $styleResult['avg_point'];
        }

        $webImage = $this->get('appbundle.graph')->getGraph($resultStyles);
        $webImageThumbnailUrl = '';
        if ($webImage instanceof ChartImage) {
            $webPath = $this->get('vich_uploader.templating.helper.uploader_helper')
                ->asset($webImage, 'imageFile');
            $webImageThumbnailUrl = $this->get('liip_imagine.cache.manager')->getBrowserPath($webPath, 'web_path');
        }

        return new JsonResponse(['data' => ['thumbnailUrl' => $webImageThumbnailUrl]]);
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param Request      $request
     *
     * @Route("/dashboard/subgroup/{id}/users/list/", name="dashboard_subgroup_users_list_ajax", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function ajaxUserListAction(ProjectGroup $projectGroup, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            (($projectGroup->getProject() && $this->getUser() != $projectGroup->getProject()->getOwner() &&
                !$projectGroup->getProject()->getParticipants()->contains($this->getUser()))) ||
            $projectGroup->getOwner() && $projectGroup->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        $userRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User');
        $users = $userRepo->getRelatedUsersBySubgroup($projectGroup, $request->query->all());
        $userCount = (int) $userRepo->getRelatedUsersBySubgroup($projectGroup, $request->query->all());
        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $users,
        ];
        $userData = $this->get('jms_serializer')->serialize($data, 'json');

        return new Response($userData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param Request      $request
     *
     * @Route("/dashboard/subgroup/{id}/available-users", name="dashboard_get_available_users_subgroup_list_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function getAvailableUserProjectGroupAjaxAction(ProjectGroup $projectGroup, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $this->getUser() != $projectGroup->getProject()->getOwner() &&
            !$projectGroup->getProject()->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        $owner = $projectGroup->getProject()->getOwner();
        $userRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User');
        $users = $userRepo->getAvailableUserToProjectGroup($projectGroup, $owner, $request->query->all());
        $userCount = $userRepo->getAvailableUserToProjectGroupCount($projectGroup, $owner, $request->query->all());
        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $users,
        ];

        $userData = $this->get('jms_serializer')->serialize($data, 'json');

        return new Response($userData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param Request      $request
     *
     * @Route("/dashboard/subgroup/{id}/get-users", name="dashboard_get_users_subgroup_list_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function getUsersProjectGroupAjaxAction(ProjectGroup $projectGroup, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            (($projectGroup->getProject() && $this->getUser() != $projectGroup->getProject()->getOwner() &&
                !$projectGroup->getProject()->getParticipants()->contains($this->getUser()))) ||
            $projectGroup->getOwner() && $projectGroup->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        $userRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User');
        $users = $userRepo->getUserProjectGroup($projectGroup, $request->query->all());
        $userCount = $userRepo->getUserProjectGroupCount($projectGroup, $request->query->all());
        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $users,
        ];

        $userData = $this->get('jms_serializer')->serialize($data, 'json');

        return new Response($userData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param ProjectGroup $projectGroup
     * @param Request      $request
     *
     * @Route("/dashboard/subgroup/{id}/add-users", name="dashboard_subgroup_add_users_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function addUserAjaxAction(ProjectGroup $projectGroup, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            (($projectGroup->getProject() && $this->getUser() != $projectGroup->getProject()->getOwner() &&
                !$projectGroup->getProject()->getParticipants()->contains($this->getUser()))) ||
            $projectGroup->getOwner() && $projectGroup->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        $userIds = $request->get('userIds', []);

        if (count($userIds) == 0) {
            return new JsonResponse([
                'status' => 'error',
                'message' => $this->get('translator')->trans('Action aborted No items were selected'),
            ]);
        }
        $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:ProjectGroup')
            ->addUser($userIds, $projectGroup);


        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @param Request      $request
     * @param Project      $project
     * @param ProjectGroup $projectGroup
     *
     * @return Response
     *
     * @NoTestNoAccess
     * @Route("/dashboard/project/{project}/subgroup/{id}/edit", name="dashboard_project_subgroup_edit", options={"expose"=true})
     */
    public function editProjectSubgroup(Request $request, Project $project, ProjectGroup $projectGroup)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            $this->getUser() != $projectGroup->getProject()->getOwner() &&
            !$projectGroup->getProject()->getParticipants()->contains($this->getUser())) {
            throw new AccessDeniedException();
        }
        $form = $this->get('appbundle.form.type.project_subgroup');
        $form = $this->createForm($form, $projectGroup);
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $subgroup = $form->getData();
                $subgroup->setProject($project);
                $this->get('doctrine.orm.entity_manager')->persist($subgroup);
                $this->get('doctrine.orm.entity_manager')->flush();
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans('Subgroup was successfully updated')
                );
                $hash = $request->get('panel', '');
                $url = $this->generateUrl('dashboard_project_subgroups', [
                        'id' => $project->getId(),
                    ]).'#panel'.$hash;

                return $this->redirect($url);
            }
        }

        return $this->render('@App/dashboard/subgroup/edit.html.twig', [
                'form'     => $form->createView(),
                'subgroup' => $projectGroup,
                'project'   => $project,
            ]);
    }

    /**
     * @param Request      $request
     * @param ProjectGroup $projectGroup
     *
     * @return Response
     *
     * @NoTestNoAccess
     * @Route("/dashboard/subgroup/{id}/edit", name="dashboard_user_subgroup_edit", options={"expose"=true})
     */
    public function editUserSubgroup(Request $request, ProjectGroup $projectGroup)
    {
        if ($this->getUser() != $projectGroup->getOwner()) {
            throw new AccessDeniedException();
        }
        $form = $this->get('appbundle.form.type.project_subgroup');
        $form = $this->createForm($form, $projectGroup);
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $subgroup = $form->getData();
                $subgroup->setProject(null);
                $subgroup->setOwner($this->getUser());
                $this->get('doctrine.orm.entity_manager')->persist($subgroup);
                $this->get('doctrine.orm.entity_manager')->flush();
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans('Subgroup was successfully updated')
                );
                $hash = $request->get('panel', '');
                $url = $this->generateUrl('dashboard_user_subgroups').'#panel'.$hash;

                return $this->redirect($url);
            }
        }

        return $this->render('@App/dashboard/subgroup/edit_user_subgroup.html.twig', [
            'form'     => $form->createView(),
            'subgroup' => $projectGroup,
        ]);
    }

    /**
     * @param ProjectGroup $projectGroup
     *
     * @Route("/dashboard/subgroup/{id}/main-style-ajax", name="dashboard_main_style", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     */
    public function getSubgroupMainStyle(ProjectGroup $projectGroup)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            (($projectGroup->getProject() && $this->getUser() != $projectGroup->getProject()->getOwner() &&
                !$projectGroup->getProject()->getParticipants()->contains($this->getUser()))) ||
            $projectGroup->getOwner() && $projectGroup->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        if (!$projectGroup->getParticipants()->isEmpty()) {
            $mainStylesScores = $this->getCoreStylesScore($projectGroup);
            $style = $this->get('testbundle.calculate_result')->calculateGroupStyle($mainStylesScores);
        } else {
            $style = null;
        }

        return $style;
    }

    /**
     * @param ProjectGroup $projectGroup
     *
     * @Route("/dashboard/subgroup/{id}/web-image", name="dashboard_subgroup_web_image", options={"expose"=true})
     * @NoTestNoAccess
     * @return Response
     * @internal param User $user
     */
    public function getSubgroupWebImagePathAction(ProjectGroup $projectGroup)
    {
        if (!$this->getUser()->hasRole('ROLE_ADMIN') &&
            (($projectGroup->getProject() && $this->getUser() != $projectGroup->getProject()->getOwner() &&
                !$projectGroup->getProject()->getParticipants()->contains($this->getUser()))) ||
            $projectGroup->getOwner() && $projectGroup->getOwner() != $this->getUser()) {
            throw new AccessDeniedException();
        }
        $resultStyles = [];
        $subgroupRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:ProjectGroup');
        $styles = $subgroupRepo->getMainStyleBySubgroup($projectGroup->getId());
        foreach ($styles as $styleResult) {
            $resultStyles[$styleResult['name']] = $styleResult['avg_point'];
        }

        $webImage = $this->get('appbundle.graph')->getGraph($resultStyles);
        $webImageThumbnailUrl = '';
        if ($webImage instanceof ChartImage) {
            $webPath = $this->get('vich_uploader.templating.helper.uploader_helper')
                ->asset($webImage, 'imageFile');
            $webImageThumbnailUrl = $this->get('liip_imagine.cache.manager')->getBrowserPath($webPath, 'web_path');
        }

        return$webImageThumbnailUrl;
    }

    /**
     * Download PDF report for subgroup
     *
     * @Route("/dashboard/subgroup/{subgroup}/download-pdf", name="dashboard_subgroup_pdf", options={"expose"=true})
     * @NoTestNoAccess
     * @param ProjectGroup $subgroup
     * @return Response
     */
    public function downloadPdfAction(ProjectGroup $subgroup)
    {
        $subgroupStyle = $this->getSubgroupMainStyle($subgroup);
        $subgroupWeb   = $this->getSubgroupWebImagePathAction($subgroup);
        $subgroupUsers = $this->getDoctrine()->getRepository('AppBundle:User')->getProjectGroupUsersWithPassedTest($subgroup->getId());

        $html = $this->renderView('@App/dashboard/subgroup/subgroup_pdf_report.html.twig', [
            'subgroup' => $subgroup,
            'subgroup_style' => $subgroupStyle,
            'subgroup_web' => $subgroupWeb,
            'subgroup_users' => $subgroupUsers,
        ]);

        $filename = 'subgroup-report.pdf';

        $mpdfService = $this->get('tfox.mpdfport');

        $response = $mpdfService->generatePdf($html, [
            'outputDest'      => 'S',
            'outputFilename'  => $filename,
            'constructorArgs' => [
                'mode'          => 'BLANK',
                'format'        => 'A4',
                'margin_left'   => 10,
                'margin_right'  => 10,
                'margin_top'    => 10,
                'margin_bottom' => 10,
                'margin_header' => 10,
                'margin_footer' => 10,
            ],
        ]);

        return new Response($response, 200, [
            'Content-Type'        => 'application/pdf',
            'Content-Disposition' => 'attachment; filename='.$filename,
        ]);
    }

    /**
     * @param ProjectGroup $projectGroup
     *
     * @return array
     */
    private function getCoreStylesScore(ProjectGroup $projectGroup)
    {
        $coreStyles = [
            'Innovator',
            'Humaniser',
            'Implementer',
            'Analyser',
        ];

        $projectRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:ProjectGroup');
        $styles = (array) $projectRepo->getMainStyleBySubgroup($projectGroup->getId(), 'formatted');
        $mainStylesScores = [];
        foreach ($styles as $styleName => $point) {
            if (in_array($styleName, $coreStyles)) {
                $mainStylesScores[$styleName] = $point;
            }
        }

        return $mainStylesScores;
    }
}

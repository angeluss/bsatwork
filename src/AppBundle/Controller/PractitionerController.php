<?php

namespace AppBundle\Controller;

use AppBundle\Annotations\NoTestNoAccess;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class PractitionerController
 *
 * @package AppBundle\Controller
 */
class PractitionerController extends Controller
{

    /**
     * @Route("/dashboard/practitioner/list", name="dashboard_practitioner_list", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return Response
     */
    public function listAction()
    {
        return $this->render('@App/dashboard/practitioner/list.html.twig');
    }

    /**
     * @param User $user
     *
     * @Route("/dashboard/practitioner/{user}/projects", name="dashboard_practitioner_related_projects", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_ADMIN')")
     * @return Response
     */
    public function relatedProjectsAction(User $user)
    {
        return $this->render('@App/dashboard/practitioner/related-projects-list.html.twig', ['user' => $user]);
    }

    /**
     * @param Request $request
     *
     * @Route("/dashboard/practitioner/create", name="dashboard_practitioner_create", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm($this->get('appbundle.form.type.practitioner'));
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                /** @var User $practitioner */
                $practitioner = $form->getData();
                $practitioner->setRoles(['ROLE_USER', 'ROLE_PRACTITIONER', 'ROLE_APPROVED']);
                $practitioner->setEnabled(true);
                $practitioner->setApproved(true);
                $this->get('doctrine.orm.entity_manager')->persist($practitioner);
                $this->get('doctrine.orm.entity_manager')->flush();
                $this->get('session')->getFlashBag()->add('success',
                    $this->get('translator')->trans('Practitioner was successfully created')
                );

                return $this->redirectToRoute('dashboard_practitioner_list');
            }
        }

        return $this->render('@App/dashboard/practitioner/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param User    $user
     * @param Request $request
     *
     * @Route("/dashboard/practitioner/edit/{user}", name="dashboard_practitioner_edit", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return Response
     */
    public function editAction(User $user, Request $request)
    {
        $form = $this->createForm($this->get('appbundle.form.type.practitioner'), $user);
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                /** @var User $practitioner */
                $practitioner = $form->getData();
                $userManager = $this->get('fos_user.user_manager');
                $userManager->updateUser($practitioner);
                $this->get('session')->getFlashBag()->add('success',
                    $this->get('translator')->trans('Practitioner was successfully updated')
                );

                return $this->redirectToRoute('dashboard_index');
            }
        }

        return $this->render(
            '@App/dashboard/practitioner/edit.html.twig',
            [
                'form' => $form->createView(),
                'user' => $user,
            ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/dashboard/practitioner/list-data", name="dashboard_practitioner_list_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return Response
     */
    public function ajaxListAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $userRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User');
        $users = $userRepo->getPractitionerUsers($request->query->all());
        $userCount = $userRepo->getCountPractitionerUsers($request->query->all());
        $data = ['data' => $users, 'recordsTotal' => $userCount, 'recordsFiltered' => $userCount];
        $userData = $this->get('jms_serializer')->serialize($data, 'json');

        return new Response($userData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param User    $user
     * @param Request $request
     *
     * @Route("/dashboard/practitioner/{user}/list-projects", name="dashboard_practitioner_related_projects_ajax", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_ADMIN')")
     * @return Response
     */
    public function ajaxRelatedProjectAction(User $user, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $projectRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Project');
        $projects = $projectRepo->getProjectsByUser($user, $request->query->all());
        $projectCount = (int) $projectRepo->getCountByUser($user, $request->query->all());
        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $projectCount,
            'recordsFiltered' => $projectCount,
            'data' => $projects
        ];

        return new JsonResponse($data);
    }

}
<?php

namespace AppBundle\Controller;

use AppBundle\Annotations\NoTestNoAccess;
use AppBundle\Entity\ChartImage;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\UserTestResult;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use TestBundle\Entity\Style;

/**
 * Class UserController
 *
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
    /**
     * @param User    $user
     * @param Request $request
     *
     * @Route("/dashboard/user/edit/{user}", name="dashboard_user_edit", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function editAction(User $user, Request $request)
    {
        $form = $this->createForm($this->get('appbundle.form.type.user'), $user);
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                /** @var User $user */
                $user = $form->getData();
                $manager = $this->get('doctrine.orm.entity_manager');
                $manager->persist($user);
                $manager->flush();
                $this->get('session')->getFlashBag()->add('success',
                    $this->get('translator')->trans('User was successfully updated')
                );

                return $this->redirectToRoute('dashboard_index');
            }
        }

        return $this->render(
            '@App/dashboard/users/user_edit.html.twig',
            [
                'form' => $form->createView(),
                'user' => $user,
            ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/dashboard/users/list/", name="dashboard_users_list_ajax", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function ajaxListAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $userRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User');
        $users = $userRepo->getRelatedUsers($this->getUser(), $request->query->all());
        $userCount = (int) $userRepo->getRelatedUsersCount($this->getUser(), $request->query->all());
        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $users,
        ];
        $userData = $this->get('jms_serializer')->serialize($data, 'json');

        return new Response($userData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param Request $request
     *
     * @Route("/dashboard/users/general-info/list", name="dashboard_users_general_info_list", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function ajaxUserGeneralInfo(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $userRepo = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User');
        /** @var User[] $users */
        $users = $userRepo->getUsersGeneralInfo($request->query->all());
        $userCount = (int) $userRepo->getUsersGeneralInfoCount($request->query->all());
        foreach ($users as $user) {
            $user->setGender($this->get('translator')->trans($user->getGender()));
            $user->setEducationLevel($this->get('translator')->trans($user->getEducationLevel()));
            $reasons = [];
            if ($user->getReason()) {
                foreach ($user->getReason() as $reason) {
                    $reason = $this->get('translator')->trans($reason);
                    $reasons[] = $reason;
                }
            }
            $user->setReason($reasons);

            $birthYear = $user->getBirthYear();
            if (is_null($birthYear)) {
                $user->setBirthYear('');
            }
        }
        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $users,
        ];
        $userData = $this->get('jms_serializer')->serialize($data, 'json');

        return new Response($userData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/dashboard/users/", name="dashboard_users_list")
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function listAction()
    {
        return $this->render('@App/dashboard/users/list.html.twig');
    }

    /**
     * @Route("/dashboard/users/general-info", name="dashboard_users_general_info")
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function generalInfoAction()
    {
        return $this->render('@App/dashboard/users/users_general_info.html.twig');
    }

    /**
     * @param User    $user
     * @param Request $request
     *
     * @Route("/dashboard/users/web-image/{user}/", name="dashboard_users_web_image_ajax", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     * @internal param Request $request
     */
    public function ajaxWebImagePathAction(User $user, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $testResult = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:UserTestResult')
            ->getEightDimension($user);
        $webImage = $this->get('appbundle.graph')->getGraph($testResult);
        $webImageThumbnailUrl = '';
        if ($webImage instanceof ChartImage) {
            $webPath = $this->get('vich_uploader.templating.helper.uploader_helper')
                ->asset($webImage, 'imageFile');
            $webImageThumbnailUrl = $this->get('liip_imagine.cache.manager')->getBrowserPath($webPath, 'web_path');
        }

        return new JsonResponse(['data' => ['thumbnailUrl' => $webImageThumbnailUrl]]);
    }

    /**
     * @param Request $request
     *
     * @Route("/change-password/", name="user_change_password", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     * @internal param Request $request
     */
    public function userChangePassword(Request $request)
    {
        $user    = $this->getUser();

        $oldPass      = $request->request->get('old-password');
        $newPass      = $request->request->get('new-password');
        $confirmation = $request->request->get('confirmation');

        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByEmail($user->getEmail());
        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($oldPass, $user->getSalt());

        if ($encodedPass != $user->getPassword()) {
            $this->addFlash('error', $this->get('translator')->trans('Wrong old password!'));
        } else {
            if ($newPass != $confirmation) {
                $this->addFlash('error', $this->get('translator')
                                              ->trans('New password and confirmation are not equal!'));

                return $this->redirectToRoute('dashboard_index');
            }
            $user->setPlainPassword($newPass);
            $userManager->updateUser($user, true);
            $this->addFlash('success', $this->get('translator')->trans('Password was changed!'));
        }

        return $this->redirectToRoute('dashboard_index');
    }

    /**
     * @param User    $user
     * @param Request $request
     *
     * @Route("/dashboard/users/user-style/{user}", name="dashboard_user_style", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     * @internal param Request $request
     */
    public function ajaxUserStyleAction(User $user, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $style = $user->getMainStyleName();

        return $this->render('@App/dashboard/users/user_style_info.html.twig', [
            'style' => $style,
        ]);
    }

    /**
     * @param User    $user
     * @param Request $request
     *
     * @Route("/dashboard/users/user-info/{user}", name="dashboard_user_info", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     * @internal param Request $request
     */
    public function ajaxUserInfoAction(User $user, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        return $this->render('@App/dashboard/users/user_info.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @param User $user
     *
     * @Route("/dashboard/users/{user}/web", name="user_web", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function userWebAction(User $user)
    {
        $testResult = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:UserTestResult')
            ->getEightDimension($user);
        $webImage = $this->get('appbundle.graph')->getGraph($testResult);
        $webImageThumbnailUrl = '';
        if ($webImage instanceof ChartImage) {
            $webPath = $this->get('vich_uploader.templating.helper.uploader_helper')
                ->asset($webImage, 'imageFile');
            $webImageThumbnailUrl = $this->get('liip_imagine.cache.manager')->getBrowserPath($webPath, 'web_path');
        }

        return $this->render('@App/dashboard/users/user_report_info.html.twig', [
            'user'  => $user,
            'image' => $webImageThumbnailUrl,
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/dashboard/users/report", name="user_report", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function userPdfReportAction(Request $request)
    {
        $rootDir = $this->get('kernel')->getRootDir();
        $locale = strtoupper($request->getLocale());
        $userStyle = $this->getUser()->getMainStyleName();

        $filename = $rootDir.'/../src/AppBundle/Resources/reports/'.$locale.'/'.$userStyle.'.pdf';

        $content = file_get_contents($filename);

        $response = new Response();
        $response->headers->set('Content-type', 'application/force-download');
        $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $userStyle.'-report.pdf'));
        $response->setContent($content);

        return $response;
    }

    /**
     * @Route("/dashboard/all-colleagues", name="user_all_colleagues", options={"expose"=true})
     * @NoTestNoAccess
     *
     * @return Response
     */
    public function allColleaguesAction()
    {
        return $this->render('@App/dashboard/users/all_colleagues.twig');
    }

    /**
     * @Route("/dashboard/all-colleagues/list", name="ajax_all_colleagues_list", options={"expose"=true})
     * @NoTestNoAccess
     * @param Request $request
     *
     * @return Response
     */
    public function ajaxColleaguesListAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        /** @var UserRepository $userRepo */
        $userRepo = $em->getRepository('AppBundle:User');
        $projectsCollection = $user->getProjects();
        $projectNames = [];
        foreach ($projectsCollection as $project) {
            $projectNames[] = $project->getName();
        }
        $users = $userRepo->getUsersByCompanyAndPractitioner(
            $projectNames,
            $request->query->all()
        );
        $userCount = (int) $userRepo->getUsersByCompanyAndPractitionerCount(
            $projectNames,
            $request->query->all()
        );

        $data = [
            'draw' => $request->get('draw', 0) + 1,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $users,
        ];
        $userData = $this->get('jms_serializer')->serialize($data, 'json');

        return new Response($userData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/dashboard/create-user", name="user_create", options={"expose"=true})
     * @param Request $request
     *
     * @return Response
     */
    public function createUser(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->get('appbundle.form.type.create_user');
        $form = $this->createForm($form);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $user->setPlainPassword('qwerty12345');
            $user->setEnabled(true);
            $user->setApproved(true);
            $user->setTestPass(true);
            $userType = $form->get('userType')->getData();
            $user->addRole(User::getRoleByType($userType));
            if ($this->getUser()->hasRole('ROLE_PRACTITIONER')) {
                $user->addPractitioners($this->getUser());
            }
            $styles = $em->getRepository('TestBundle:Style')->getMainStyles();
            $points = [];
            foreach ($styles as $style) {
                $point = $request->get($style->getName());
                $testResult = new UserTestResult($point, $user, $style);
                $em->persist($testResult);
                $points[$style->getName()] = $point;
            }
            $style = $this->get('testbundle.calculate_result')->calculateGroupStyle($points);
            if ($style) {
                $userStyle = $em->getRepository('TestBundle:Style')->findOneBy(['name' => $style]);
                $user->setMainStyle($userStyle);
            }
            try {
                $em->persist($user);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans('User was successfully created')
                );
            } catch (\Exception $e) {
                $this->get('logger')->addError($e);
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $this->get('translator')->trans('User was not created')
                );
            }

            return $this->redirectToRoute('dashboard_users_list');
        }

        return $this->render('@App/dashboard/users/create_user.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

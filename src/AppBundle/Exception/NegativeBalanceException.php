<?php

namespace AppBundle\Exception;


/**
 * Class NegativeBalanceException
 *
 * @package AppBundle\Exception
 */
class NegativeBalanceException extends \Exception
{

}
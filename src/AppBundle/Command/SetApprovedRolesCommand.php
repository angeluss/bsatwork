<?php
namespace AppBundle\Command;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SetApprovedRolesCommand
 *
 * @package AppBundle\Command
 */
class SetApprovedRolesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('users:set-approved-role')->setDescription('Set approved role');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $users = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository('AppBundle:User')->findAll();
        /** @var User $user */
        foreach ($users as $user) {
            if ($user->hasRole('ROLE_ADMIN') || $user->hasRole('ROLE_PRACTITIONER')) {
                $user->setApproved(true);
            }
            if ($user->isApproved() && !$user->hasRole('ROLE_APPROVED')) {
                $user->addRole('ROLE_APPROVED');
            }
        }
        $this->getContainer()->get('doctrine.orm.entity_manager')->flush();
    }
}

<?php
namespace AppBundle\Mailer;

use AppBundle\Entity\User;
use FOS\UserBundle\Mailer\TwigSwiftMailer as BaseMailer;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\Translator;

/**
 * Class Mailer
 */
class Mailer extends BaseMailer
{
    /**
     * @var string $sender
     */
    public $sender;

    /**
     * @param \Swift_Mailer         $mailer
     * @param UrlGeneratorInterface $router
     * @param \Twig_Environment     $twig
     * @param array                 $parameters
     * @param Translator            $translator
     */
    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface $router, \Twig_Environment $twig, array $parameters, $translator)
    {
        parent::__construct($mailer, $router, $twig, $parameters);
        $key = key($this->parameters['from_email']['resetting']);
        $this->sender[$key] = $translator->trans($this->parameters['from_email']['resetting'][$key]);
    }

    /**
     * @param UserInterface $user
     */
    public function sendWelcomeEmailMessage(UserInterface $user)
    {
        $template = 'AppBundle:emails:welcome.html.twig';

        $context = array(
            'user' => $user,
        );

        $this->sendMessage($template, $context, $this->sender, $user->getEmail());
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $phone
     */
    public function sendContactUsMessage($name, $email, $phone)
    {
        $template = 'AppBundle:emails:contact_us.html.twig';
        $address = 'sandra.minnee@a-mare.nl';

        $context = [
            'email' => $email,
            'name' => $name,
            'phone' => $phone,
        ];

        $this->sendMessage($template, $context, $this->sender, $address);
    }

    /**
     * @param User $user
     */
    public function sendAccountApproveMessage(User $user)
    {
        $template = 'AppBundle:emails:account_approve.html.twig';

        $this->sendMessage($template, [], $this->sender, $user->getEmail());
    }

    /**
     * @param UserInterface $user
     */
    public function sendResettingEmailMessage(UserInterface $user)
    {
        $template = 'AppBundle:emails:password_resset.html.twig';
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), true);

        $context = array(
            'user' => $user,
            'confirmationUrl' => $url,
        );

        $this->sendMessage($template, $context, $this->sender, $user->getEmail());
    }
}

<?php

namespace AppBundle\Annotations;

/**
 * Class NoTestNoAccess
 * @Annotation
 */
class NoTestNoAccess
{
    public $access;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->access = true;
    }
}

<?php
namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class OrderStatusType
 */
class OrderStatusType extends AbstractEnumType
{
    const STATUS_NEW  = 0;
    const STATUS_PAID = 1;

    protected static $choices = [
        self::STATUS_NEW  => 'New',
        self::STATUS_PAID => 'Paid',
    ];
}
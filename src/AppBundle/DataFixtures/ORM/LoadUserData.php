<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadUserData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadUserData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $users = [
            [
                'name' => 'admin',
                'pass' => 'qwerty',
                'role' => 'ROLE_ADMIN',
            ]
        ];

        $eduLevels = [
            'primary school',
            'junior high school',
            'senior high school',
            'junior college',
            'senior college',
            'Master of Arts (MA) or Master of Science (MS) or equivalent',
            'Doctoral thesis and degree (Phd)'
        ];

        $reasons = [
            'to get an idea of the type of jobs that would suit me',
            'to gain more insight in myself',
            'to develop myself',
            'to prepare for a job interview or an assessment'
        ];

        $genders = [
            'male',
            'female'
        ];

        foreach ($users as $key => $user) {
            $nUser = new User();
            $nUser->setEmail($user['name'].'@brainstyle.com');
            $nUser->setFirstName('first_name_'.$user['name']);
            $nUser->setLastName('last_name_'.$user['name']);
            $nUser->setPlainPassword($user['pass']);
            shuffle($genders);
            $nUser->setGender($genders[0]);
            shuffle($eduLevels);
            $nUser->setEducationLevel($eduLevels[0]);
            shuffle($reasons);
            $nUser->setReason($reasons);
            $nUser->setBirthYear(rand(1950, 1996));
            $nUser->addRole($user['role']);
            if ($user['role'] == 'ROLE_PRACTITIONER') {
                $nUser->setCompanyName('company'.$user['name']);
            }
            $nUser->setEnabled(true);
            $nUser->setApproved(true);
            $manager->persist($nUser);
            $this->addReference($user['name'].'-user', $nUser);
        }
        $manager->flush();

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 30;
    }
}
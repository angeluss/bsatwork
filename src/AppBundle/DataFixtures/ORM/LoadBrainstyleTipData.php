<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\BrainstyleTip;
use AppBundle\Entity\Translation\BrainstyleTipTranslation;

/**
 * Class LoadBrainstyleTipData
 */
class LoadBrainstyleTipData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $tipsEn = [
            'Innovator' => [
                'Adopt a positive attitude: open yourself up to suggestions and add your ideas or opinion',
                'Think more in AND than in OR',
                'Assume that there are always multiple solutions to a problem, there is never just one way',
                'Be curious about new information, opinions or ways of doing something',
                'Try something new, experiment, and pay attention to what you are experiencing',
                'Make a conscious effort with others when there is a problem or challenge',
                'Let go of your preconceived goal or structure during a conversation, react with the first thing that comes to mind and see what happens',
                'In your next meeting, try to sit somewhere other than your usual sport, sit next to other colleagues and take part of the meeting from this different perspective',
                'Give yourself time and space: plan a half day per month without appointments, to think about what you want to change in your work',
                'Stimulate your creativity, create a collage, or visualise how you would like to work in 3 years time'
            ],
            'Humaniser' => [
                'Deepen your relationships by giving a sincere compliment when you`re satisfied with a delivered performance',
                'Get involved: Refer back to a personal theme with your colleague or conversational partner before you discuss business',
                'Colleagues often appreciate personal contact: just pop round their office, instead of sending an email',
                'You will be even more effective if you know what your closest colleagues find important: get to know them, and ask them about their hobbies and what they find important in their work ',
                'Have a chat at the coffee machine, with whomever is there, and enjoy the conversation',
                'Pay attention to (your) feelings: if you feel stressed or angry, take it seriously and find out what it is that makes you feel that way',
                'Identify your feelings and discuss them, that`s how you maintain good relationships and solve issues between people ',
                'Do things together when you would have preferred to do them on your own and see where it gets you'
            ],
            'Implementer' => [
                'Make clear agreements with people and respect these',
                'Explain when and why you can’t keep to made agreements or rules',
                'Design a clear filing system for all your projects, ideas and activities and stick to it',
                'Take time to organise your desk and cabinets and keep them neat',
                'Prepare any meeting or project properly',
                'Make an overview of all activities of a project in advance',
                'After completing a project, have a look at the activities you`ve actually realised ',
                'Evaluate projects and keep the list with points to improve on for the next project',
                'Take a fixed time every day to answer your email or calls ',
                'Make a to-do list every morning and cross off what you`ve done, and move on to the rest the next day',
                'Keep your peace of mind by doing the necessary computer checks, updates, backups and security checks'
            ],
            'Analyser' => [
                'First decide on what you want to say, then start talking',
                'When you talk to someone, get to the point quickly, summarise what you want to say and use only half the number of words you would otherwise use',
                'Found your own claims with hard facts (numbers, percentages)',
                'Make a list of things that you or others have to do, shouldn’t do, or could improve on',
                'Put the pros and cons of a situation or decision together and weigh them against each other',
                'If you feel you are overwhelmed by work, try and figure out why that is',
                'Occasionally take some distance from your work or an emotional situation',
                'Think in advance what you want as an outcome or output of a consultation or negotiation ',
                'Take some time alone to think',
                'Create templates for frequently recurring texts and spread sheets for statistical summaries'
            ]
        ];

        $tipsNl = [
            'Innovator' => [
                'Neem een Ja, en…houding aan: stel je open voor suggesties en voeg daar jouw idee of mening aan toe',
                'Denk meer in EN-EN dan in OF-OF',
                'Ga ervan uit dat er altijd meerdere oplossingen voor een probleem zijn, er is nooit maar 1 weg',
                'Wees nieuwsgierig naar nieuwe informatie, opinies of manieren om iets te doen',
                'Probeer eens iets nieuws uit, experimenteer, en let op wat je ervaart ',
                'Maak bewust tijd om met anderen te brainstormen over een probleem of uitdaging',
                'Ga tijdens een volgende vergadering op een andere plek, naast andere collega’s zitten en overleg eens vanuit dit perspectief ',
                'Laat tijdens een gesprek je vooropgezette doel of structuur los, reageer met het eerste wat in je opkomt, en kijk wat er gebeurt',
                'Geef jezelf tijd en ruimte: plan een dagdeel per maand zonder afspraken, om na te denken over wat je in het werk wilt veranderen',
                'Prikkel je creativiteit, maak een collage, of visualiseer hoe je je werk graag zou willen hebben'
            ],
            'Humaniser' => [
                'Verdiep relaties door, als je tevreden bent over een geleverde prestatie, een welgemeend compliment uit te spreken',
                'Toon je betrokkenheid: kom eerst nog even terug op een persoonlijk thema van je gesprekspartner voordat je de inhoud in gaat ',
                'Collega’s stellen persoonlijk contact vaak op prijs: loop even langs, in plaats van een mail te sturen ',
                'Je wordt nog effectiever als je weet wat je naaste collega’s belangrijk vinden: verdiep je in hen, en vraag ze eens wat zij leuk en belangrijk vinden in het werk',
                'Maak een praatje bij het koffieapparaat, wie er ook staat, en beleef plezier aan de ontmoeting op zichzelf',
                'Besteed aandacht aan (je) gevoelens: als je je gestressed voelt of boos, neem dit dan serieus en ga na waardoor dat komt',
                'Benoem je gevoelens en spreek ze uit, zo houd je relaties goed en los je problemen tussen mensen op',
                'Doe dingen samen, waar je ze eigenlijk liever alleen zou doen en kijk wat dit je oplevert'
            ],
            'Implementer' => [
                'Maak heldere afspraken met mensen en respecteer deze',
                'Leg uit wanneer en waarom je je niet aan gemaakte afspraken of regels kunt houden',
                'Ontwerp een duidelijk archiefsysteem voor al je projecten, ideeën en werkzaamheden en houd je eraan',
                'Neem de tijd om je bureau en kasten eens uit te ruimen en te ordenen',
                'Bereid een vergadering, project of bijeenkomstbijeenkomst goed voor',
                'Maak van tevoren een overzicht van alle activiteiten van een project',
                'Ga na afloop van een project na welke activiteiten je daadwerkelijk hebt gerealiseerd',
                'Evalueer projecten en pak de lijst met verbeterpunten er bij het volgende project daadwerkelijk bij',
                'Neem elke dag een vast tijdstip om je mail of telefoontjes te beantwoorden ',
                'Maak elke ochtend een to-do lijst en kruis af wat je hebt gedaan, en schuif de overige door naar morgen',
                'Organiseer je eigen rust door de nodige computer checks, updates, back ups en beveiliging aan te brengen'
            ],
            'Analyser' => [
                'Bedenk eerst wat je wilt zeggen, ga dan pas praten',
                'Als je praat: kom snel ‘to the point’, vat samen wat je wilt zeggen en gebruik voor je gevoel hooguit de helft van het aantal woorden dat je anders zou gebruiken',
                'Staaf je eigen beweringen met harde feiten (cijfers, percentages)',
                'Maak lijstjes met dingen die jij of anderen moeten doen, laten, of verbeteren',
                'Zet de voors en tegens van een situatie of beslissing naast elkaar en weeg ze tegen elkaar af',
                'Als je voelt dat je overbelast raakt, zet dan even op een rijtje hoe je denkt dat dat komt',
                'Neem af en toe meer afstand van je werk of een emotionele situatie',
                'Denk van tevoren na wat je als uitkomst of opbrengst wilt hebben van een overleg of onderhandeling ',
                'Neem wat tijd alleen, om na te denken',
                'Maak templates voor vaak terugkomende teksten en speadsheets voor cijfermatige overzichten'
            ]
        ];

        foreach ($tipsNl as $key => $nlTip) {
            foreach ($nlTip as $index => $tip) {
                $brainTip = new BrainstyleTip();
                $brainTip->setText($tip);
                $style = $this->getReference('style-' . $key);
                $brainTip->setStyle($style);
                $eng = new BrainstyleTipTranslation('en', 'text', $tipsEn[$key][$index]);
                $brainTip->addTranslation($eng);
                $manager->persist($eng);
                $manager->persist($brainTip);
           }
        }
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}
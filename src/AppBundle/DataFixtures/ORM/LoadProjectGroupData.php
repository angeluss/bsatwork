<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectGroup;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadUserData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadProjectGroupData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
//        for ($i = 2; $i < 12; $i++) {
//            /** @var Project $project */
//            $project = $this->getReference('project_' . $i);
//            $subCount = rand(1, 3);
//            for ($j = 0; $j <= $subCount; $j++) {
//                $subgroup = new ProjectGroup();
//                $subgroup->setName($project->getName() . '_group_' . $j);
//                $subgroup->setProject($project);
//                $projectUsers = $project->getParticipants()->toArray();
//                $userCount = rand(0, count($projectUsers) - 1);
//                for ($ui = 0; $ui <= $userCount; $ui++) {
//                    if ($projectUsers[$ui]->hasRole('ROLE_MANAGER') || $projectUsers[$ui]->hasRole('ROLE_EMPLOYER')) {
//                        $subgroup->addParticipants($projectUsers[$ui]);
//                    }
//                }
//
//                $manager->persist($subgroup);
//            }
//
//        }
//        $manager->flush();

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 51;
    }
}
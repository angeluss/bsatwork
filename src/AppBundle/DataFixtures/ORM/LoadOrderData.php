<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Balance;
use AppBundle\Entity\Order;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadOrderData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadOrderData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
//        for ($i = 2; $i <= 14; $i++) {
//            $user = $this->getReference('practitioner' . $i . '-user');
//            $count = rand(1, 3);
//            $balance = $user->getBalance();
//            for ($j = 1; $j <= $count; $j++) {
//                $order = new Order();
//                $order->setUser($user);
//                $order->setAmount(rand(1, 20));
//                $order->setBalance($balance);
//                $order->setStatus(rand(0, 1));
//                $balance->setUser($user);
//                $balance->setOrders(new ArrayCollection([$order]));
//                $balance->setAmountUsers($balance->getAmountUsers() + $order->getAmount());
//                $manager->persist($order);
//            }
//            $manager->persist($balance);
//        }
//        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 40;
    }
}
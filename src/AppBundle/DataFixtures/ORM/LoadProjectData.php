<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadUserData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadProjectData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
//        $practitioners = $this->getReference('practitioner-user');
//        $project = new Project();
//        $project->setTeam('team_1');
//        $project->setUsersActivated(false);
//        $project->setHash(uniqid());
//        $project->setAmountNewUsers(5);
//        $project->addParticipants($this->getReference('employer-user'));
//
//        $project->setOwner($practitioners);
//        /** @var User $user */
//        $user = $this->getReference('employer-user');
//        $user->addPractitioners($this->getReference('practitioner-user'));
//        for ($i = 1; $i <= 12; $i++) {
//            /** @var User $user */
//            $user = $this->getReference('employer' . $i . '-user');
//            $user->addPractitioners($this->getReference('practitioner-user'));
//            $project->addParticipants($this->getReference('employer' . $i . '-user'));
//        }
//
//        $project->setName('test_1');
//        $manager->persist($project);
//
//        $project = new Project();
//        $project->setTeam('team_2');
//        $project->setUsersActivated(false);
//        $project->setHash(uniqid());
//        $project->setAmountNewUsers(0);
//        $project->addParticipants($this->getReference('manager-user'));
//        $project->setOwner($practitioners);
//        /** @var User $owner */
//        $user = $this->getReference('manager-user');
//        $user->addPractitioners($this->getReference('practitioner-user'));
//        $project->setName('test_2');
//        $manager->persist($project);
//
//        for ($i = 2; $i < 12; $i++) {
//            $project = new Project();
//            $project->setTeam('team_' . $i);
//            $project->setUsersActivated(false);
//            $project->setHash(uniqid());
//            $project->setAmountNewUsers(0);
//            $project->addParticipants($this->getReference('manager-user'));
//            $project->setOwner($practitioners);
//            /** @var User $owner */
//            $user = $this->getReference('manager-user');
//            $user->addPractitioners($this->getReference('practitioner-user'));
//            $project->addParticipants($this->getReference('employer' . $i . '-user'));
//            $project->setName('test_' . $i);
//            $manager->persist($project);
//            $this->addReference('project_'. $i, $project);
//        }
//
//        $project = new Project();
//        $project->setName('test_' . rand(100, 200));
//        $project->setUsersActivated(false);
//        $project->setTeam('team_' . rand(100, 200));
//        $project->setHash(uniqid());
//        $project->setAmountNewUsers(0);
//        $project->addParticipants($arturUser = $this->getReference('artur-user'));
//        $project->setOwner($practitioners);
//
//        $manager->flush();

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 50;
    }
}
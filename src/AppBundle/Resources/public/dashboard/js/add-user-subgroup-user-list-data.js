(function ($) {
    var all_checked = [];
    $('a[data-reveal-id="user-add-modal"]').on('click', function () {
        var table = $('#add-users-table').dataTable({
            "ajax": Routing.generate('ajax_all_colleagues_list'),
            "processing": true,
            "serverSide": true,
            "aLengthMenu": [[5, 10, 50], [5, 10, 50]],
            "iDisplayLength": 5,
            destroy: true,
            "columns": [
                {"data": "company_names"},
                {"data": "company_names"},
                {"data": "team_names"},
                {"data": "user_type"},
                {"data": "first_name"},
                {"data": "last_name"},
                {"data": "main_style"}
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "render": function (data, type, row) {
                        return '<input type="checkbox" value="' + row.DT_RowId + '">';
                    }
                }
            ],
            "fnDrawCallback": function (oSettings) {
                $.each(all_checked, function (index, value) {
                    $("#add-users-table input[value='" + value + "']").prop('checked', true);
                });

            }

        });
        $('#warning-alert').hide();
        $('#user-added-info').hide();
    });
    $(document).on("click", "#add-user-submit", function () {
        var checked = $('#add-users-table').find(':checked');
        $.ajax({
            'type': 'POST',
            'url': Routing.generate('dashboard_subgroup_add_users_ajax', {'id': subgroup}),
            'data': {'userIds': all_checked}
        }).success(function (msg) {
            if (msg.status == 'ok') {
                $('#user-added-info').show();
                $('[data-reveal]').foundation('reveal', 'close');
                $('.reveal-modal-bg').hide();
            }

        }).error(function (msg) {
                if (msg.status == 'error') {
                    $('#warning-alert-test').text('Something gone wrong');
                    $('#warning-alert').show();

                }
            }
        );

    });

    $(document).on('change', "#add-users-table input[type='checkbox']", function () {
        if (this.checked) {
            if (all_checked.indexOf($(this).val()) < 0) {
                all_checked.push($(this).val())
            }
        } else {
            var index = all_checked.indexOf($(this).val());
            if (index > -1) {
                all_checked.splice(index, 1);
            }
        }
    });

})(jQuery);
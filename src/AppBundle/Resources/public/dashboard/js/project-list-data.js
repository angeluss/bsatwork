(function ($) {
    $('#project-table').dataTable({
        "ajax": Routing.generate('dashboard_ajax_projects_list'),
        "processing": true,
        "serverSide": true,
        "columns": [
            {"data": "name"},
            {"data": "team"},
            {"data": "userCount"},
            {"data": "userCount"}
        ],
        "fnDrawCallback": function( oSettings ) {
            $(document).foundation();
        },
        "columnDefs": [
            {
                "targets": -1,
                "render": function (data, type, row) {
                    return '<a  href="'+ Routing.generate('dashboard_projects_info', {'project': row.DT_RowId})+'" class="button tiny bg-light-green round web-show"><i class="fontello-wrench">Show</i></a>';
                }
            }
        ]

    });
})(jQuery);
(function ($) {
    $('#order-table').dataTable({
        "ajax": Routing.generate('dashboard_order_list_ajax'),
        "processing": true,
        "serverSide": true,
        "order": [[5, "asc"], [0, "desc"]],
        "columns": [
            {"data": "created"},
            {"data": "firstName"},
            {"data": "lastName"},
            {"data": "companyName"},
            {"data": "amount"},
            {"data": "status"}
        ],
        "columnDefs": [
            {
                "targets": -1,
                "render": function (data, type, row) {
                    if (row.status == 0) {
                        return '<button data-reveal-id="change-status-modal" class="tiny bg-orange round web-show paid">Unpaid</button>';
                    } else {
                        return '<button data-reveal-id="change-status-modal" class="tiny bg-light-green round web-show paid">Paid</button>';

                    }
                }
            }
        ]

    });

    $(document).on("click", ".paid", function () {
        button = $(this);
        status = $(this).hasClass('bg-orange') ? 1 : 0;
        order = $(this).closest("tr").attr('id');
    });

    $(document).on("click", ".status-change", function () {
        $.ajax({
            context: this,
            url: Routing.generate('dashboard_change_status_ajax', {'order': order}),
            type: "POST",
            data: {'status': status}
        })
            .success(function (msg) {
                if (msg.status == 'ok') {
                    if (button.hasClass('bg-orange')) {
                        button.removeClass('bg-orange');
                        button.addClass('bg-light-green');
                        button.text('Paid');
                    } else {
                        button.removeClass('bg-light-green');
                        button.addClass('bg-orange');
                        button.text('Unpaid');
                    }
                    $('#change-status-modal').foundation('reveal', 'close');
                    $('.reveal-modal-bg').remove();
                }
            });
    });

    $('.close-confirm-modal').on('click', function(){
        $('.close-reveal-modal').click();
    });

})(jQuery);

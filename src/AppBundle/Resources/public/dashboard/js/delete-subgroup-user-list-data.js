(function ($) {
    var delete_checked = [];

    $('a[data-reveal-id="user-delete-modal"]').on('click', function () {
        var table = $('#delete-users-table').dataTable({
            "ajax": Routing.generate('dashboard_get_users_subgroup_list_ajax', {'id': subgroup}),
            "processing": true,
            "serverSide": true,
            "aLengthMenu": [[5, 10, 50], [5, 10, 50]],
            "iDisplayLength": 5,
            destroy: true,
            "columns": [
                {"data": "company_names"},
                {"data": "company_names"},
                {"data": "team_names"},
                {"data": "user_type"},
                {"data": "first_name"},
                {"data": "last_name"},
                {"data": "main_style"}
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "render": function (data, type, row) {
                        return '<input type="checkbox" value="' + row.DT_RowId + '">';
                    }
                }
            ],
            "fnDrawCallback": function (oSettings) {
                $.each(delete_checked, function (index, value) {
                    $("#delete-users-table input[value='" + value + "']").prop('checked', true);
                });

            }

        });
        $('#warning-alert').hide();
        $('#user-added-info').hide();
    });

    $('#confirm-remove').on('click', function() {
        $.ajax({
            'type': 'POST',
            'url': Routing.generate('dashboard_ajax_subgroup_delete', {'id': subgroup})
        }).success(function (data) {
            if (data.redirect) {
                window.location.href = data.redirect;
            }
        });
    });

    $(document).on("click", "#delete-user-submit", function () {
        var checked = $('#delete-users-table').find(':checked');
        $.ajax({
            'type': 'POST',
            'url': Routing.generate('dashboard_subgroup_users_delete', {'id': subgroup}),
            'data': {'userIds': delete_checked}
        }).success(function (msg) {
            if (msg.status == 'ok') {
                $('#user-deleted-info').show();
                $('[data-reveal]').foundation('reveal', 'close');
                $('.reveal-modal-bg').hide();
            }

        }).error(function (msg) {
                if (msg.status == 'error') {
                    $('#warning-alert-test').text('Something gone wrong');
                    $('#warning-alert').show();

                }
            }
        );

    });

    $(document).on('change', "#delete-users-table input[type='checkbox']", function () {
        if (this.checked) {
            if (delete_checked.indexOf($(this).val()) < 0) {
                delete_checked.push($(this).val())
            }
        } else {
            var index = delete_checked.indexOf($(this).val());
            if (index > -1) {
                delete_checked.splice(index, 1);
            }
        }
    });

})(jQuery);
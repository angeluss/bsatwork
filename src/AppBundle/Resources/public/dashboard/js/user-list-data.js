(function ($) {
    $('#users-table').dataTable({
        "ajax": Routing.generate('dashboard_users_list_ajax'),
        "processing": true,
        "serverSide": true,
        "columns": [
            {"data": "company_names"},
            {"data": "team_names"},
            {"data": "user_type"},
            {"data": "first_name"},
            {"data": "last_name"},
            {"data": "main_style"},
            {"data": "main_style"}
        ],
        "columnDefs": [
            {
                "targets": -2,
                "render": function (data, type, row) {
                    if (row.test_pass) {
                        return '<a href="#" class="user-style">'+data+'</a>';
                    } else {
                        return 'The test is not passed';
                    }
                }
            },
            {
                "targets": -1,
                "render": function (data, type, row) {
                    if (row.test_pass) {
                        return '<a href="#" data-reveal-id="user-web-image-modal" class="expand button tiny bg-light-green round web-show user-web" data-reveal-ajax="false"><i class="fontello-chart-pie">Open</i></a>';
                    } else {
                        return 'The test is not passed';
                    }
                }
            }
        ]

    });

    $(document).on("click", ".user-web", function () {
        var user = $(this).closest("tr").attr('id');
        $('#web-image-modal').foundation('reveal', 'open');
        $('#loader').show();
        $('#web-image-graph').hide();
        $.ajax({
            url: Routing.generate('dashboard_users_web_image_ajax', {'user': user})
        })
            .success(function (msg) {
                var image_graph = $('#web-image-graph');
                image_graph.attr('src', msg.data.thumbnailUrl);
                $('#loader').hide();
                image_graph.show();
            });
    });

    $(document).on('click', '.user-style', function (e) {
        e.preventDefault();
        var user = $(this).closest("tr").attr('id');
        var route = Routing.generate('dashboard_user_style', {'user': user});
        $.ajax({
            'type': 'POST',
            'url': route
        }).success(function (html) {
            $('#user-style-modal .row .small-centered').empty();
            $('#user-style-modal .row .small-centered').append(html);
            $('#user-style-modal').foundation('reveal', 'open');
        });
    });

    $(document).on('click', '.user-info', function (e) {
        e.preventDefault();
        var user = $(this).closest("tr").attr('id');
        var route = Routing.generate('dashboard_user_info', {'user': user});
        $.ajax({
            'type': 'POST',
            'url': route
        }).success(function (html) {
            $('#user-info-modal .row .small-centered').empty();
            $('#user-info-modal .row .small-centered').append(html);
            $('#user-info-modal').foundation('reveal', 'open');
        });
    });

})(jQuery);
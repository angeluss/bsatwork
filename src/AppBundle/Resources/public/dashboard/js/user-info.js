(function ($) {
    $(document).ready(function () {
        setImage();
        equalHeight('.panel');
        equalHeight('.tips');
    });

    function setImage() {
        var route = Routing.generate('dashboard_users_web_image_ajax', {'user': user});
        $.ajax({
            url: route
        })
            .success(function (msg) {
                if (msg.data.thumbnailUrl) {
                    thumbnailUrl = msg.data.thumbnailUrl;
                    $('.user-image').attr('src', thumbnailUrl);
                    $('.user-web-thumbnail').show();
                }
            }
        );
    }

    function equalHeight(elementClass) {
        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = new Array(),
            $el,
            topPosition = 0;

        $(elementClass).each(function() {

            $el = $(this);
            topPosition = $el.position().top;

            if (currentRowStart != topPosition) {

                // we just came to a new row.  Set all the heights on the completed row
                for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }

                // set the variables for the new row
                rowDivs.length = 0; // empty the array
                currentRowStart = topPosition;
                currentTallest = $el.height();
                rowDivs.push($el);

            } else {

                // another div on the current row.  Add it to the list and check if it's taller
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);

            }

            // do the last row
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }

            });
    }

    $(document).on("click", ".user-image", function () {
        $('#user-web-image-modal').foundation('reveal', 'open');
        $('#user-image-loader').show();
        $('#user-web-image-graph').hide();
        var image_graph = $('#user-web-image-graph');
        image_graph.attr('src', thumbnailUrl);
        $('#user-image-loader').hide();
        image_graph.show();
    });

    $(document).on("click", ".profile-open", function () {
        $('#user-web-image-modal').foundation('reveal', 'open');
        $('#user-image-loader').show();
        $('#user-web-image-graph').hide();
        var image_graph = $('#user-web-image-graph');
        image_graph.attr('src', thumbnailUrl);
        $('#user-image-loader').hide();
        image_graph.show();
    });

    $('.close-confirm-modal').on('click', function(){
        $('.close-reveal-modal').click();
    });
})(jQuery);
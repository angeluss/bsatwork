(function ($) {
    var all_checked = [];
    var table = $('#add-users-table').dataTable({
        "ajax": Routing.generate('dashboard_get_project_users_list_ajax', {'project': project}),
        "processing": true,
        "serverSide": true,
        "aLengthMenu": [[5, 10, 50], [5, 10, 50]],
        "iDisplayLength": 5,
        destroy: true,
        "columns": [
            {"data": "company_names"},
            {"data": "company_names"},
            {"data": "team_names"},
            {"data": "user_type"},
            {"data": "first_name"},
            {"data": "last_name"},
            {"data": "main_style"}
        ],
        "columnDefs": [
            {
                "targets": 0,
                "render": function (data, type, row) {
                    return '<input type="checkbox" value="' + row.DT_RowId + '">';
                }
            }
        ],
        "fnDrawCallback": function (oSettings) {
            $.each(all_checked, function (index, value) {
                $("#add-users-table input[value='" + value + "']").prop('checked', true);
            });

        }

    });



    $(document).on('change', "#add-users-table input[type='checkbox']", function () {
        if (this.checked) {
            if (all_checked.indexOf($(this).val()) < 0) {
                all_checked.push($(this).val())
            }
        } else {
            var index = all_checked.indexOf($(this).val());
            if (index > -1) {
                all_checked.splice(index, 1);
            }
        }
        $('#user-ids').val(all_checked);
    });

})(jQuery);

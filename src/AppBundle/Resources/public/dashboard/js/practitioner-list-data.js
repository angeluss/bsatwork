(function ($) {
    $('#practitioner-table').dataTable({
        "ajax": Routing.generate('dashboard_practitioner_list_ajax'),
        "processing": true,
        "serverSide": true,
        "columns": [
            {"data": "firstName"},
            {"data": "lastName"},
            {"data": "companyName"},
            {"data": "projectCount"},
            {"data": "usersCount"},
            {"data": "usersCount"}
        ],
        "columnDefs": [
            {
                "targets": -1,
                "render": function (data, type, row) {
                    return '<a href="' +Routing.generate('dashboard_practitioner_related_projects', {'user': row.DT_RowId}) +'" class="button tiny bg-light-green round web-show"><i class="fontello-wrench"></i>Show</a>';
                },
                "orderable": false
            },
            {
                "targets": 0,
                "render": function (data, type, row) {
                    return '<a href="' +Routing.generate('dashboard_practitioner_edit', {'user': row.DT_RowId}) +'">' + row.firstName + '</a>';
                }
            }
        ]

    });
})(jQuery);
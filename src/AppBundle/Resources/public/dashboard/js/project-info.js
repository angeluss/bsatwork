(function ($) {

    $(document).ready(function () {
        var hash = window.location.hash.substring(window.location.hash.indexOf('#') + 1);
        if (hash) {
            $('a[href="#' + hash + '"]').click();
        }

        userTableInit();
    });

    $(document).on("click", "#project-url", function () {
        $('#project-url-modal').foundation('reveal','open');
    });

    $(document).on("click", "#add-accounts", function () {
        $('#add-accounts-modal').foundation('reveal','open');
    });

    $(document).on("click", "#project-url-text", function () {
        $(this).select();
    });

    function userTableInit() {
        var route = Routing.generate('dashboard_get_project_users_list_ajax', {'project': project});
        var usersTable = $('#users-table').dataTable({
            "ajax": route,
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "order": [[2, "asc"]],
            "scrollX": true,
            "columns": [
                {"data": "user_type"},
                {"data": "first_name"},
                {"data": "last_name"},
                {"data": "subgroup_names"},
                {"data": "main_style"},
                {"data": "main_style"}
            ],
            "columnDefs": [
                {
                    "targets": -2,
                    "render": function (data, type, row) {
                        if (row.test_pass) {
                            return '<a href="#" class="user-style">'+data+'</a>';
                        } else {
                            return 'The test is not passed';
                        }
                    }
                },
                {
                    "targets": -1,
                    "render": function (data, type, row) {
                        if (row.test_pass) {
                            return '<a href="#" data-reveal-id="user-web-image-modal" class="expand button tiny bg-light-green round web-show user-web" data-reveal-ajax="false"><i class="fontello-chart-pie">'+openMessage+'</i></a>';
                        } else {
                            return 'The test is not passed';
                        }
                    }
                }
            ]

        });

    }

    $(document).on("click", 'a[data-reveal-id="dimensions-modal"]', function () {
        var route = Routing.generate('dashboard_ajax_project_dimensions', {'id': project});
        $('#dimensions-table').dataTable({
            "ajax": route,
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "order": [[0, "asc"]],
            "scrollX": true,
            "columns": [
                {"data": "user_name"},
                {"data": "company_name"},
                {"data": "style_1"},
                {"data": "style_2"},
                {"data": "style_3"},
                {"data": "style_4"},
                {"data": "style_5"},
                {"data": "style_6"},
                {"data": "style_7"},
                {"data": "style_8"},
                {"data": "style_name"}

            ],
            "columnDefs": [
                {"visible": false, "targets": 1}
            ],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;

                api.column(1, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {
                        var data_row = '<tr class="bg-opacity-white">' + '<td>' + group + '</td>';
                        $.each(settings.json.group_data, function (index, value) {
                            data_row += '<td>' + value.avg_point + '</td>';

                        });
                        if (settings.json.group_style){
                            data_row += '<td>' + settings.json.group_style + '</td>';
                        }
                        data_row += '</tr>';
                        $(rows).eq(i).before(data_row);
                        last = group;
                    }
                });
            }
        });
    });

    $(document).on("click", ".user-web", function () {
        var user = $(this).closest("tr").attr('id');
        $('#user-web-image-modal').foundation('reveal', 'open');
        $('#user-image-loader').show();
        $('#user-web-image-graph').hide();
        $.ajax({
            url: Routing.generate('dashboard_users_web_image_ajax', {'user': user})
        })
            .success(function (msg) {
                var image_graph = $('#user-web-image-graph');
                image_graph.attr('src', msg.data.thumbnailUrl);
                $('#user-image-loader').hide();
                image_graph.show();
            });
    });

    $(document).on("click", 'a[data-reveal-id="web-image-modal"]', function () {
        $('#web-image-loader').show();
        $('#web-image-graph').hide();
        $.ajax({
            url: Routing.generate('dashboard_project_web_image_ajax', {'id': project})
        })
            .success(function (msg) {
                var image_graph = $('#web-image-graph');
                image_graph.attr('src', msg.data.thumbnailUrl);
                $('#web-image-loader').hide();
                image_graph.show();
            });
    });

    var all_checked = [];
    $('a[data-reveal-id="user-add-modal"]').on('click', function () {
        var table = $('#add-users-table').dataTable({
            "ajax": Routing.generate('dashboard_get_available_users_list_ajax', {'project': project}),
            "processing": true,
            "serverSide": true,
            "aLengthMenu": [[5, 10, 50], [5, 10, 50]],
            "iDisplayLength": 5,
            "bRetrieve": true,
            "columns": [
                {"data": "company_names"},
                {"data": "company_names"},
                {"data": "team_names"},
                {"data": "user_type"},
                {"data": "first_name"},
                {"data": "last_name"},
                {"data": "main_style"}
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "render": function (data, type, row) {
                        return '<input type="checkbox" value="' + row.DT_RowId + '">';
                    }
                }
            ],
            "fnDrawCallback": function (oSettings) {
                $.each(all_checked, function (index, value) {
                    $("#add-users-table input[value='" + value + "']").prop('checked', true);
                });

            }

        });
        $('#warning-alert').hide();
        $('#user-added-info').hide();
    });
    $(document).on("click", "#add-user-submit", function () {
        var checked = $('#add-users-table').find(':checked');
        $.ajax({
            'type': 'POST',
            'url': Routing.generate('dashboard_add_users_project_ajax', {'project': project}),
            'data': {'userIds': all_checked}
        }).success(function (msg) {
            if (msg.status == 'ok') {
                userTableInit();
                $('#user-added-info').show();
                $('[data-reveal]').foundation('reveal', 'close');
                $('.reveal-modal-bg').hide();
            }
            if (msg.status == 'error') {
                $('#warning-alert-add-test').text(msg.message);
                $('#warning-alert-add').show();
            }

        }).error(function (msg) {
                if (msg.status == 'error') {
                    $('#warning-alert-add-test').text('Something gone wrong');
                    $('#warning-alert-add').show();

                }
            }
        );

    });

    $(document).on('change', "#add-users-table input[type='checkbox']", function () {
        if (this.checked) {
            if (all_checked.indexOf($(this).val()) < 0) {
                all_checked.push($(this).val())
            }
        } else {
            var index = all_checked.indexOf($(this).val());
            if (index > -1) {
                all_checked.splice(index, $(this).val());
            }
        }
    });

    $('a[data-reveal-id="user-delete-modal"]').on('click', function () {
        var table = $('#delete-users-table').dataTable({
            "ajax": Routing.generate('dashboard_get_project_users_list_ajax', {'project': project}),
            "processing": true,
            "serverSide": true,
            "aLengthMenu": [[5, 10, 50], [5, 10, 50]],
            "iDisplayLength": 5,
            destroy: true,
            "columns": [
                {"data": "company_names"},
                {"data": "company_names"},
                {"data": "team_names"},
                {"data": "user_type"},
                {"data": "first_name"},
                {"data": "last_name"},
                {"data": "main_style"}
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "render": function (data, type, row) {
                        return '<input type="checkbox" value="' + row.DT_RowId + '">';
                    }
                }
            ],
            "fnDrawCallback": function (oSettings) {
                $.each(all_checked, function (index, value) {
                    $("#delete-users-table input[value='" + value + "']").prop('checked', true);
                });

            }

        });
        $('#warning-alert').hide();
        $('#user-deleted-info').hide();
    });
    $(document).on("click", "#delete-user-submit", function () {
        var checked = $('#delete-users-table').find(':checked');
        $.ajax({
            'type': 'POST',
            'url': Routing.generate('dashboard_delete_users_project_ajax', {'project': project}),
            'data': {'userIds': all_checked}
        }).success(function (msg) {
            if (msg.status == 'ok') {
                userTableInit();
                $('#user-deleted-info').show();
                $('[data-reveal]').foundation('reveal', 'close');
                $('.reveal-modal-bg').hide();
            }
            if (msg.status == 'error') {
                $('#warning-alert-delete-test').text(msg.message);
                $('#warning-alert-delete').show();
            }

        }).error(function (msg) {
                if (msg.status == 'error') {
                    $('#warning-alert-delete-test').text('Something gone wrong');
                    $('#warning-alert-delete').show();

                }
            }
        );

    });

    $(document).on('change', "#delete-users-table input[type='checkbox']", function () {
        if (this.checked) {
            if (all_checked.indexOf($(this).val()) < 0) {
                all_checked.push($(this).val())
            }
        } else {
            var index = all_checked.indexOf($(this).val());
            if (index > -1) {
                all_checked.splice(index, $(this).val());
            }
        }
    });

    $(document).on('click', '.user-style', function (e) {
        e.preventDefault();
        var user = $(this).closest("tr").attr('id');
        var route = Routing.generate('dashboard_user_style', {'user': user});
        $.ajax({
            'type': 'POST',
            'url': route
        }).success(function (html) {
            $('#user-style-modal .row .small-centered').empty();
            $('#user-style-modal .row .small-centered').append(html);
            $('#user-style-modal').foundation('reveal', 'open');
        });
    });

    $(document).on('click', '.user-info', function (e) {
        e.preventDefault();
        var user = $(this).closest("tr").attr('id');
        var route = Routing.generate('dashboard_user_info', {'user': user});
        $.ajax({
            'type': 'POST',
            'url': route
        }).success(function (html) {
            $('#user-info-modal .row .small-centered').empty();
            $('#user-info-modal .row .small-centered').append(html);
            $('#user-info-modal').foundation('reveal', 'open');
        });
    });

    $("#download-pdf").click(function () {
        window.location = Routing.generate("dashboard_project_pdf", {'project': project});
    });

})(jQuery);
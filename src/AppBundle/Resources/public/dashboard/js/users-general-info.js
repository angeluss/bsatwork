(function ($) {
    $('#users-table').dataTable({
        "ajax": Routing.generate('dashboard_users_general_info_list'),
        "processing": true,
        "serverSide": true,
        "columns": [
            {"data": "first_name"},
            {"data": "last_name"},
            {"data": "email"},
            {"data": "gender"},
            {"data": "birth_year"},
            {"data": "education_level"},
            {"data": "reasons"}
        ]
    });

})(jQuery);
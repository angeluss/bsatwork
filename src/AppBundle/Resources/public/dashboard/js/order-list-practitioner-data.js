(function ($) {
    $('#order-table').dataTable({
        "ajax": Routing.generate('dashboard_order_user_list_ajax'),
        "processing": true,
        "serverSide": true,
        "order": [[0, "desc"]],
        "columns": [
            {"data": "created"},
            {"data": "amount"},
            {"data": "amount"}
        ],
        "columnDefs": [
            {
                "targets": -1,
                "render": function (data, type, row) {
                    return '<form method="post" action="' + Routing.generate('dashboard_order_delete', {'order': row.DT_RowId}) + '">'
                        + '<button  type="submit" class="tiny bg-red-orange round web-show">Delete</button>'
                        + '</form>';
                }
            }
        ]

    });
    $(document).ready(function () {
        if (show_create) {
            $('button[data-reveal-id="add-order"]').trigger('click');
        }
    });

})(jQuery);
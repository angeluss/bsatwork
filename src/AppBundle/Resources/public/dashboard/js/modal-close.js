(function ($) {
    $(document).on('click', '.close-reveal-modal', function() {
        $('.reveal-modal-bg').remove();
    });

    $(document).on('click', '.reveal-modal-bg', function() {
        $('.reveal-modal-bg').remove();
    });

})(jQuery);
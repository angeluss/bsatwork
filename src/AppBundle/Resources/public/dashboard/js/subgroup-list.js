(function ($) {
    var all_checked = [];
    $(document).ready(function () {
        var hash = window.location.hash.substring(window.location.hash.indexOf('#') + 1);
        if (hash) {
            $('a[href="#' + hash + '"]').click();
        }

        setMainStyle();
        var group = $('.tab-title.active').attr('data-group');
        init_user_table(group);

    });

    $('.tab-title').on('click', function (event, tab) {
        var style = $('.active').find('.main-style-group').text();
        if (!style) {
            setMainStyle();
        }
        var group = $('.tab-title.active').attr('data-group');
        init_user_table(group);
        window.location.hash = $(this).find('a').attr('href');
    });

    function setMainStyle() {
        var group = $('.tab-title.active').attr('data-group');
        var route = Routing.generate('dashboard_ajax_main_style', {'id': group});
        $.ajax({
            url: route
        })
            .success(function (msg) {
                if (msg.style) {
                    $('.active').find('.main-style-group').text(msg.style);
                    $('.active').find('.subgroup-title').show();

                } else {
                    $('.active').find('.subgroup-title').hide();
                }
            }).error(function () {
                $('.active').find('.subgroup-title').hide();
            }
        );
    }

    $(document).on("click", 'a[data-reveal-id="web-image-modal"]', function () {
        var group = $('.tab-title.active').attr('data-group');
        $('#loader').show();
        $('#web-image-graph').hide();
        $.ajax({
            url: Routing.generate('dashboard_subgroup_web_image_ajax', {'id': group})
        })
            .success(function (msg) {
                var image_graph = $('#web-image-graph');
                image_graph.attr('src', msg.data.thumbnailUrl);
                $('#loader').hide();
                image_graph.show();
            });
    });

    $(document).on("click", 'a[data-reveal-id="dimensions-modal"]', function () {
        var group = $('.tab-title.active').attr('data-group');
        var route = Routing.generate('dashboard_ajax_dimensions', {'id': group});
        $('#dimensions-table').dataTable({
            "ajax": route,
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "order": [[0, "asc"]],
            "scrollX": true,
            "columns": [
                {"data": "user_name"},
                {"data": "group_name"},
                {"data": "style_1"},
                {"data": "style_2"},
                {"data": "style_3"},
                {"data": "style_4"},
                {"data": "style_5"},
                {"data": "style_6"},
                {"data": "style_7"},
                {"data": "style_8"},
                {"data": "style_name"}

            ],
            "columnDefs": [
                {"visible": false, "targets": 1}
            ],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;

                api.column(1, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {
                        var data_row = '<tr class="bg-opacity-white">' + '<td>' + group + '</td>';
                        $.each(settings.json.group_data, function (index, value) {
                            data_row += '<td>' + value.avg_point + '</td>';

                        });
                        if (settings.json.group_style){
                            data_row += '<td>' + settings.json.group_style + '</td>';
                        }
                        data_row += '</tr>';
                        $(rows).eq(i).before(data_row);
                        last = group;
                    }
                });
            }
        });
    });


    function init_user_table(id) {
        var route = Routing.generate('dashboard_subgroup_users_list_ajax', {'id': id});
        $('#users-table-' + id).dataTable({
            "ajax": route,
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "order": [[4, "asc"]],
            "scrollX": true,
            "columns": [
                {"data": "company_names"},
                {"data": "team_names"},
                {"data": "user_type"},
                {"data": "first_name"},
                {"data": "last_name"},
                {"data": "main_style"},
                {"data": "test_pass"}
            ],
            "columnDefs": [
                {
                    "targets": -2,
                    "render": function (data, type, row) {
                        if (row.test_pass) {
                            return '<a href="#" class="user-style">'+data+'</a>';
                        } else {
                            return 'The test is not passed';
                        }
                    }
                },
                {
                    "targets": -1,
                    "render": function (data, type, row) {
                        if (row.test_pass) {
                            return '<a href="#" data-reveal-id="user-web-image-modal" class="expand button tiny bg-light-green round web-show user-web" data-reveal-ajax="false"><i class="fontello-chart-pie">Open</i></a>';
                        } else {
                            return 'The test is not passed';
                        }
                    }
                }
            ]

        });
    }


    $(document).on("click", ".user-web", function () {
        var user = $(this).closest("tr").attr('id');
        $('#user-web-image-modal').foundation('reveal', 'open');
        $('#user-image-loader').show();
        $('#user-web-image-graph').hide();
        $.ajax({
            url: Routing.generate('dashboard_users_web_image_ajax', {'user': user})
        })
            .success(function (msg) {
                var image_graph = $('#user-web-image-graph');
                image_graph.attr('src', msg.data.thumbnailUrl);
                $('#user-image-loader').hide();
                image_graph.show();
            });
    });

    $(document).on('click', '.user-style', function (e) {
        e.preventDefault();
        var user = $(this).closest("tr").attr('id');
        var route = Routing.generate('dashboard_user_style', {'user': user});
        $.ajax({
            'type': 'POST',
            'url': route
        }).success(function (html) {
            $('#user-style-modal .row .small-centered').empty();
            $('#user-style-modal .row .small-centered').append(html);
            $('#user-style-modal').foundation('reveal', 'open');
        });
    });

    $(document).on('click', '.user-info', function (e) {
        e.preventDefault();
        var user = $(this).closest("tr").attr('id');
        var route = Routing.generate('dashboard_user_info', {'user': user});
        $.ajax({
            'type': 'POST',
            'url': route
        }).success(function (html) {
            $('#user-info-modal .row .small-centered').empty();
            $('#user-info-modal .row .small-centered').append(html);
            $('#user-info-modal').foundation('reveal', 'open');
        });
    });

    $("#download-pdf").click(function () {
        var group = $('.tab-title.active').attr('data-group');
        window.location = Routing.generate("dashboard_subgroup_pdf", {'subgroup': group});
    });

    $('.close-confirm-modal').on('click', function(){
        $('.close-reveal-modal').click();
    });

})(jQuery);

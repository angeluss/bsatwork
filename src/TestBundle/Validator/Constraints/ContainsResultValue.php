<?php

namespace TestBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsResultValue extends Constraint
{
    public $message = 'Please select at least "%int%" items.';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'test_result_validator';
    }

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
<?php

namespace TestBundle\Validator\Constraints;


use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use TestBundle\Entity\TestResult;

/**
 * Class ContainsResultValueValidator
 *
 * @package TestBundle\Validator
 */
class ContainsResultValueValidator extends ConstraintValidator
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param TestResult $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     *
     * @api
     */
    public function validate($value, Constraint $constraint)
    {
        if (count($value->getResults()) != $value->getQuestion()->getMinAvailableResult()) {
            $message = sprintf($this->translator->trans('_min_choice_validator_error'), $value->getQuestion()->getMinAvailableResult());

            $this->context->buildViolation($message)
                ->addViolation();
        }

    }
}
$(function () {
    progressbar_change();
    $(function () {
        var sortable = $('#sortable');
        sortable.sortable({
            update: function() {
                var i=0;
                $('.ans-index').each(function () {
                    $(this).text(++i+')');
                });
            }
        });
        sortable.disableSelection();
    });

    $('li.next').on('click', function () {
        var next_qw_route = Routing.generate('test_next_qw');
        var data = {};
        data.results = [];
        var question_type = $("input[name='question_type']").val();
        data.hash = $("input[name='hash']").val();
        if (question_type == 'list') {
            $.each($('#sortable').find("input[name='answer_number']"), function (key, value) {
                data.results[key + 1] = $(value).val();
            });
        }
        if (question_type == 'checkbox') {
            var checked_ans = $("#checkbox-type").find("input:checked");
            $.each(checked_ans, function (key, value) {
                data.results[key + 1] = $(this).val();
            });
        }
        if (question_type == 'radio_button') {
            var checked_radio_ans = $("#radio-type").find("input:checked");
            $.each(checked_radio_ans, function (key, value) {
                data.results[key + 1] = $(this).val();
            });
        }

        $.ajax({
            type: "POST",
            url: next_qw_route,
            data: data,
            success: function (msg) {
                if (msg.last_question == true) {
                    window.location.replace(Routing.generate('user_info'));
                }
                if (msg.status == 'ok') {
                    render_result(msg);
                }
                if (msg.status == 'error') {
                    render_alert(msg);
                }

            }
        });
    });

    $('li.previous').on('click', function () {
        var prev_qw_route = Routing.generate('test_prev_qw');
        $.ajax({
            url: prev_qw_route,
            success: function (msg) {
                if (msg.status == 'ok') {
                    render_result(msg);
                }
                if (msg.status == 'error') {
                    render_alert(msg);
                }
            }
        });
    });

    function render_result(msg) {
        var question = msg.question;
        var question_title = $(".question-title");
        var list_content = $('#list-type');
        var checkbox_content = $('#checkbox-type');
        var radio_content = $('#radio-type');
        var alert_content = $('.alert');
        question_title.text(question.text);
        radio_content.hide();
        checkbox_content.hide();
        list_content.hide();
        alert_content.hide();
        $("input[name='question_type']").val(question.question_type);
        $("input[name='hash']").val(question.hash);
        $("input[name='order_type']").val(question.order_type);
        if (question.question_type == 'list') {
            var answers_data = $('#sortable').find('li');
            $.each(question.answers, function (key, value) {
                $(answers_data.get(key)).find('.ans-text').text(value.text);
                $(answers_data.get(key)).find("input[name='answer_number']").val(value.answer_number);
            });
            list_content.show();
        }
        if (question.question_type == 'checkbox') {
            checkbox_content.text('');
            if (typeof  question.answers != 'undefined') {
                $.each(question.answers, function (key, value) {
                    var item = "<label style='font-size: 14px'><input type='checkbox' value='" + value.answer_number + "' name='an_" + value.answer_number + "'> " + value.text + "</label>";
                    checkbox_content.append(item);
                });
            }
            if (typeof  msg.selected != 'undefined') {
                $.each(msg.selected, function (key, value) {
                    $("input[name='an_" + value + "']").prop('checked', true);
                });
            }

            checkbox_content.show();
        }
        if (question.question_type == 'radio_button') {
            radio_content.text('');
            if (typeof  question.answers != 'undefined') {
                $.each(question.answers, function (key, value) {
                    var item = "<label style='font-size: 14px'><input type='radio' value='" + value.answer_number + "' name='an_group'> " + value.text + "</label>";
                    radio_content.append(item);
                });
            }
            if (typeof  msg.selected != 'undefined') {
                $("input[value='" + msg.selected[1] + "']").prop('checked', true);
            }

            radio_content.show();
        }


        if (msg.has_previous) {
            $('li.previous').show();
        } else {
            $('li.previous').hide();
        }
        progressbar_change();
    }

    function render_alert(msg) {
        $('#alert-description').text(msg.message);
        $('.alert').show();
        $('.box').show();
    }

    function progressbar_change(){
        var q_num = $("input[name='order_type']").val();
        var percent = Math.round((q_num / (q_cnt + 1)) * 100);
        $(".meter").css('width', ''+percent+'%');
    }


});
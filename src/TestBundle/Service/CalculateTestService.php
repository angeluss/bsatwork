<?php

namespace TestBundle\Service;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use TestBundle\Entity\Style;

/**
 * Class CalculateTest
 *
 * @package TestBundle\Service
 */
class CalculateTestService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $points
     *
     * @return mixed
     */
    public function calculateGroupStyle($points)
    {
        $lst3 = [];
        $lst2 = $this->clearList($points);
        if (count($lst2) > 1) {
            $lst3 = $this->clearList($points);
        }

        $userPointResult = $this->expresions('group', $points, $lst2, $lst3);

        return $userPointResult['mainStyle'];
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function calculateUserTest(User $user)
    {
        $lst3 = [];
        $userPointResult = $this->calculateMainStyles($user);
        $userPointResult = $this->calculateOtherStyles($user, $userPointResult);
        $lst2 = $this->clearList($userPointResult);
        if (count($lst2) > 1) {
            $userPointResult = $this->exp3($user, $userPointResult);
            $lst3 = $this->clearList($userPointResult);
        }
        $userPointResult = $this->expresions('user', $userPointResult, $lst2, $lst3, $user);


        return $userPointResult;

    }

    private function expresions($type, $userPointResult, $lst2, $lst3, $user = null)
    {
        if ($type == 'user') {
            $userResult = $this->entityManager
                ->getRepository('TestBundle:TestResult')
                ->getUserResults($user);
            $hoog1 = $this->getHoog1($userResult, $lst2, $lst3);
        } else {
            $hoog1 = $this->getGroupHoog1($lst2, $lst3);
        }
        $hoog2 = $this->getHoog2($userPointResult, $hoog1);
        $gen1 = abs(($userPointResult[Style::ANALYZER] - $userPointResult[Style::IMPLEMENTER]));
        $gen2 = abs(($userPointResult[Style::ANALYZER] - $userPointResult[Style::HUMANIZER]));
        $gen3 = abs(($userPointResult[Style::ANALYZER] - $userPointResult[Style::INNOVATOR]));
        $gen4 = abs(($userPointResult[Style::IMPLEMENTER] - $userPointResult[Style::HUMANIZER]));
        $gen5 = abs(($userPointResult[Style::IMPLEMENTER] - $userPointResult[Style::INNOVATOR]));
        $gen6 = abs(($userPointResult[Style::HUMANIZER] - $userPointResult[Style::INNOVATOR]));

        $userPointResult[Style::SYSTEMISER] = (($userPointResult[Style::ANALYZER] + $userPointResult[Style::IMPLEMENTER]) / 2);
        $userPointResult[Style::ORGANIZER] = (($userPointResult[Style::IMPLEMENTER] + $userPointResult[Style::HUMANIZER]) / 2);
        $userPointResult[Style::CONNECTOR] = (($userPointResult[Style::HUMANIZER] + $userPointResult[Style::INNOVATOR]) / 2);
        $userPointResult[Style::CONCEPTUALISER] = (($userPointResult[Style::ANALYZER] + $userPointResult[Style::INNOVATOR]) / 2);

        $styleMap = [1 => Style::ANALYZER, 2 => Style::IMPLEMENTER, 3 => Style::HUMANIZER, 4 => Style::INNOVATOR];
        $uitk = isset($styleMap[$hoog1]) ? $styleMap[$hoog1] : '';

        $tus5 = abs(($userPointResult[Style::ANALYZER] - $userPointResult[Style::IMPLEMENTER]));
        $tus6 = abs(($userPointResult[Style::IMPLEMENTER] - $userPointResult[Style::HUMANIZER]));
        $tus7 = abs(($userPointResult[Style::HUMANIZER] - $userPointResult[Style::INNOVATOR]));
        $tus8 = abs(($userPointResult[Style::INNOVATOR] - $userPointResult[Style::ANALYZER]));
        if ((($tus5 < 5) && (($hoog1 == 1) && ($hoog2 == 2))) || (($tus5 < 5) && (($hoog1 == 2) && ($hoog2 == 1)))) {
            $uitk = Style::SYSTEMISER;
        }
        if ((($tus6 < 5) && (($hoog1 == 2) && ($hoog2 == 3))) || (($tus6 < 5) && (($hoog1 == 3) && ($hoog2 == 2)))) {
            $uitk = Style::ORGANIZER;
        }
        if ((($tus7 < 5) && (($hoog1 == 3) && ($hoog2 == 4))) || (($tus7 < 5) && (($hoog1 == 4) && ($hoog2 == 3)))) {
            $uitk = Style::CONNECTOR;
        }
        if ((($tus8 < 5) && (($hoog1 == 4) && ($hoog2 == 1))) || (($tus8 < 5) && (($hoog1 == 1) && ($hoog2 == 4)))) {
            $uitk = Style::CONCEPTUALISER;
        }
        $tusE1 = abs(($userPointResult[Style::IMPLEMENTER] - $userPointResult[Style::INNOVATOR]));
        $tusE2 = abs(($userPointResult[Style::ANALYZER] - $userPointResult[Style::HUMANIZER]));
        if (($tusE1 < 5) && ((($hoog1 == 2) && ($hoog2 == 4)) || (($hoog1 == 4) && ($hoog2 == 2)))) {
            $uitk = Style::CHANGE_MANAGER;
        }

        if (($tusE2 < 5) && ((($hoog1 == 1) && ($hoog2 == 3)) || (($hoog1 == 3) && ($hoog2 == 1)))) {
            $uitk = Style::TEACHER;
        }

        $hoog3 = $this->getHoog3($hoog1, $hoog2, $userPointResult);

        if (((($hoog1 == 2) && (($hoog2 == 3) && ($hoog3 == 4))) || ((($hoog1 == 2) && (($hoog2 == 4) && ($hoog3 == 3))) || ((($hoog1 == 3) && (($hoog2 == 2) && ($hoog3 == 4))) || ((($hoog1 == 3) && (($hoog2 == 4) && ($hoog3 == 2))) || ((($hoog1 == 4) && (($hoog2 == 2) && ($hoog3 == 3))) || (($hoog1 == 4) && (($hoog2 == 3) && ($hoog3 == 2)))))))) && (($tus6 < 5) && (($tusE1 < 5) && ($tus7 < 5)))) {
            $uitk = Style::CHANGE_MASTER;
        }
        if (((($hoog1 == 1) && (($hoog2 == 2) && ($hoog3 == 4))) || ((($hoog1 == 1) && (($hoog2 == 4) && ($hoog3 == 2))) || ((($hoog1 == 2) && (($hoog2 == 1) && ($hoog3 == 4))) || ((($hoog1 == 2) && (($hoog2 == 4) && ($hoog3 == 1))) || ((($hoog1 == 4) && (($hoog2 == 1) && ($hoog3 == 2))) || (($hoog1 == 4) && (($hoog2 == 2) && ($hoog3 == 1)))))))) && (($tus5 < 5) && (($tusE1 < 5) && ($tus8 < 5)))) {
            $uitk = Style::INVENTOR;
        }
        if (((($hoog1 == 1) && (($hoog2 == 3) && ($hoog3 == 4))) || ((($hoog1 == 1) && (($hoog2 == 4) && ($hoog3 == 3))) || ((($hoog1 == 3) && (($hoog2 == 1) && ($hoog3 == 4))) || ((($hoog1 == 3) && (($hoog2 == 4) && ($hoog3 == 1))) || ((($hoog1 == 4) && (($hoog2 == 1) && ($hoog3 == 3))) || (($hoog1 == 4) && (($hoog2 == 3) && ($hoog3 == 1)))))))) && (($tus7 < 5) && (($tusE2 < 5) && ($tus8 < 5)))) {
            $uitk = Style::IDEALIST;
        }
        if (((($hoog1 == 1) && (($hoog2 == 2) && ($hoog3 == 3))) || ((($hoog1 == 1) && (($hoog2 == 3) && ($hoog3 == 2))) || ((($hoog1 == 2) && (($hoog2 == 1) && ($hoog3 == 3))) || ((($hoog1 == 2) && (($hoog2 == 3) && ($hoog3 == 1))) || ((($hoog1 == 3) && (($hoog2 == 1) && ($hoog3 == 2))) || (($hoog1 == 3) && (($hoog2 == 2) && ($hoog3 == 1)))))))) && (($tus5 < 5) && (($tusE2 < 5) && ($tus6 < 5)))) {
            $uitk = Style::REALIST;
        }
        if (($gen1 <= 4) && (($gen2 <= 4) && (($gen3 <= 4) && (($gen4 <= 4) && (($gen5 <= 4) && ($gen6 <= 4)))))) {
            $uitk = Style::GENERALIST;
        }
        if ($uitk) {
            $userPointResult['mainStyle'] = $uitk;
        }

        return $userPointResult;
    }

    private function getHoog1($userPointResult, $lst2, $lst3)
    {
        $hoog1 = 0;
        for ($i = 1; $i <= 4; $i++) {
            if ((in_array($i, $lst2) && count($lst2) == 1)
                || ((in_array($i, $lst3) && count($lst3) == 1)
                    || (($this->getResultPoint($userPointResult, 14, $i) == 4)
                        && ((count($lst2) > 1) && (count($lst3) > 1)))
                )
            ) {
                $hoog1 = $i;
            }
        }

        return $hoog1;
    }

    private function getGroupHoog1($lst2, $lst3)
    {
        $hoog1 = 0;
        for ($i = 1; $i <= 4; $i++) {
            if ((in_array($i, $lst2) && count($lst2) == 1)
                || ((in_array($i, $lst3) && count($lst3) == 1)
                    || (((count($lst2) > 1) && (count($lst3) > 1)))
                )
            ) {
                $hoog1 = $i;
            }
        }

        return $hoog1;
    }

    private function getHoog2($userPointResult, $hoog1)
    {
        $hoog2 = 0;
        if (($hoog1 == 1) && (($userPointResult[Style::IMPLEMENTER] >= $userPointResult[Style::HUMANIZER])
                && ($userPointResult[Style::IMPLEMENTER] >= $userPointResult[Style::INNOVATOR]))
        ) {
            $hoog2 = 2;
        }
        if (($hoog1 == 1) && (($userPointResult[Style::HUMANIZER] >= $userPointResult[Style::IMPLEMENTER])
                && ($userPointResult[Style::HUMANIZER] >= $userPointResult[Style::INNOVATOR]))
        ) {
            $hoog2 = 3;
        }
        if (($hoog1 == 1) && (($userPointResult[Style::INNOVATOR] >= $userPointResult[Style::IMPLEMENTER])
                && ($userPointResult[Style::INNOVATOR] >= $userPointResult[Style::HUMANIZER]))
        ) {
            $hoog2 = 4;
        }
        if (($hoog1 == 2) && (($userPointResult[Style::ANALYZER] >= $userPointResult[Style::HUMANIZER])
                && ($userPointResult[Style::ANALYZER] >= $userPointResult[Style::INNOVATOR]))
        ) {
            $hoog2 = 1;
        }
        if (($hoog1 == 2) && (($userPointResult[Style::HUMANIZER] >= $userPointResult[Style::ANALYZER])
                && ($userPointResult[Style::HUMANIZER] >= $userPointResult[Style::INNOVATOR]))
        ) {
            $hoog2 = 3;
        }
        if (($hoog1 == 2) && (($userPointResult[Style::INNOVATOR] >= $userPointResult[Style::ANALYZER])
                && ($userPointResult[Style::INNOVATOR] >= $userPointResult[Style::HUMANIZER]))
        ) {
            $hoog2 = 4;
        }
        if (($hoog1 == 3) && (($userPointResult[Style::ANALYZER] >= $userPointResult[Style::IMPLEMENTER])
                && ($userPointResult[Style::ANALYZER] >= $userPointResult[Style::INNOVATOR]))
        ) {
            $hoog2 = 1;
        }
        if (($hoog1 == 3) && (($userPointResult[Style::IMPLEMENTER] >= $userPointResult[Style::ANALYZER])
                && ($userPointResult[Style::IMPLEMENTER] >= $userPointResult[Style::INNOVATOR]))
        ) {
            $hoog2 = 2;
        }
        if (($hoog1 == 3) && (($userPointResult[Style::INNOVATOR] >= $userPointResult[Style::ANALYZER])
                && ($userPointResult[Style::INNOVATOR] >= $userPointResult[Style::IMPLEMENTER]))
        ) {
            $hoog2 = 4;
        }
        if (($hoog1 == 4) && (($userPointResult[Style::ANALYZER] >= $userPointResult[Style::IMPLEMENTER])
                && ($userPointResult[Style::ANALYZER] >= $userPointResult[Style::HUMANIZER]))
        ) {
            $hoog2 = 1;
        }
        if (($hoog1 == 4) && (($userPointResult[Style::IMPLEMENTER] >= $userPointResult[Style::ANALYZER])
                && ($userPointResult[Style::IMPLEMENTER] >= $userPointResult[Style::HUMANIZER]))
        ) {
            $hoog2 = 2;
        }
        if (($hoog1 == 4) && (($userPointResult[Style::HUMANIZER] >= $userPointResult[Style::ANALYZER])
                && ($userPointResult[Style::HUMANIZER] >= $userPointResult[Style::IMPLEMENTER]))
        ) {
            $hoog2 = 3;
        }

        return $hoog2;
    }

    private function getHoog3($hoog1, $hoog2, $userPointResult)
    {
        $hoog3 = 0;
        $hfdhuma = $userPointResult[Style::HUMANIZER];
        $hfdinno = $userPointResult[Style::INNOVATOR];
        $hfdimpl = $userPointResult[Style::IMPLEMENTER];
        $hfdanal = $userPointResult[Style::ANALYZER];
        if (($hoog1 == 1) && (($hoog2 == 2) && ($hfdhuma >= $hfdinno))) {
            $hoog3 = 3;
        }
        if (($hoog1 == 1) && (($hoog2 == 2) && ($hfdinno >= $hfdhuma))) {
            $hoog3 = 4;
        }
        if (($hoog1 == 1) && (($hoog2 == 3) && ($hfdimpl >= $hfdinno))) {
            $hoog3 = 2;
        }
        if (($hoog1 == 1) && (($hoog2 == 3) && ($hfdinno >= $hfdimpl))) {
            $hoog3 = 4;
        }
        if (($hoog1 == 1) && (($hoog2 == 4) && ($hfdimpl >= $hfdhuma))) {
            $hoog3 = 2;
        }
        if (($hoog1 == 1) && (($hoog2 == 4) && ($hfdhuma >= $hfdimpl))) {
            $hoog3 = 3;
        }
        if (($hoog1 == 2) && (($hoog2 == 1) && ($hfdhuma >= $hfdinno))) {
            $hoog3 = 3;
        }
        if (($hoog1 == 2) && (($hoog2 == 1) && ($hfdinno >= $hfdhuma))) {
            $hoog3 = 4;
        }
        if (($hoog1 == 2) && (($hoog2 == 3) && ($hfdanal >= $hfdinno))) {
            $hoog3 = 1;
        }
        if (($hoog1 == 2) && (($hoog2 == 3) && ($hfdinno >= $hfdanal))) {
            $hoog3 = 4;
        }
        if (($hoog1 == 2) && (($hoog2 == 4) && ($hfdanal >= $hfdhuma))) {
            $hoog3 = 1;
        }
        if (($hoog1 == 2) && (($hoog2 == 4) && ($hfdhuma >= $hfdanal))) {
            $hoog3 = 3;
        }
        if (($hoog1 == 3) && (($hoog2 == 1) && ($hfdimpl >= $hfdinno))) {
            $hoog3 = 2;
        }
        if (($hoog1 == 3) && (($hoog2 == 1) && ($hfdinno >= $hfdimpl))) {
            $hoog3 = 4;
        }
        if (($hoog1 == 3) && (($hoog2 == 2) && ($hfdanal >= $hfdinno))) {
            $hoog3 = 1;
        }
        if (($hoog1 == 3) && (($hoog2 == 2) && ($hfdinno >= $hfdanal))) {
            $hoog3 = 4;
        }

        if (($hoog1 == 3) && (($hoog2 == 4) && ($hfdanal >= $hfdimpl))) {
            $hoog3 = 1;
        }
        if (($hoog1 == 3) && (($hoog2 == 4) && ($hfdimpl >= $hfdanal))) {
            $hoog3 = 2;
        }
        if (($hoog1 == 4) && (($hoog2 == 1) && ($hfdimpl >= $hfdhuma))) {
            $hoog3 = 2;
        }
        if (($hoog1 == 4) && (($hoog2 == 1) && ($hfdhuma >= $hfdimpl))) {
            $hoog3 = 3;
        }
        if (($hoog1 == 4) && (($hoog2 == 2) && ($hfdanal >= $hfdhuma))) {
            $hoog3 = 1;
        }
        if (($hoog1 == 4) && (($hoog2 == 2) && ($hfdhuma >= $hfdanal))) {
            $hoog3 = 3;
        }
        if (($hoog1 == 4) && (($hoog2 == 3) && ($hfdanal >= $hfdimpl))) {
            $hoog3 = 1;
        }
        if (($hoog1 == 4) && (($hoog2 == 3) && ($hfdimpl >= $hfdanal))) {
            $hoog3 = 2;
        }

        return $hoog3;
    }

    /**
     * @param $user
     * @param $userPointResult
     *
     * @return mixed
     */
    private function exp3($user, $userPointResult)
    {
        $userResult = $this->entityManager
            ->getRepository('TestBundle:TestResult')
            ->getUserResults($user);
        $this->getResultPoint($userResult, 1, 2);

        $userPointResult[Style::ANALYZER] +=
            + $this->getResultPoint($userResult, 1, 2)
            + $this->getResultPoint($userResult, 12, 1)
            + $this->getResultPoint($userResult, 15, 1)
            + $this->getResultPoint($userResult, 16, 4)
            + $this->getResultPoint($userResult, 18, 2);

        $userPointResult[Style::IMPLEMENTER] +=
            + $this->getResultPoint($userResult, 1, 4)
            + $this->getResultPoint($userResult, 12, 3)
            + $this->getResultPoint($userResult, 15, 2)
            + $this->getResultPoint($userResult, 16, 3)
            + $this->getResultPoint($userResult, 18, 3);

        $userPointResult[Style::HUMANIZER] +=
            + $this->getResultPoint($userResult, 1, 3)
            + $this->getResultPoint($userResult, 12, 4)
            + $this->getResultPoint($userResult, 15, 3)
            + $this->getResultPoint($userResult, 16, 2)
            + $this->getResultPoint($userResult, 18, 4);

        $userPointResult[Style::INNOVATOR] +=
            + $this->getResultPoint($userResult, 1, 1)
            + $this->getResultPoint($userResult, 12, 2)
            + $this->getResultPoint($userResult, 15, 4)
            + $this->getResultPoint($userResult, 16, 1)
            + $this->getResultPoint($userResult, 18, 1);

        $userPointResult[Style::ANALYZER] = $userPointResult[Style::ANALYZER] * (276 / 326);
        $userPointResult[Style::IMPLEMENTER] = $userPointResult[Style::IMPLEMENTER] * (276 / 326);
        $userPointResult[Style::HUMANIZER] = $userPointResult[Style::HUMANIZER] * (276 / 326);
        $userPointResult[Style::INNOVATOR] = $userPointResult[Style::INNOVATOR] * (276 / 326);


        return $userPointResult;
    }

    private function getResultPoint($results, $questionNumber, $answerNumber)
    {
        $point = 0;
        foreach ($results as $resultItem) {
            if ($resultItem['question']['orderType'] == $questionNumber) {
                $flippedResult = array_flip($resultItem['results']);
                $answer = $resultItem['question']['answers'][$answerNumber - 1];
                $point = count($resultItem['question']['answers']) + 1 - $flippedResult[$answer['id']];
                break;
            }
        }

        return $point;

    }

    /**
     * @param array $userPointResult
     *
     * @return array
     */
    private function clearList(array $userPointResult)
    {
        $result = [];
        ($userPointResult[Style::ANALYZER] < $userPointResult[Style::IMPLEMENTER]) || (($userPointResult[Style::ANALYZER] < $userPointResult[Style::HUMANIZER]) || ($userPointResult[Style::ANALYZER] < $userPointResult[Style::INNOVATOR]))
            ?: $result[] = 1;
        ($userPointResult[Style::IMPLEMENTER] < $userPointResult[Style::ANALYZER]) || (($userPointResult[Style::IMPLEMENTER] < $userPointResult[Style::HUMANIZER]) || ($userPointResult[Style::IMPLEMENTER] < $userPointResult[Style::INNOVATOR]))
            ?: $result[] = 2;
        ($userPointResult[Style::HUMANIZER] < $userPointResult[Style::ANALYZER]) || (($userPointResult[Style::HUMANIZER] < $userPointResult[Style::IMPLEMENTER]) || ($userPointResult[Style::HUMANIZER] < $userPointResult[Style::INNOVATOR]))
            ?: $result[] = 3;
        ($userPointResult[Style::INNOVATOR] < $userPointResult[Style::ANALYZER]) || (($userPointResult[Style::INNOVATOR] < $userPointResult[Style::IMPLEMENTER]) || ($userPointResult[Style::INNOVATOR] < $userPointResult[Style::HUMANIZER]))
            ?: $result[] = 4;

        return $result;
    }

    /**
     * @param User $user
     *
     * @return array
     */
    private function calculateMainStyles(User $user)
    {
        $userPointResult = [
            Style::ANALYZER => 0,
            Style::IMPLEMENTER => 0,
            Style::HUMANIZER => 0,
            Style::INNOVATOR => 0
        ];

        $mapping = self::getMainStyleMapping();
        $userResult = $this->entityManager
            ->getRepository('TestBundle:TestResult')
            ->getUserResults($user);
        foreach ($userResult as $resultKey => $result) {
            $order = $result['question']['orderType'];
            if (!isset($mapping[$order])) {
                continue;
            }

            $flippedResult = array_flip($result['results']);
            foreach ($result['question']['answers'] as $defKey => $defAnswer) {
                $point = count($flippedResult) +1  - $flippedResult[$defAnswer['id']];
                $style = $mapping[$order][$defAnswer['orderType']];
                $userPointResult[$style] += $point;

            }

        }

        return $userPointResult;
    }

    /**
     * @param User  $user
     * @param array $userPointResult
     *
     * @return array
     */
    private function calculateOtherStyles(User $user, array $userPointResult)
    {
        $points = [
            13 => Style::ANALYZER,
            1 => Style::ANALYZER,
            10 => Style::ANALYZER,
            11 => Style::ANALYZER,
            16 => Style::IMPLEMENTER,
            2 => Style::IMPLEMENTER,
            14 => Style::IMPLEMENTER,
            4 => Style::IMPLEMENTER,
            12 => Style::HUMANIZER,
            15 => Style::HUMANIZER,
            6 => Style::HUMANIZER,
            9 => Style::HUMANIZER,
            3 => Style::INNOVATOR,
            7 => Style::INNOVATOR,
            5 => Style::INNOVATOR,
            8 => Style::INNOVATOR,
        ];
        $userResult = $this->entityManager
            ->getRepository('TestBundle:TestResult')
            ->getDynamicListResult($user);
        foreach ($userResult as $result) {
            $answers = $this->entityManager->getRepository('TestBundle:Answer')
                ->getAnswers($result['results']);
            foreach ($answers as $answer) {
                $style = $points[$answer['resultId']];
                $userPointResult[$style] += 4;

            }
        }

        return $userPointResult;
    }

    /**
     * @return array
     */
    private static function getMainStyleMapping()
    {
        return [
            1 => [
                2 => Style::ANALYZER,
                4 => Style::IMPLEMENTER,
                3 => Style::HUMANIZER,
                1 => Style::INNOVATOR,
            ],
            2 => [
                2 => Style::ANALYZER,
                1 => Style::IMPLEMENTER,
                4 => Style::HUMANIZER,
                3 => Style::INNOVATOR,
            ],
            3 => [
                4 => Style::ANALYZER,
                2 => Style::IMPLEMENTER,
                1 => Style::HUMANIZER,
                3 => Style::INNOVATOR,
            ],
            4 => [
                1 => Style::ANALYZER,
                4 => Style::IMPLEMENTER,
                2 => Style::HUMANIZER,
                3 => Style::INNOVATOR,
            ],
            5 => [
                2 => Style::ANALYZER,
                1 => Style::IMPLEMENTER,
                4 => Style::HUMANIZER,
                3 => Style::INNOVATOR,
            ],
            6 => [
                1 => Style::ANALYZER,
                2 => Style::IMPLEMENTER,
                3 => Style::HUMANIZER,
                4 => Style::INNOVATOR,
            ],
            7 => [
                1 => Style::ANALYZER,
                2 => Style::IMPLEMENTER,
                3 => Style::HUMANIZER,
                4 => Style::INNOVATOR,
            ],
            8 => [
                2 => Style::ANALYZER,
                1 => Style::IMPLEMENTER,
                4 => Style::HUMANIZER,
                3 => Style::INNOVATOR,
            ],
            9 => [
                2 => Style::ANALYZER,
                4 => Style::IMPLEMENTER,
                3 => Style::HUMANIZER,
                1 => Style::INNOVATOR,
            ],
            10 => [
                1 => Style::ANALYZER,
                3 => Style::IMPLEMENTER,
                2 => Style::HUMANIZER,
                4 => Style::INNOVATOR,
            ],
            11 => [
                2 => Style::ANALYZER,
                4 => Style::IMPLEMENTER,
                1 => Style::HUMANIZER,
                3 => Style::INNOVATOR,
            ],
            12 => [
                1 => Style::ANALYZER,
                3 => Style::IMPLEMENTER,
                4 => Style::HUMANIZER,
                2 => Style::INNOVATOR,
            ],
            13 => [
                4 => Style::ANALYZER,
                3 => Style::IMPLEMENTER,
                2 => Style::HUMANIZER,
                1 => Style::INNOVATOR,
            ],
            14 => [
                1 => Style::ANALYZER,
                2 => Style::IMPLEMENTER,
                3 => Style::HUMANIZER,
                4 => Style::INNOVATOR,
            ],
            15 => [
                4 => Style::ANALYZER,
                3 => Style::IMPLEMENTER,
                2 => Style::HUMANIZER,
                1 => Style::INNOVATOR,
            ],
            16 => [
                4 => Style::ANALYZER,
                2 => Style::IMPLEMENTER,
                3 => Style::HUMANIZER,
                1 => Style::INNOVATOR,
            ],
            17 => [
                4 => Style::ANALYZER,
                2 => Style::IMPLEMENTER,
                3 => Style::HUMANIZER,
                1 => Style::INNOVATOR,
            ],
            18 => [
                2 => Style::ANALYZER,
                3 => Style::IMPLEMENTER,
                4 => Style::HUMANIZER,
                1 => Style::INNOVATOR,
            ],
            19 => [
                4 => Style::ANALYZER,
                1 => Style::IMPLEMENTER,
                3 => Style::HUMANIZER,
                2 => Style::INNOVATOR,
            ],
            20 => [
                1 => Style::ANALYZER,
                2 => Style::IMPLEMENTER,
                3 => Style::HUMANIZER,
                4 => Style::INNOVATOR,
            ],
            21 => [
                2 => Style::ANALYZER,
                3 => Style::IMPLEMENTER,
                4 => Style::HUMANIZER,
                1 => Style::INNOVATOR,
            ],
            22 => [
                2 => Style::ANALYZER,
                3 => Style::IMPLEMENTER,
                1 => Style::HUMANIZER,
                4 => Style::INNOVATOR,
            ],
            23 => [
                1 => Style::ANALYZER,
                4 => Style::IMPLEMENTER,
                2 => Style::HUMANIZER,
                3 => Style::INNOVATOR,
            ],
            24 => [
                2 => Style::ANALYZER,
                1 => Style::IMPLEMENTER,
                3 => Style::HUMANIZER,
                4 => Style::INNOVATOR,
            ],
        ];
    }
}
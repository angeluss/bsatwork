<?php

namespace TestBundle\Service;


use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use TestBundle\Entity\Answer;
use TestBundle\Entity\Question;
use TestBundle\Entity\Style;
use TestBundle\Entity\TestResult;
use TestBundle\Exception\ResultValidatorException;

/**
 * Class QuestionService
 *
 * @package TestBundle\Service
 */
class QuestionService
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var Serializer
     */
    private $serializer;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var CalculateTestService
     */
    private $calculateTestService;

    /**
     * @param EntityManager        $entityManager
     * @param Serializer           $serializer
     * @param ValidatorInterface   $validator
     * @param CalculateTestService $calculateTestService
     */
    public function __construct(
        EntityManager $entityManager,
        Serializer $serializer,
        ValidatorInterface $validator,
        CalculateTestService $calculateTestService
    ) {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->calculateTestService = $calculateTestService;
    }

    /**
     * @param Question $question
     *
     * @param User     $user
     *
     * @return null|Question
     */
    public function getNextQuestion(Question $question, User $user)
    {
        $nextQuestion = $this->entityManager
            ->getRepository('TestBundle:Question')
            ->getNextQuestion($question);
        if (!$nextQuestion) {
            return null;
        }
        $nextQuestionResult = $this->getQuestionResult($nextQuestion->getId(), $user);
        if ($nextQuestionResult instanceof TestResult) {
            return $nextQuestionResult->getQuestion();
        }
        if ($nextQuestion->getAnswerType() == Question::TYPE_RADIO_BUTTON) {
            $nextQuestion = $this->filterStyles($nextQuestion, $user);
        }

        $nextQuestion = $this->shuffleAnswers($nextQuestion);

        return $nextQuestion;
    }

    /**
     * @param int  $questionId
     * @param User $user
     *
     * @return null|TestResult
     */
    public function getQuestionResult($questionId, User $user)
    {
        $prevResult = $this->entityManager
            ->getRepository('TestBundle:TestResult')
            ->getUserQuestionResult($questionId, $user);
        if ($prevResult instanceof TestResult) {
            $answersArray = $prevResult->getQuestion()->getAnswers()->toArray();
            $answersMap = [];
            /** @var Answer $item */
            foreach ($answersArray as $item) {
                $answersMap[$item->getId()] = $item;
            }

            $resultAnswers = $prevResult->getResults();
            $orderedAnswer = new ArrayCollection();
            foreach ($resultAnswers as $resultAnswer) {
                if (isset($answersMap[$resultAnswer])) {
                    $orderedAnswer->add($answersMap[$resultAnswer]);
                    unset($answersMap[$resultAnswer]);
                }
            }

            foreach ($answersMap as $answer) {
                $orderedAnswer->add($answer);
            }

            $prevResult->getQuestion()->setAnswers($orderedAnswer);


            if ($prevResult->getQuestion()->getAnswerType() == Question::TYPE_RADIO_BUTTON) {
                $question = $this->filterStyles($prevResult->getQuestion(), $user);
                $prevResult->setQuestion($question);
            }

        }

        return $prevResult;
    }

    /**
     * @param array    $data
     * @param Question $question
     * @param User     $user
     *
     * @throws ResultValidatorException
     */
    public function setResult(array $data, Question $question, User $user)
    {
        $existsResult = $this->entityManager
            ->getRepository('TestBundle:TestResult')
            ->findOneBy(['user' => $user, 'question' => $question]);

        $data['results'] = isset($data['results']) ? $data['results'] : [];

        if (null != $existsResult) {
            $existsResult->setResults($data['results']);
            $constraints = $this->validator->validate($existsResult);
            if ($constraints->count() > 0) {
                throw new ResultValidatorException($constraints->get(0)->getMessage());
            }
        } else {
            $resultData = json_encode($data);
            /** @var TestResult $result */
            $result = $this->serializer->deserialize($resultData, 'TestBundle\Entity\TestResult', 'json');
            $result->setQuestion($question);
            $result->setUser($user);
            $constraints = $this->validator->validate($result);
            if ($constraints->count() > 0) {
                throw new ResultValidatorException($constraints->get(0)->getMessage());
            }
            $this->entityManager->persist($result);
        }
        $this->entityManager->flush();

    }

    /**
     * @return mixed
     */
    public function getFirstQuestion()
    {
        $firstQuestion = $this->entityManager
            ->getRepository('TestBundle:Question')
            ->getFirstQuestion();

        return $firstQuestion;
    }

    /**
     * @param Question $question
     * @param User     $user
     *
     * @return null|TestResult
     */
    public function getPrevResult(Question $question, User $user)
    {
        $result = null;
        $prevQuestion = $this->entityManager
            ->getRepository('TestBundle:Question')
            ->findOneBy(['orderType' => $question->getOrderType() - 1]);

        if ($prevQuestion instanceof Question) {
            $result = $this->getQuestionResult($prevQuestion->getId(), $user);
        }

        return $result;
    }

    /**
     * @param Question $question
     * @param User     $user
     *
     * @return Question
     */
    private function filterStyles(Question $question, User $user)
    {
        $prevResult = $this->getPrevResult($question, $user);
        if (!$prevResult) {
            return $question;
        }
        $answers = $this->entityManager
            ->getRepository('TestBundle:Answer')
            ->getRadioAnswers($question, $prevResult->getResults());
        $question->setAnswers(new ArrayCollection($answers));

        return $question;
    }

    /**
     * @param Question $question
     *
     * @return Question
     */
    public function shuffleAnswers(Question $question)
    {
        $answers = $question->getAnswers()->toArray();
        shuffle($answers);
        $question->setAnswers(new ArrayCollection($answers));

        return $question;
    }

    /**
     * @param User $user
     */
    public function buildResult(User $user)
    {
        $results = $this->calculateTestService->calculateUserTest($user);
        $this->entityManager->getRepository('AppBundle:UserTestResult')
            ->setUserResults($results, $user);
    }

    /**
     * @return mixed
     */
    public function getQuestionCount()
    {
        $questionsCount = $this->entityManager
            ->getRepository('TestBundle:Question')
            ->getQuestionsCount();

        return $questionsCount;
    }

}
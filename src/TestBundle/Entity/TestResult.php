<?php

namespace TestBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use TestBundle\Validator\Constraints as ResultAssert;

/**
 * Class TestResult
 *
 * @package TestBundle\Entity
 * @ORM\Entity(repositoryClass="TestBundle\Entity\Repository\TestResultRepository")
 * @ORM\Table(name="test_results", uniqueConstraints={@ORM\UniqueConstraint(name="result_unq_idx", columns={"user_id", "question_id"})})
 * @ResultAssert\ContainsResultValue
 */
class TestResult
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $question;

    /**
     * @var array
     *
     * @ORM\Column(name="results", type="json_array")
     *
     */
    private $results;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    public function __construct()
    {
        $this->id = md5(uniqid() . mt_rand());
        $this->results = [];
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param Question $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return array
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param array $results
     */
    public function setResults($results)
    {
        $results = array_filter($results);
        $this->results = $results;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
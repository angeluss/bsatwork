<?php

namespace TestBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Translatable;
use Gedmo\Mapping\Annotation as Gedmo;
use TestBundle\Entity\Translation\StyleTranslation;

/**
 * Class Style
 *
 * @package TestBundle\Entity
 * @ORM\Entity(repositoryClass="TestBundle\Entity\Repository\StyleRepository")
 * @ORM\Table(name="test_styles")
 * @Gedmo\TranslationEntity(class="TestBundle\Entity\Translation\StyleTranslation")
 */
class Style implements Translatable
{
    const ANALYZER = 'Analyser';
    const IMPLEMENTER = 'Implementer';
    const HUMANIZER = 'Humaniser';
    const INNOVATOR = 'Innovator';
    const CONCEPTUALISER = 'Conceptualiser';
    const CHANGE_MANAGER = 'Change Manager';
    const CHANGE_MASTER = 'Change Master';
    const CONNECTOR = 'Connector';
    const GENERALIST = 'Generalist';
    const IDEALIST = 'Idealist';
    const INVENTOR = 'Inventor';
    const ORGANIZER = 'Organiser';
    const REALIST = 'Realist';
    const SYSTEMISER = 'Systemiser';
    const TEACHER = 'Teacher';

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=45, unique=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="order_type", type="integer", unique=true, nullable=false)
     */
    private $orderType;

    /**
     * @ORM\OneToMany(
     *   targetEntity="AppBundle\Entity\BrainstyleCharacteristic",
     *   mappedBy="style",
     *   cascade={"persist", "remove"}
     * )
     */
    private $characteristics;

    /**
     * @ORM\OneToMany(
     *   targetEntity="AppBundle\Entity\BrainstyleTip",
     *   mappedBy="style",
     *   cascade={"persist", "remove"}
     * )
     */
    private $tips;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *   targetEntity="TestBundle\Entity\Translation\StyleTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;


    public function __construct()
    {
        $this->id = md5(uniqid() . mt_rand());
        $this->translations = new ArrayCollection();
        $this->characteristics = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param StyleTranslation $t
     */
    public function addTranslation(StyleTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getOrderType()
    {
        return $this->orderType;
    }

    /**
     * @param int $orderType
     */
    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;
    }

    /**
     * @return mixed
     */
    public function getCharacteristics()
    {
        return $this->characteristics;
    }

    /**
     * @param mixed $characteristics
     */
    public function setCharacteristics($characteristics)
    {
        $this->characteristics = $characteristics;
    }

    /**
     * @return mixed
     */
    public function getTips()
    {
        return $this->tips;
    }

    /**
     * @param mixed $tips
     */
    public function setTips($tips)
    {
        $this->tips = $tips;
    }
}
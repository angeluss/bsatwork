<?php

namespace TestBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use TestBundle\Entity\Translation\QuestionTranslation;

/**
 * Class Question
 *
 * @package TestBundle\Entity
 * @ORM\Entity(repositoryClass="TestBundle\Entity\Repository\QuestionRepository")
 * @ORM\Table(name="test_question")
 * @Gedmo\TranslationEntity(class="TestBundle\Entity\Translation\QuestionTranslation")
 */
class Question implements Translatable
{
    const TYPE_LIST = 'list';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_RADIO_BUTTON = 'radio_button';

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var string
     * @ORM\Column(name="hash", type="string")
     */
    private $hash;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_type", type="integer", unique=true)
     */
    private $orderType;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="question")
     */
    private $answers;

    /**
     * @var string
     *
     * @ORM\Column(name="answer_type", type="string")
     */
    private $answerType;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_available_result", type="integer")
     */
    private $minAvailableResult;

    /**
     * @ORM\OneToMany(
     *   targetEntity="TestBundle\Entity\Translation\QuestionTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;

    public function __construct()
    {
        $this->id = md5(uniqid() . mt_rand());
        $this->answers = new ArrayCollection();
        $this->translations = new ArrayCollection();
        $this->hash = uniqid();
    }

    /**
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param QuestionTranslation $t
     */
    public function addTranslation(QuestionTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param ArrayCollection $answers
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    /**
     * @param Answer $answer
     */
    public function addAnswer(Answer $answer)
    {
        if (!$this->answers->contains($answer)) {
            $this->answers->add($answer);
        }
    }

    /**
     * @param Answer $answer
     */
    public function deleteAnswer(Answer $answer)
    {
        if ($this->answers->contains($answer)) {
            $this->answers->removeElement($answer);
        }
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return integer
     */
    public function getOrderType()
    {
        return $this->orderType;
    }

    /**
     * @param integer $orderType
     */
    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;
    }

    /**
     * @return string
     */
    public function getAnswerType()
    {
        return $this->answerType;
    }

    /**
     * @param string $answerType
     * @throws \Exception
     */
    public function setAnswerType($answerType)
    {
        if (!in_array($answerType, self::getAvailableAnswerTypes())) {
            throw new \Exception('Unsupported question type');
        }
        $this->answerType = $answerType;
    }

    /**
     * @return array
     */
    public static function getAvailableAnswerTypes()
    {
        return [self::TYPE_LIST, self::TYPE_CHECKBOX, self::TYPE_RADIO_BUTTON];
    }

    /**
     * @return int
     */
    public function getMinAvailableResult()
    {
        return $this->minAvailableResult;
    }

    /**
     * @param int $minAvailableResult
     */
    public function setMinAvailableResult($minAvailableResult)
    {
        $this->minAvailableResult = $minAvailableResult;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getText();
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

}
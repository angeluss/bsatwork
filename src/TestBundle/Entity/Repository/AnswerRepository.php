<?php

namespace TestBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use TestBundle\Entity\Question;

/**
 * Class AnswerRepository
 *
 * @package TestBundle\Entity\Repository
 */
class AnswerRepository extends EntityRepository
{

    /**
     * @param Question $question
     * @param array    $ids
     *
     * @return array
     */
    public function getRadioAnswers(Question $question, array $ids)
    {
        $orderIds = $this->createQueryBuilder('a')
            ->select('a.resultId')->distinct()
            ->where('a.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getArrayResult();

        $qb = $this->createQueryBuilder('a');
        $qb->where('a.resultId IN (:rids)')
            ->andWhere('a.question = :question')
            ->setParameter('rids', $orderIds)
            ->setParameter('question', $question);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function getAnswers(array $ids)
    {
        return $this->createQueryBuilder('a')
            ->where('a.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getArrayResult();
    }
}
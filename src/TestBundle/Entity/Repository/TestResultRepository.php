<?php

namespace TestBundle\Entity\Repository;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use TestBundle\Entity\Question;
use TestBundle\Entity\TestResult;

/**
 * Class TestResultRepository
 *
 * @package TestBundle\Entity\Repository
 */
class TestResultRepository extends EntityRepository
{
    /**
     * @param int  $questionId
     * @param User $user
     *
     * @return null|TestResult
     */
    public function getUserQuestionResult($questionId, User $user)
    {
        $prevResultQuery = $this->createQueryBuilder('tr')
            ->where('tr.user = :user')
            ->andWhere('tr.question = :question')
            ->setParameters(['user' => $user, 'question' => $questionId])
            ->getQuery();

        return $prevResultQuery->getOneOrNullResult();
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getUserResults(User $user)
    {
        $query = $this->createQueryBuilder('tr')
            ->select('tr', 'tq', 'ans')
            ->join('tr.question', 'tq')
            ->join('tq.answers', 'ans')
            ->where('tr.user = :user')
            ->setParameter('user', $user)
            ->orderBy('tq.orderType')
            ->getQuery();

        return $query->getArrayResult();
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getDynamicListResult(User $user)
    {
        $query = $this->createQueryBuilder('tr')
            ->select('tr', 'tq', 'ans')
            ->join('tr.question', 'tq')
            ->join('tq.answers', 'ans')
            ->where('tr.user = :user')
            ->andWhere('tq.answerType IN (:types)')
            ->setParameter('user', $user)
            ->setParameter('types', [Question::TYPE_CHECKBOX, Question::TYPE_RADIO_BUTTON])
            ->getQuery();

        return $query->getArrayResult();
    }
}
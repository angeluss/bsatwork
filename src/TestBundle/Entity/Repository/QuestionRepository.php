<?php

namespace TestBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use TestBundle\Entity\Question;

/**
 * Class QuestionRepository
 *
 * @package TestBundle\Entity\Repository
 */
class QuestionRepository extends EntityRepository
{
    /**
     * @param Question $question
     *
     * @return Question|null
     */
    public function getNextQuestion(Question $question)
    {

        $query = $this->createQueryBuilder('q')
            ->where('q.orderType = :orderType')
            ->setMaxResults(1)
            ->getQuery();
        $query->setParameter('orderType', $question->getOrderType() + 1);

        return $query->getOneOrNullResult();
    }


    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getFirstQuestion()
    {
        $query = $this->createQueryBuilder('q')
            ->where('q.orderType = 1')
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getQuestionsCount()
    {
        $query = $this->createQueryBuilder('q')
            ->select('COUNT(q.id)')
            ->getQuery();

        return $query->getSingleScalarResult();
    }
}
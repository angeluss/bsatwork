<?php

namespace TestBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use TestBundle\Entity\Question;

/**
 * Class StyleRepository
 *
 * @package TestBundle\Entity\Repository
 */
class StyleRepository extends EntityRepository
{
    /**
     * @param string $name
     *
     * @return null|object
     */
    public function getStyleByName($name)
    {
        return $this->findOneBy(['name' => $name]);
    }

    /**
     * @return array
     */
    public function getMainStyles()
    {
        $qb = $this->createQueryBuilder('s');
        $qb->where($qb->expr()->lte('s.orderType', 8))
            ->addOrderBy('s.orderType');

        return $qb->getQuery()->getResult();
    }
}
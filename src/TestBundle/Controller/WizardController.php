<?php

namespace TestBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Annotations\NoTestNoAccess;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Exception\ValidatorException;
use TestBundle\Entity\Question;
use TestBundle\Entity\TestResult;
use TestBundle\Exception\ResultValidatorException;

/**
 * Class WizardController
 *
 * @package TestBundle\Controller
 */
class WizardController extends Controller
{
    /**
     * @Route("/test", name="test_index", options={"expose"=true})
     * @Security("has_role('ROLE_USER')")
     * @return Response
     */
    public function indexAction()
    {
        if ($this->getUser()->isTestPass()) {
            return $this->redirect($this->generateUrl('dashboard_index'));
        }
        $question = $this->get('testbundle.questionservice')->getFirstQuestion();
        $questionsCount = $this->get('testbundle.questionservice')->getQuestionCount();
        $question = $this->get('testbundle.questionservice')->shuffleAnswers($question);

        $serializer = $this->get('jms_serializer');
        $nextQuestionSerialized = $serializer->serialize($question, 'json');
        $question = json_decode($nextQuestionSerialized, true);

        return $this->render('@Test/wizard/index.html.twig', [
            'question' => $question,
            'questions_count' => $questionsCount
        ]);
    }

    /**
     * @param Request $request Request
     * @Route("/user-info", name="user_info", options={"expose"=true})
     * @NoTestNoAccess
     * @Security("has_role('ROLE_USER')")
     *
     * @return Response
     */
    public function userInfoAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm('user_info', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('test_last_step');
        }

        return $this->render('@Test/wizard/user_info.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/test-explanation", name="test_explanation", options={"expose"=true})
     * @Security("has_role('ROLE_USER')")
     * @return Response
     */
    public function explanationPageAction()
    {
        return $this->render('@Test/wizard/explanation.html.twig');
    }

    /**
     * @Route("/test-last-step", name="test_last_step", options={"expose"=true})
     * @Security("has_role('ROLE_USER')")
     * @NoTestNoAccess
     * @return Response
     */
    public function lastPageAction()
    {
        return $this->render('@Test/wizard/last_step.html.twig');
    }

    /**
     * @param Request $request
     *
     * @Route("/next-qw", name="test_next_qw", options={"expose"=true})
     * @Security("has_role('ROLE_USER')")
     * @Method("POST")
     *
     * @return Response
     */
    public function nextQuestionAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        /** @var User $user */
        $user = $this->getUser();
        $question = $this->get('doctrine.orm.entity_manager')
            ->getRepository('TestBundle:Question')
            ->findOneBy(['hash' => $request->get('hash')]);
        $questionService = $this->get('testbundle.questionservice');
        try {
            $questionService->setResult(
                $request->request->all(),
                $question,
                $user
            );
        } catch (ResultValidatorException $e) {
            return new JsonResponse(['status' => 'error', 'message' => $this->get('translator')->trans($e->getMessage())]);
        }
        $nextQuestion = $questionService->getNextQuestion($question, $user);
        if (!$nextQuestion) {
            $questionService->buildResult($user);

            return new JsonResponse(['last_question' => true]);
        }
        $serializer = $this->get('jms_serializer');
        $data = [
            'question' => $nextQuestion,
            'status' => 'ok',
            'has_previous' => true
        ];

        if (in_array($nextQuestion->getAnswerType(), [Question::TYPE_CHECKBOX, Question::TYPE_RADIO_BUTTON])) {
            $questionResult = $questionService->getQuestionResult($nextQuestion->getId(), $this->getUser());
            if ($questionResult instanceof TestResult) {
                $data['selected'] = $questionResult->getResults();
            }

        }
        $nextQuestionSerialized = $serializer->serialize($data, 'json');

        $this->get('session')->set('prev_question_number', $question->getId());

        return new Response($nextQuestionSerialized, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param Request $request
     *
     * @Route("/prew-qw", name="test_prev_qw", options={"expose"=true})
     * @Security("has_role('ROLE_USER')")
     *
     * @return Response
     */
    public function previousQuestionAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        /** @var TestResult $testResult */
        $testResult = $this->get('testbundle.questionservice')
            ->getQuestionResult($this->get('session')->get('prev_question_number'), $this->getUser());
        if (null == $testResult) {
            return new JsonResponse(['status' => 'error', 'has_previous' => false]);
        }

        /** @var TestResult $currentResult */
        $prevResult = $this->get('testbundle.questionservice')
            ->getPrevResult($testResult->getQuestion(), $this->getUser());
        $hasPrevious = false;
        if ($prevResult) {
            $hasPrevious = true;
            $this->get('session')->set('prev_question_number', $prevResult->getQuestion()->getId());
        }
        $data = [
            'question' => $testResult->getQuestion(),
            'status' => 'ok',
            'has_previous' => $hasPrevious
        ];
        if (in_array($testResult->getQuestion()->getAnswerType(),
            [Question::TYPE_CHECKBOX, Question::TYPE_RADIO_BUTTON])) {
            $data['selected'] = $testResult->getResults();
        }
        $serializer = $this->get('jms_serializer');
        $prevData = $serializer->serialize($data, 'json');

        return new Response($prevData, 200, ['Content-Type' => 'application/json']);
    }
}
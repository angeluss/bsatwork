<?php

namespace TestBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use TestBundle\Entity\Question;
use TestBundle\Entity\TestResult;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadTestResultData
 *
 * @package TestBundle\DataFixtures\ORM
 */
class LoadTestResultsData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
//        $users = [
//            'manager-user',
//            'employer-user',
//            'employer1-user',
//            'employer2-user',
//            'employer3-user',
//            'employer4-user',
//            'employer5-user',
//            'employer6-user',
//            'employer7-user',
//            'employer8-user',
//            'employer9-user',
//            'employer10-user',
//            'employer11-user',
//            'employer12-user',
//        ];
//
//        $questions = [
//            '1-question',
//            '2-question',
//            '3-question',
//            '4-question',
//            '5-question',
//            '6-question',
//            '7-question',
//            '8-question',
//            '9-question',
//            '10-question',
//            '11-question',
//            '12-question',
//            '13-question',
//            '14-question',
//            '15-question',
//            '16-question',
//            '17-question',
//            '18-question',
//            '19-question',
//            '20-question',
//            '21-question',
//            '22-question',
//            '23-question',
//            '24-question',
//        ];
//
//        for ($i = 1; $i < count($users); $i++) {
//            for ($j = 1; $j <= count($questions); $j++) {
//                $testResult = new TestResult();
//                $testResult->setQuestion($this->getReference($questions[$j - 1]));
//                $answers = [
//                    $this->getReference('1-answer-' . $j . '-question')->getId(),
//                    $this->getReference('2-answer-' . $j . '-question')->getId(),
//                    $this->getReference('3-answer-' . $j . '-question')->getId(),
//                    $this->getReference('4-answer-' . $j . '-question')->getId(),
//                ];
//                shuffle($answers);
//                for ($c = 0; $c < count($answers); $c++) {
//                    $testResult->setResults([
//                        '1' => $answers[0],
//                        '2' => $answers[1],
//                        '3' => $answers[2],
//                        '4' => $answers[3],
//                    ]);
//                }
//                $testResult->setUser($this->getReference($users[$i - 1]));
//                $manager->persist($testResult);
//                $this->addReference($i . '-test-' . $j . '-question-result', $testResult);
//            }
//
//
//            // test result for question with checkboxes
//            $testResult = new TestResult();
//            $testResult->setQuestion($this->getReference('25-question'));
//            $answers = [
//                $this->getReference('1-answer-25-question')->getId(),
//                $this->getReference('2-answer-25-question')->getId(),
//                $this->getReference('3-answer-25-question')->getId(),
//                $this->getReference('4-answer-25-question')->getId(),
//                $this->getReference('5-answer-25-question')->getId(),
//                $this->getReference('6-answer-25-question')->getId(),
//                $this->getReference('7-answer-25-question')->getId(),
//                $this->getReference('8-answer-25-question')->getId(),
//                $this->getReference('9-answer-25-question')->getId(),
//                $this->getReference('10-answer-25-question')->getId(),
//                $this->getReference('11-answer-25-question')->getId(),
//                $this->getReference('12-answer-25-question')->getId(),
//                $this->getReference('13-answer-25-question')->getId(),
//                $this->getReference('14-answer-25-question')->getId(),
//                $this->getReference('15-answer-25-question')->getId(),
//                $this->getReference('16-answer-25-question')->getId(),
//            ];
//            shuffle($answers);
//            $testResult->setResults([
//                '1' => $answers[0],
//                '2' => $answers[1],
//                '3' => $answers[2],
//                '4' => $answers[3],
//                '5' => $answers[4],
//                '6' => $answers[5],
//                '7' => $answers[6],
//                '8' => $answers[7],
//            ]);
//            $testResult->setUser($this->getReference($users[$i - 1]));
//            $manager->persist($testResult);
//            $this->addReference($i . '-test-25-question-result', $testResult);
//
//            // test result for question with checkboxes
//            $testResult = new TestResult();
//            $testResult->setQuestion($this->getReference('26-question'));
//            shuffle($answers);
//            $testResult->setResults([
//                '1' => $answers[0]
//            ]);
//            $testResult->setUser($this->getReference($users[$i - 1]));
//            $manager->persist($testResult);
//            $manager->flush();
//            $this->addReference($i . '-test-26-question-result', $testResult);
//            //calculating test results
//            $user = $this->getReference($users[$i - 1]);
//            $this->container->get('testbundle.questionservice')->buildResult($user);
//        }
//
//        $this->loadArturResult($manager);
    }

    private function loadArturResult(ObjectManager $manager)
    {

        $arturResults = [
            1 => [1 => 2, 2 => 1, 3 => 4, 4 => 3],
            2 => [1 => 2, 2 => 3, 3 => 4, 4 => 1],
            3 => [1 => 1, 2 => 2, 3 => 4, 4 => 3],
            4 => [1 => 4, 2 => 1, 3 => 3, 4 => 2],
            5 => [1 => 3, 2 => 2, 3 => 4, 4 => 1],
            6 => [1 => 4, 2 => 3, 3 => 2, 4 => 1],
            7 => [1 => 2, 2 => 3, 3 => 1, 4 => 4],
            8 => [1 => 3, 2 => 4, 3 => 2, 4 => 1],
            9 => [1 => 2, 2 => 4, 3 => 3, 4 => 1],
            10 => [1 => 1, 2 => 4, 3 => 2, 4 => 3],
            11 => [1 => 4, 2 => 2, 3 => 3, 4 => 1],
            12 => [1 => 4, 2 => 3, 3 => 1, 4 => 2],
            13 => [1 => 2, 2 => 4, 3 => 1, 4 => 3],
            14 => [1 => 3, 2 => 4, 3 => 1, 4 => 2],
            15 => [1 => 3, 2 => 1, 3 => 4, 4 => 2],
            16 => [1 => 1, 2 => 2, 3 => 4, 4 => 3],
            17 => [1 => 4, 2 => 2, 3 => 3, 4 => 1],
            18 => [1 => 4, 2 => 3, 3 => 1, 4 => 2],
            19 => [1 => 3, 2 => 2, 3 => 4, 4 => 1],
            20 => [1 => 4, 2 => 3, 3 => 1, 4 => 2],
            21 => [1 => 2, 2 => 4, 3 => 1, 4 => 3],
            22 => [1 => 1, 2 => 3, 3 => 4, 4 => 2],
            23 => [1 => 3, 2 => 2, 3 => 1, 4 => 4],
            24 => [1 => 2, 2 => 3, 3 => 1, 4 => 4],
            25 => [1 => 13, 2 => 14, 3 => 16, 4 => 3, 5 => 4, 6 => 9, 7 => 12, 9 => 10],
            26 => [1 => 13],
        ];

        /** @var User $arturUser */
        $arturUser = $this->getReference('artur-user');
        for ($i = 1; $i < 27; $i++) {
            /** @var Question $question */
            $question = $this->getReference($i . '-question');

            $testResult = new TestResult();
            $testResult->setUser($arturUser);
            $testResult->setQuestion($question);
            $answers = [];
            foreach ($arturResults[$i] as $key => $ans) {
                $answers[$key] = $this->getReference($ans . '-answer-' . $i . '-question')->getId();
            }
            $testResult->setResults($answers);
            $manager->persist($testResult);
        }
        $manager->flush();
        $this->container->get('testbundle.questionservice')->buildResult($arturUser);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 40;
    }
}
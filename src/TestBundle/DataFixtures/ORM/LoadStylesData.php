<?php

namespace TestBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TestBundle\Entity\Answer;
use TestBundle\Entity\Question;
use TestBundle\Entity\Style;
use TestBundle\Entity\Translation\AnswerTranslation;
use TestBundle\Entity\Translation\QuestionTranslation;
use TestBundle\Entity\Translation\StyleTranslation;

/**
 * Class LoadStylesData
 *
 * @package TestBundle\DataFixtures\ORM
 */
class LoadStylesData extends  AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $engData = [
            Style::CONCEPTUALISER,
            Style::INNOVATOR,
            Style::CONNECTOR,
            Style::HUMANIZER,
            Style::ORGANIZER,
            Style::IMPLEMENTER,
            Style::SYSTEMISER,
            Style::ANALYZER,
            Style::GENERALIST,
            Style::CHANGE_MASTER,
            Style::TEACHER,
            Style::CHANGE_MANAGER,
            Style::INVENTOR,
            Style::IDEALIST,
            Style::REALIST,
        ];

        $nlData = [
            Style::CONCEPTUALISER,
            Style::INNOVATOR,
            Style::CONNECTOR,
            Style::HUMANIZER,
            Style::ORGANIZER,
            Style::IMPLEMENTER,
            Style::SYSTEMISER,
            Style::ANALYZER,
            Style::GENERALIST,
            Style::CHANGE_MASTER,
            Style::TEACHER,
            Style::CHANGE_MANAGER,
            Style::INVENTOR,
            Style::IDEALIST,
            Style::REALIST,
        ];
        foreach ($engData as $key => $engStyle) {
            $style = new Style();
            $style->setOrderType($key+1);
            $style->setName($engStyle);
            $nl = new StyleTranslation('nl', 'name', $nlData[$key]);
            $style->addTranslation($nl);
            $manager->persist($nl);
            $manager->persist($style);
            $this->setReference('style-'. $engStyle, $style);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}
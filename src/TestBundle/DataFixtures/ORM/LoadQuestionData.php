<?php

namespace TestBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TestBundle\Entity\Answer;
use TestBundle\Entity\Question;
use TestBundle\Entity\Translation\AnswerTranslation;
use TestBundle\Entity\Translation\QuestionTranslation;

/**
 * Class LoadQuestionData
 *
 * @package TestBundle\DataFixtures\ORM
 */
class LoadQuestionData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $questionsen = [
            'When it comes to planning, I would prefer...',
            'Regarding my accountability for the financial aspect of my job, I prefer...',
            'Administrative duties I prefer are...',
            'When it comes to problem solving, I prefer...',
            'The training and development function I prefer is...',
            'I tend to make decisions based on...',
            'At the workplace when I wish to influence another person toward accepting my idea, I would...',
            'My favourite room exudes an atmosphere that...',
            'I prefer participating in meetings which include...',
            'I most value information which...',
            'I enjoy talking to people when we can...',
            'When organizing, I prefer the task of...',
            'I have trouble communicating with people who...',
            'Sometimes people say about me that I am...',
            'A task that fits me very well is...',
            'Rules that don’t make sense should be...',
            'If I like to get information about something, I prefer...',
            'Work situations which excite me are...',
            'My favorite leisure activities provide an opportunity for...',
            'When I have read a book, I remember...',
            'People who know me well would describe me as:',
            'I have most in common with people who...',
            'If I were included in developing a marketing strategy, I would most enjoy...',
            'In developing my own five year wish list, it would include...',
        ];

        $questionsNl = [
            'Als het om plannen gaat, geef ik er de voorkeur aan om...',
            'Als het om mijn verantwoordelijkheid voor de financiële aspecten van mijn werk gaat, geef ik er de voorkeur aan om...',
            'De managementtaken die ik prefereer zijn...',
            'Als het gaat om het oplossen van problemen, geef ik de voorkeur aan...',
            'Ik train en ontwikkel mensen het liefst door...',
            'Ik baseer mijn beslissingen meestal op...',
            'Als ik iemand anders wil beïnvloeden om een idee te accepteren, zou ik...',
            'Mijn favoriete kamer is...',
            'Ik vind het prettig om vergaderingen bij te wonen waar we...',
            'Ik vind het volgende type informatie het meest waardevol...',
            'Ik vind het prettig om gesprekken te voeren met mensen, als we...',
            'Als het gaat om organiseren, geef ik de voorkeur aan de volgende taak...',
            'Ik heb problemen met het communiceren met mensen die...',
            'Er wordt ook wel van mij gezegd dat ik...',
            'De volgende opdracht is mij op het lijf geschreven...',
            'Ik zou kiezen voor...',
            'Als ik ergens over geïnformeerd wil worden, ben ik op zoek naar het volgende type informatie...',
            'Werksituaties die ik opwindend vind, zijn...',
            'Ik heb het liefst informatie in de vorm van...',
            'Als ik een boek gelezen heb, onthoud ik vooral...',
            'Mensen die mij echt goed kennen zouden mij beschrijven als:',
            'In mijn contact met andere mensen, lijk ik het meest op mensen die...',
            'Als ik betrokken was bij het ontwikkelen van een marketingstrategie, zou ik het liefst...',
            'Ik neem binnen een team het liefst de volgende rol in...',
        ];

        $answersNl = [
            [
                '...een 10-jaren visie te presenteren die aangeeft welke richting de organisatie zal inslaan.',
                '...de feiten te onderzoeken op basis waarvan we voorspellingen kunnen doen.',
                '...een planningsessie met ons team te begeleiden.',
                '...het actieplan uit te voeren als de visie is geformuleerd.',
            ],
            [
                '...financiële overzichten samen te stellen of toe te lichten.',
                '...financiële overzichten te analyseren naar maanden/kwartalen/jaren.',
                '...financiële overzichten te gebruiken alleen voor mijn inzicht in het grotere geheel.',
                '...informeel te praten met anderen die gekwalificeerd zijn en mij op de hoogte houden.',

            ],
            [
                '...menselijke relaties onderhouden.',
                '...ervoor zorgen dat plannen en procedures gevolgd worden.',
                '...nieuwe manieren vinden om het werk gedaan te krijgen.',
                '...met getallen en gegevens werken.',
            ],
            [
                '...de feiten te onderzoeken om goed te zien wat precies het probleem is.',
                '...het probleem met anderen te bespreken, om te weten wat er leeft.',
                '...te komen met originele en innovatieve oplossingen.',
                '...een overeengekomen oplossing te implementeren.',
            ],
            [
                '...met hen samen te werken en hen laten zien hoe het werk gedaan moet worden.',
                '...hun functioneren te analyseren en hen gebieden voor verbetering aan te reiken.',
                '...ze uit te dagen om hun eigen ontdekkingen te doen en van hun ervaringen te leren.',
                '...open te staan voor hun behoeften en hen te helpen in deze behoeften te voorzien.',
            ],
            [
                '...logisch, stapsgewijs denken.',
                '...ervaringen uit het verleden.',
                '...mijn emotionele reactie.',
                '...mijn inzicht en intuïtie.',
            ],
            [
                '...een logische redenering opbouwen die deze persoon niet onderuit kan halen.',
                '...stap voor stap met deze persoon door de principes en details van mijn idee lopen.',
                '...samen met deze persoon doornemen welke persoonlijke voordelen dit voor hem heeft.',
                '...de unieke aspecten van mijn idee aangeven die nieuwe perspectieven kunnen bieden.',

            ],
            [
                '...netjes en ordelijk.',
                '...ingericht waar die voor bedoeld is.',
                '...inspirerend en stimulerend.',
                '...warm en vriendelijk.',

            ],
            [
                '...brainstormen over ideeën.',
                '...informatie uitwisselen.',
                '...aan teambuilding doen.',
                '...besluiten nemen.',
            ],
            [
                '...informatie die feiten onderscheidt van meningen en zo een basis vormt voor analyse.',
                '...informatie die persoonlijke betekenis heeft en motiverend werkt.',
                '...informatie die gebruikt kan worden om resultaat te behalen.',
                '...informatie die verborgen mogelijkheden of nieuwe kansen biedt.',
            ],
            [
                '...onze gevoelens eerlijk kunnen uiten.',
                '...een gezond debat kunnen voeren.',
                '...met nieuwe ideeën of inzichten kunnen komen.',
                '...iets tot stand kunnen brenen.',
            ],
            [
                '...prioriteiten stellen en werk toedelen aan de meest capabele mensen.',
                '...overzicht krijgen over het grote geheel en hoe alles in elkaar zal passen.',
                '...de stappen controleren en toezien op de details.',
                '...manieren vinden om effectieve communicatie te verzekeren.',
            ],
            [
                '...het kernpunt van wat ik probeer te zeggen, niet zien.',
                '...niet ontvankelijk zijn voor gevoelens.',
                '...hun gedachten niet ordenen en steeds van onderwerp veranderen.',
                '...onlogisch zijn.',

            ],
            [
                '...berekenend ben.',
                '...rechtlijnig ben.',
                '...teveel rekening houd met anderen.',
                '...onrealistisch ben.',
            ],
            [
                '...bedenken hoe we iets op een andere manier kunnen benaderen.',
                '...de sfeer proeven om te voelen wat er speelt.',
                '...kijken hoe we iets handiger en efficiënter kunnen organiseren.',
                '...uitzoeken en in kaart brengen wat het probleem nu precies is.',

            ],
            [
                '...de meest kansrijke oplossing.',
                '...de meest praktische oplossing.',
                '...de oplossing waar de meeste overeenstemming over is.',
                '...de meest doordachte oplossing.',
            ],
            [
                '...informatie waar alleen de belangrijkste zaken zijn aangegeven.',
                '...een handboek dat je stap voor stap kunt volgen.',
                '...informatie die iemand je persoonlijk geeft.',
                '...uitgebreide informatie, logisch geordend.',
            ],
            [
                '...een unieke ontdekking doen die de doorbraak vormt voor een chronisch organisatieprobleem.',
                '...allerlei gegevens kwantificeren en daardoor begrijpen welke keuze je moet maken.',
                '...een belangrijk project uitvoeren en tot een goed einde brengen, stap voor stap.',
                '...onderhandelen tot we een oplossing bereiken, die acceptabel is voor alle deelnemers.',
            ],
            [
                '...geschreven tekst en cijfers.',
                '...kleuren en tekeningen.',
                '...persoonlijke overdracht.',
                '...schema\'s en symbolen.',

            ],
            [
                '...wat ik er wel of niet goed aan vond.',
                '...de opbouw of het verloop van het verhaal.',
                '...de emoties die ik doormaakte tijdens het lezen.',
                '...wat ik dacht dat de schrijver probeerde over te brengen.',

            ],
            [
                '...een visionair die vooral de grote lijnen ziet.',
                '...iemand die rationeel beslissingen neemt.',
                '...een echte planner, die de zaken onder controle heeft.',
                '...iemand die goed kan luisteren.',
            ],
            [
                '...ondersteunend zijn en rekening houden met andermans gevoelens.',
                '...anderen kritisch, maar op een opbouwende manier benaderen.',
                '...hun afspraken nakomen.',
                '...altijd nieuwe, creatieve mogelijkheden zien.',
            ],
            [
                '...onze concurrentie analyseren en de beste positionering van ons product aanbevelen.',
                '...de behoeften van klanten in kaart brengen d.m.v. empathie en intuïtie.',
                '...toekomsttrends voorspellen om de mogelijkheden voor innovatie te bepalen.',
                '...ervaringen uit het verleden bestuderen en de strategieën kiezen die de tand des tijd doorstaan hebben.',

            ],
            [
                '...het plannen van het werk en controleren van de voortgang.',
                '...de inhoudelijke expert.',
                '...de coach die het welzijn van de teamleden in de gaten houdt en het proces bewaakt.',
                '...degene die steeds met nieuwe ideeën komt en die risico\'s neemt.',
            ],
        ];

        $answersen = [
            [
                '...Coming up with a 10 year vision statement, describing the direction our organization will take.',
                '...Researching and analyzing the facts and figures on which we might make projections.',
                '...Facilitating a planning session for our team.',
                '...Developing the plan after the research and the vision statement are completed.',
            ],
            [
                '...Providing and/or explaining financial reports.',
                '...Analyzing financial reports and making comparisons to other months, quarters, or years.',
                '...Utilizing financial reports, only when necessary to support my interpretations of the big picture.',
                '...Informally talking with others, who are qualified and willing to keep me informed.',

            ],
            [
                '...Dealing with human relations.',
                '...Insuring established plans and procedures are followed.',
                '...Finding new ways to get the work done.',
                '...Working with numbers and data.',
            ],
            [
                '...Researching the facts and/or figures in order to define the problem.',
                '...Discussing the problem with others in order to get different feelings and opinions about the situation.',
                '...Coming up with an innovative solution to the problem.',
                '...Implementing the agreed upon solution.',
            ],
            [
                '...Working with people to tell them and show them how to do the job.',
                '...Analyzing and evaluating a person’s performance and suggesting areas for improvement.',
                '...Challenging people to discover on their own and learn from their experience.',
                '...Being sensitive to peoples needs, and assisting them in meeting those needs.',
            ], [
                '...considering it well first.',
                '...experiences I have gained in the past.',
                '...my emotional reaction.',
                '...my intuition.',
            ], [
                '...Develop a logical rationale that the person can not refute.',
                '...Walk the person through the details and sound principles of my ideas, one step at a time.',
                '...Share reasons why my ideas will bring personal satisfaction to that person.',
                '...Point out the unique factors of my ideas which can open new vistas.',
            ], [
                '...is neat and orderly.',
                '...has been furnished for its purpose.',
                '...inspires and stimulates.',
                '...is warm and friendly.',
            ], [
                '...Brainstorming ideas.',
                '...Information exchange.',
                '...Team development.',
                '...Decision making.',
            ], [
                '...Separates facts from opinions, providing a base for analysis.',
                '...Provides personal meaning and stimulates motivation.',
                '...Can be put to use in producing results.',
                '...Offers hidden possibilities or new opportunities.',
            ], [
                '...Express our feelings honestly.',
                '...Have a healthy debate.',
                '...Come to new ideas or views.',
                '...Come to something substantial.',

            ], [
                '...Setting priorities and assigning work to the most capable people.',
                '...Considering the big picture and how everything will fit.',
                '...Controlling the steps and seeing to details.',
                '...Finding ways to ensure effective communication.',
            ], [
                '...Can’t see the main point of what I am trying to say.',
                '...Are insensitive to feelings.',
                '...Don’t sequence their thoughts and constantly change the subject.',
                '...Who are illogical.',
            ], [
                '...calculating, putting my own interests first.',
                '...straight, inflexible.',
                '...too considerate of others, too soft.',
                '...unrealistic, a dreamer, walking too far ahead of the troops.',

            ], [
                '...thinking about different ways to approach a complex situation.',
                '...sensing the atmosphere to know what is going on.',
                '...see how we can organise a task in a more practical and efficient way.',
                '...do some research and figure out what exactly is the problem.',

            ], [
                '...Challened.',
                '...Followed.',
                '...Flexible.',
                '...Evaluated.',

            ], [
                '...an overview of the most important key points, with colored highlights or dots.',
                '...a handbook that I can follow, step by step.',
                '...a person showing and telling me what I want to know.',
                '...detailed information, in a logical order, with figures, charts or tables.',
            ], [
                '...Discovering a unique breakthrough solution for a chronic business problem.',
                '...Quantifying and therefore understanding how to make a difficult choice.',
                '...Completing an important project, one step at a time.',
                '...Negotiating a solution within a work group which is acceptable to all members.',

            ], [
                '...Developing skills.',
                '...Emotional excitement.',
                '...Artistic expression.',
                '...Exercising my mind.',

            ], [
                '...What I liked and/or didn’t like about it.',
                '...The sequence of the story or content.',
                '...The emotions that I felt while reading it.',
                '...The message I thought the author was trying to convey.',

            ], [
                '...a visionary who sees mainly the “big picture”.',
                '...someone who would take decisions rationally.',
                '...an effective planner who follows through.',
                '...someone who listens well.',
            ], [
                '...Are supportive and consider other people’s feelings.',
                '...Approach others in a critical but constructive manner.',
                '...When asked a question, provide appropriate, common sense answers.',
                '...Always see new, creative possibilities.',
            ], [
                '...Analyzing our competition and recommending the best positioning of our product and/or service.',
                '...Identifying the needs of consumers through empathy and intuition.',
                '...Predicting future trends in order to determine opportunities for innovation.',
                '...Reviewing past experiences, and selecting strategies which have stood the test of time.',
            ], [
                '...Planning for and managing a	work group.',
                '...Being the technical expert and consultant, the company looks to in times of crisis.',
                '...Being a mentor who helps people improve the quality of their lives.',
                '...Taking new risks which will give me an opportunity to grow.',
            ], [],
        ];
        for ($i = 0; $i <= count($questionsNl) - 1; $i++) {
            $question = new Question();
            $question->setText($questionsNl[$i]);
            $question->setOrderType($i + 1);
            $this->addReference(($i + 1) . '-question', $question);
            $eng = new QuestionTranslation('en', 'text', $questionsen[$i]);
            $question->addTranslation($eng);
            $question->setMinAvailableResult(4);
            $question->setAnswerType(Question::TYPE_LIST);
            $manager->persist($question);
            $manager->persist($eng);
            for ($j = 0; $j <= count($answersNl[$i]) - 1; $j++) {
                $answer = new Answer();
                $answer->setOrderType($j+1);
                $answer->setText($answersNl[$i][$j]);
                $answer->setQuestion($question);
                $this->addReference(($j + 1) . '-answer-' . ($i + 1) . '-question', $answer);
                $eng = new AnswerTranslation('en', 'text', $answersen[$i][$j]);
                $answer->addTranslation($eng);
                $manager->persist($answer);
                $manager->persist($eng);
            }
        }

        $ansNl = [
            'analytisch',
            'grondig',
            'fantasierijk',
            'afspraak is afspraak',
            'visionair',
            'verzoenend',
            'origineel',
            'inventief',
            'emotioneel',
            'onderzoekend',
            'scherpzinnig',
            'warm',
            'rationeel',
            'risico\'s uitsluiten',
            'ondersteunend',
            'georganiseerd'
        ];

        $ansen = [
            0 =>'analytical',
            1 =>'thorough',
            2 =>'imaginative',
            3 =>'promise is a promise',
            4 =>'visionary',
            5 =>'conciliatory',
            6 =>'original',
            7 =>'inventive',
            8 =>'emotional',
            9 =>'inquisitive',
            10 =>'perceptive',
            11 =>'warm',
            12 =>'rational',
            13 =>'eliminating risks',
            14 =>'supportive',
            15 =>'well organised',
        ];

        $question = new Question();
        $question->setText('Selecteer de 8 kenmerken die het best beschrijven hoe jij jezelf ziet.');
        $question->setOrderType($i + 1);
        $this->addReference(($i + 1) . '-question', $question);
        $eng = new QuestionTranslation('en', 'text',
            'Please choose 8 adjectives which best describe the way you see yourself.');
        $question->addTranslation($eng);
        $question->setMinAvailableResult(8);
        $question->setAnswerType(Question::TYPE_CHECKBOX);
        $manager->persist($question);
        $manager->persist($eng);

        for ($j = 0; $j <= count($ansNl) - 1; $j++) {
            $answer = new Answer();
            $answer->setOrderType($j+1);
            $answer->setText($ansNl[$j]);
            $answer->setQuestion($question);
            $answer->setResultId($j + 1);
            $this->addReference(($j + 1) . '-answer-' . ($i + 1) . '-question', $answer);
            $eng = new AnswerTranslation('en', 'text', $ansen[$j]);
            $answer->addTranslation($eng);
            $manager->persist($answer);
            $manager->persist($eng);
        }

        $question = new Question();
        $question->setText('Kies nu uit de 8 woorden die je net hebt aangevinkt er één, die jou het allerbest omschrijft.');
        $question->setOrderType($i + 2);
        $this->addReference(($i + 2) . '-question', $question);
        $eng = new QuestionTranslation('en', 'text',
            'Now, out of the 8 adjectives that you have chosen, pick 1 that describes you best of all.');
        $question->addTranslation($eng);
        $question->setMinAvailableResult(1);
        $question->setAnswerType(Question::TYPE_RADIO_BUTTON);
        $manager->persist($question);
        $manager->persist($eng);

        for ($j = 0; $j <= count($ansNl) - 1; $j++) {
            $answer = new Answer();
            $answer->setOrderType($j+1);
            $answer->setText($ansNl[$j]);
            $answer->setQuestion($question);
            $answer->setResultId($j + 1);
            $this->addReference(($j + 1) . '-answer-' . ($i + 2) . '-question', $answer);
            $eng = new AnswerTranslation('en', 'text', $ansen[$j]);
            $answer->addTranslation($eng);
            $manager->persist($answer);
            $manager->persist($eng);
        }
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 20;
    }
}